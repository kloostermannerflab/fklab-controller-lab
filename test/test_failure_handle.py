import logging
import unittest

from hive.actions import HardwareFailureError
from hive.registry import CallAction


class fake_catalog:
    def __init__(self):
        self.log_command = []

    def error_command(self):
        self.log_command.append("error_command")
        raise HardwareFailureError("hd1", "Error - generate fake command")

    def error_withid_command(self, element_id):
        self.log_command.append("error_command " + str(element_id))
        raise HardwareFailureError("hd1", "Error - generate fake command")

    def success_command(self, element_id):
        self.log_command.append("success_command " + str(element_id))

    def success_withoutid_command(self):
        self.log_command.append("success_command")


class TestFailureHandle(unittest.TestCase):
    def setUp(self):
        self.manage_actions = CallAction()
        self.fake = fake_catalog()
        self.manage_actions.add_action_catalog(self.fake)

    def test_handle_with_success_command(self):
        """Test without handle for success command and fail command."""

        with self.assertLogs(level="INFO") as cm:
            self.manage_actions.get_action("success_command", 3)
            self.manage_actions.get_action("fail_command")
        self.assertEqual(len(self.fake.log_command), 2)
        self.assertEqual(self.fake.log_command[0], "success_command 3")
        self.assertEqual(self.fake.log_command[0], "fail_command")
        self.assertEqual(
            cm.output,
            [
                "WARNING:root:Error - generate fake command",
                "WARNING:root:success_command 3",
            ],
        )

    def test_handle_with_success_command(self):
        """Test that handle are doing nothing in case of a success command."""
        handler = {"hd1": {"mode": "retry"}}
        self.manage_actions.set_handler(handler)
        self.manage_actions.get_action("success_command", 3)

        self.assertEqual(len(self.fake.log_command), 1)
        for i in range(len(self.fake.log_command)):
            self.assertEqual(self.fake.log_command[i], "success_command 3")

    def test_wrong_mode(self):
        """Test with a non-existing command."""
        handler = {"hd1": {"mode": {"notexistingmode": "success_command"}}}
        with self.assertRaises(ValueError):
            self.manage_actions.set_handler(handler)

    def test_default_retry_handle(self):
        """Test default retry mode.

        - max retry time = 1
        - without sleeping between retry
        """
        handler = {"hd1": {"mode": "retry"}}
        self.manage_actions.set_handler(handler)
        with self.assertLogs(level="INFO") as cm:
            self.manage_actions.get_action("error_command")

        self.assertEqual(len(self.fake.log_command), 2)
        for i in range(len(self.fake.log_command)):
            self.assertEqual(self.fake.log_command[i], "error_command")

        self.assertEqual(
            cm.output,
            [
                "WARNING:root:Error - generate fake command",
                "WARNING:root:After failure, try again (x1) times after sleeping for 0 secs.",
                "WARNING:root:Error - generate fake command",
            ],
        )

    def test_different_hardware_type(self):
        """Test with handler for a different hardware type. Should do nothing except log the errors."""
        handler = {"hd2": {"mode": "retry"}}
        self.manage_actions.set_handler(handler)
        with self.assertLogs(level="INFO") as cm:
            self.manage_actions.get_action("error_command")

        self.assertEqual(len(self.fake.log_command), 1)
        self.assertEqual(self.fake.log_command[0], "error_command")
        self.assertEqual(cm.output, ["WARNING:root:Error - generate fake command"])

    def test_retry_handle_with_max_times(self):
        """Test retry method with max_times option = 3 without sleeping."""
        retry = 3
        handler = {"hd1": {"mode": {"retry": {"max_times": retry}}}}
        self.manage_actions.set_handler(handler)
        with self.assertLogs(level="INFO") as cm:
            self.manage_actions.get_action("error_command")

        self.assertEqual(len(self.fake.log_command), retry + 1)
        for i in range(len(self.fake.log_command)):
            self.assertEqual(self.fake.log_command[i], "error_command")

        self.assertEqual(
            cm.output,
            [
                "WARNING:root:Error - generate fake command",
                "WARNING:root:After failure, try again (x1) times after sleeping for 0 secs.",
                "WARNING:root:Error - generate fake command",
                "WARNING:root:After failure, try again (x2) times after sleeping for 0 secs.",
                "WARNING:root:Error - generate fake command",
                "WARNING:root:After failure, try again (x3) times after sleeping for 0 secs.",
                "WARNING:root:Error - generate fake command",
            ],
        )

    def test_retry_handle_with_wait_times(self):
        """Test retry method with sleeping time = 1 between each retry."""
        handler = {"hd1": {"mode": {"retry": {"wait_time": 1}}}}
        self.manage_actions.set_handler(handler)
        with self.assertLogs(level="INFO") as cm:
            self.manage_actions.get_action("error_command")

        self.assertEqual(len(self.fake.log_command), 2)
        for i in range(len(self.fake.log_command)):
            self.assertEqual(self.fake.log_command[i], "error_command")

        self.assertEqual(
            cm.output,
            [
                "WARNING:root:Error - generate fake command",
                "WARNING:root:After failure, try again (x1) times after sleeping for 1 secs.",
                "WARNING:root:Error - generate fake command",
            ],
        )

    def test_default_change_command_mode_without_new_id(self):
        """Test default command method. element id is propagate from the initial failed command."""
        handler = {"hd1": {"mode": {"command": "success_command"}}}
        self.manage_actions.set_handler(handler)
        with self.assertLogs(level="INFO") as cm:
            self.manage_actions.get_action("error_withid_command", 3)

        self.assertEqual(len(self.fake.log_command), 2)
        self.assertEqual(self.fake.log_command[0], "error_command 3")
        self.assertEqual(self.fake.log_command[1], "success_command 3")
        self.assertEqual(
            cm.output,
            [
                "WARNING:root:Error - generate fake command",
                "WARNING:root:After failure, try this new command: success_command",
            ],
        )

    def test_command_mode_with_options(self):
        """Test command method with options.

        No element id were in the initial command but one is needed in the new command. This element id is passed
        throught the options.
        """
        handler = {"hd1": {"mode": {"command": "success_command 4"}}}
        self.manage_actions.set_handler(handler)
        with self.assertLogs(level="INFO") as cm:
            self.manage_actions.get_action("error_command")

        self.assertEqual(len(self.fake.log_command), 2)
        self.assertEqual(self.fake.log_command[0], "error_command")
        self.assertEqual(self.fake.log_command[1], "success_command 4")
        self.assertEqual(
            cm.output,
            [
                "WARNING:root:Error - generate fake command",
                "WARNING:root:After failure, try this new command: success_command",
            ],
        )

    def test_command_mode_with_full_description(self):
        """Test command method with full command description."""
        handler = {
            "hd1": {"mode": {"command": {"key": "success_command", "element_id": 4}}}
        }
        self.manage_actions.set_handler(handler)
        with self.assertLogs(level="INFO") as cm:
            self.manage_actions.get_action("error_withid_command", 3)

        self.assertEqual(len(self.fake.log_command), 2)
        self.assertEqual(self.fake.log_command[0], "error_command 3")
        self.assertEqual(self.fake.log_command[1], "success_command 4")
        self.assertEqual(
            cm.output,
            [
                "WARNING:root:Error - generate fake command",
                "WARNING:root:After failure, try this new command: success_command",
            ],
        )

    def test_command_mode_without_ids(self):
        """Test command method with full command description."""
        handler = {"hd1": {"mode": {"command": "success_withoutid_command"}}}
        self.manage_actions.set_handler(handler)
        with self.assertLogs(level="INFO") as cm:
            self.manage_actions.get_action("error_command")

        self.assertEqual(len(self.fake.log_command), 2)
        self.assertEqual(self.fake.log_command[0], "error_command")
        self.assertEqual(self.fake.log_command[1], "success_command")
        self.assertEqual(
            cm.output,
            [
                "WARNING:root:Error - generate fake command",
                "WARNING:root:After failure, try this new command: success_withoutid_command",
            ],
        )

    def test_specifier(self):
        """Two handlers for two different error messages.

        - error message ending by generate fake command should go in retry mode
        - other should do the command success_command instead.
        """
        handler = {
            "hd1": [
                {"type": ".*", "mode": {"command": "success_command"}},
                {"type": ".* generate fake command", "mode": "retry"},
            ]
        }

        self.manage_actions.set_handler(handler)
        with self.assertLogs(level="INFO") as cm:
            self.manage_actions.get_action("error_command")

        self.assertEqual(len(self.fake.log_command), 2)
        for i in range(len(self.fake.log_command)):
            self.assertEqual(self.fake.log_command[i], "error_command")

        self.assertEqual(
            cm.output,
            [
                "WARNING:root:Error - generate fake command",
                "WARNING:root:After failure, try again (x1) times after sleeping for 0 secs.",
                "WARNING:root:Error - generate fake command",
            ],
        )

    def test_specifier_falling_back_default_case(self):
        """The error message does not match the handler specified.
        It is falling back on the default behavior = just logging.
        """
        handler = {"hd1": [{"type": ".* filter wrong error message", "mode": "retry"}]}

        self.manage_actions.set_handler(handler)
        with self.assertLogs(level="INFO") as cm:
            self.manage_actions.get_action("error_command")

        self.assertEqual(len(self.fake.log_command), 1)
        self.assertEqual(self.fake.log_command[0], "error_command")

        self.assertEqual(cm.output, ["WARNING:root:Error - generate fake command"])

    def test_redundant_specifier(self):
        """Test with a non-existing command."""
        handler = {
            "hd1": [
                {"type": ".* filter wrong error message", "mode": "retry"},
                {
                    "type": ".* filter wrong error message",
                    "mode": {"command": "success_command"},
                },
            ]
        }
        with self.assertRaises(ValueError) as error:
            self.manage_actions.set_handler(handler)
