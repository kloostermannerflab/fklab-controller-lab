import unittest

import PyQt5
from PyQt5.QtCore import pyqtSlot

from hive.trigger import Trigger
from hive.trigger import UserCommand


class ResultMessage(PyQt5.QtCore.QObject):
    def __init__(self, trigger):
        PyQt5.QtCore.QObject.__init__(self)
        signal, state = trigger.who_to_connect_where()
        signal.connect(self.get_events)
        self.event_type = None
        self.event_data = None

    @pyqtSlot(dict)
    def get_events(self, event):
        self.event_type = event
        # self.filter_event(event_type, event_data)


class TestUserCommand(unittest.TestCase):
    def setUp(self):
        command = {"test": "test 1"}
        hardware = {"hdwtest": {"1", "2"}}
        self.user_cmd = UserCommand(command, hardware)
        self.result = ResultMessage(self.user_cmd)

    def test_parse_action_dict(self):
        pass
