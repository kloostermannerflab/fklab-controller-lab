import socket
import time

import zmq


def event_test():
    HOST = "127.0.0.1"  # Standard loopback interface address (localhost)
    PORT = 65432  # Port to listen on (non-privileged ports are > 1023)

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind((HOST, PORT))
        s.listen()
        conn, addr = s.accept()
        with conn:
            while True:
                data = conn.recv(1024)
                if data:
                    print(data)
                    conn.send(data)

    # print('Received', repr(data))


def event_listen():
    import socket
    import time

    HOST = "127.0.0.1"  # Standard loopback interface address (localhost)
    PORT = 65430  # Port to listen on (non-privileged ports are > 1023)

    MESSAGE_DETECTION = [
        ">EVNT 31 104 809 22-04-2020 18:52:53 on\r\n",
        ">EVNT 31 105 809 22-04-2020 18:52:53 on\r\n",
        ">EVNT 31 101 809 22-04-2020 18:52:53 on\r\n",
        ">EVNT 31 105 809 22-04-2020 18:52:53 on\r\n",
        ">EVNT 31 104 809 22-04-2020 18:52:53 on\r\n",
        ">EVNT 31 104 809 22-04-2020 18:52:53 on\r\n",
        ">EVNT 31 105 809 22-04-2020 18:52:53 on\r\n",
        ">EVNT 31 101 809 22-04-2020 18:52:53 on\r\n",
        ">EVNT 31 106 809 22-04-2020 18:52:53 on\r\n",
    ]

    MESSAGE_BATTERY = [
        ">EVNT 60 104 809 22-04-2020 18:52:53 30%\r\n",
        ">EVNT 60 105 809 22-04-2020 18:52:53 20%\r\n",
    ]

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind((HOST, PORT))
        s.listen(1)
        conn, addr = s.accept()
        i = 0
        j = 0
        with conn:
            while 1:
                message = input()
                if message == "q":
                    conn.send(MESSAGE_DETECTION[i].encode("utf-8"))
                    i = i + 1
                if message == "b":
                    conn.send(MESSAGE_BATTERY[j].encode("utf-8"))
                    j = j + 1
            s.close()


def neuralynx_fake_server():
    context = zmq.Context.instance()
    socket = context.socket(zmq.REP)
    socket.bind("tcp://*:53000")

    while 1:
        req = socket.recv_multipart()
        print(req)
        socket.send_multipart([b"OK"])


def tracking_fake_server():

    context = zmq.Context.instance()
    socket = context.socket(zmq.PUB)
    socket.bind("tcp://*:12345")

    while 1:
        socket.send(b"1 F1,1,1")
        time.sleep(1)
