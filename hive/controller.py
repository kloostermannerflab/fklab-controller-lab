from typing import Dict

from hive.actions import ArduinoDevices
from hive.actions import PeiraDevices
from hive.communication import Server
from hive.registry import CallAction
from hive.registry import CallTask


class Controller:
    """Layer between the view and the Task logic layer.

    Parameters
    ----------
    test : bool, opt
        test mode : all external communication are mocked. See Documentation about how to test the app as standalone.

    Attributes
    ----------
    _hardware : obj:`dict` of :obj:`list``of :obj:`str`
        Keep track of the devices used in the app organized by {hardware_type:[id1,id2,id...]}
    call_action : obj:`CallAction`
        Command registry with all catalog of actions used in the session.
    call_task : :obj:`CallTask`
        Task registry saving the task mode used with its selected triggers.
    task : :obj:`fklab.hive.implementation.task.task`
        Actual task logic layer
    trigger : obj:`dict` of :obj:`fklab.hive.trigger`
        all triggers used in the app {trigger_name : trigger_object}
    """

    def __init__(self, test: bool = False, ip: str = "192.168.0.100"):
        self.test = test
        self._command = None
        self._hardware: Dict[str, Dict[str, str]] = {}  # type

        self.call_action = CallAction()
        self.call_action.add_action_catalog(
            PeiraDevices(Server(test=test, ip_address=ip))
        )

        self.call_action.add_action_catalog(ArduinoDevices())
        self.call_task = CallTask(self.call_action)
        self.task = None
        self.trigger = None

    @property
    def config(self) -> Dict[str, str]:
        """Set the configuration of the experimentation.

        Only the Command and the Hardware part is saved in two attributes, respectively _command and _hardware.

        Parameters
        ----------
        config : :obj:`dict` of :obj:`str`
        """
        return self._command

    @config.setter
    def config(self, config: dict):

        self._command = config["Command"]
        self._hardware = config["Hardware"]
        self._failure_handler = config.get("On failure", {})

    def setup_devices(self, username: str):
        """Set the user for controlling devices and setup the devices."""
        assert self._command, "ERROR First, set config file.\n"
        self.call_action.set_user(username)
        self.call_action.set_handler(self._failure_handler)

        for hardware in self._hardware:
            for element_id in self._hardware[hardware].values():
                self.task.setup_devices(hardware, element_id)

    def create_task(self, config: Dict[str, str]):
        if not isinstance(config["trigger"], list):
            config["trigger"] = [config["trigger"]]

        for trigger_def in config["trigger"]:
            default_trigger = {"name": "", "options": {}}
            default_trigger.update(trigger_def)
            self.trigger = self.call_task.add_trigger(
                default_trigger["name"], test=self.test, **default_trigger["options"]
            )

        self.trigger = self.call_task.add_trigger(
            "UserCommand", command=self._command, hardware=self._hardware
        )
        self.task = (
            self.call_task.create_task(config["mode"])
            if "options" in config
            else self.call_task.create_task(config["mode"])
        )

    def disconnect(self):
        for hardware in self._hardware:
            for element_id in self._hardware[hardware].values():
                self.trigger["UserCommand"].trigger_internal_command(
                    {"action": "release", "targets": element_id}
                )

    def send_detection(self, key: str):
        """Receive manual detection event from the keyboard and send it to the keyboard trigger."""
        self.trigger["Keyboard"].create_detection(key)

    def send_task(self, command: str):
        """Interpret user order and engage actions."""
        if self._command is None:
            raise ValueError("ERROR first set config file.\n")
        self.trigger["UserCommand"].trigger_user_command(command)
