import logging
import random
import time
from typing import Dict
from typing import Generator
from typing import List
from typing import Optional

from ..trigger import Trigger
from .abstract_task import *
from .task_protocol import SequenceControl
from .task_protocol import SequenceStepResult

__all__ = ["TwoPWS"]


class TwoPWS(AbstractSequenceTask):
    """Protocol for 2 phases win-shift

    In this protocol, we put an animal in 8-arms radial maze.

       Phase 1:
           Choose 4 randoms arms
           Open door one after another + give reward when detected

       sleep 15s

       Phase 2:
            open all doors
            after visit, door closes
            give food in arm not selected in phase 1
            stop the trial once all 4 arm not selected has been visited

       **Fail conditions and consequences on the next steps**

       - Phase 1 : no fail
       - Phase 2 : no food in arm selected in phase 1 + stop the trial

       **Use in config file**

       .. code-block::

           Task:
             mode : 2PWS
             options:
               wait_time_btw_phase: 15 #s
             trigger :
               - name: Keyboard or Tracking
               - name: Timer

       **Devices used**

       `Food_delivery` devices named in `F1`, index from 1 to 8.
       `Door_delivery` devices names in `D1`, index from 1 to 8
       A detection center zone named `0`

    """

    def __init__(self, action, trigger):
        super().__init__(action, trigger)
        self._selected_arms = []
        self._non_selected_arms = []
        self._arms = list(range(1, 9))
        self._phase = 1
        self.mode_visit_arm_outside_sequence = True
        self._wait_time_btw_phase = 15  # s
        self.trial_number = 0
        self.detection_enabled = True

    def get_para(self, para: str = None):
        possible_para = ["wait_time_btw_phase"]
        if para is None:
            ret = super().get_para()
            ret.extend(possible_para)
            return ret
        else:
            return super().get_para(para)

    @property
    def wait_time_btw_phase(self):
        return self._wait_time_btw_phase

    @wait_time_btw_phase.setter
    def wait_time_btw_phase(self, value):
        self.log_emit(f"wait time between phase updated to {value}")
        self._wait_time_btw_phase = value

    @property
    def selected_arms(self) -> List[int]:
        return self._selected_arms

    @selected_arms.setter
    def selected_arms(self, value):
        if len(value) != 4:
            raise (ValueError("selected arms should a list of 4 arms."))

        self._selected_arms = value
        self._non_selected_arms = [v for v in self._arms if v not in value]

        self.log_emit(
            f"Selected arms : "
            f"[F{self._selected_arms[0]}; "
            f"F{self._selected_arms[1]}; "
            f"F{self._selected_arms[2]}; "
            f"F{self._selected_arms[3]}] "
        )
        self.log_emit(
            f"None selected arms : "
            f"[F{self._non_selected_arms[0]}; "
            f"F{self._non_selected_arms[1]}; "
            f"F{self._non_selected_arms[2]}; "
            f"F{self._non_selected_arms[3]}] "
        )

    @property
    def sequence(self) -> List[int]:
        return self.give_sequence(self._phase)

    def filter_event(self, event_type: str, event_data: str):
        logging.debug("%s %s", event_type, event_data)

        if (
            event_type in ("command", "detection")
            and self._state is self.STATE_RUN
            and self.detection_enabled
        ):
            event_data = event_data.strip()

            try:
                self.process_detection(event_data)
            except BaseException as e:
                logging.debug(f"ERROR %s %s", event_type, event_data, exc_info=e)
                self.log_emit(f"ERROR {event_type} {event_data}: {str(e)}")

        elif event_type == "battery level":
            device_id, percentage = event_data.strip().split(",")
            self.process_low_battery(device_id.strip(), percentage.strip())

        elif event_type == "timer":
            if event_data == "time_limit":
                self._cmd_end()
            elif event_data == "time_out":
                self.set_visit_note(
                    f"Animal stay timeout (time: {self._get_time_str(self.task_duration)})"
                )
            elif event_data == ("sleep_change_phase" + str(self.trial_number)):
                self.log_emit("Stop sleeping between phase")
                self.detection_enabled = True
                self.door_control.set_door(None, state=True)
                self.phase = 2
            elif event_data == "battery_check":
                devices = [*self.use_door_devices, *self.use_food_devices]
                if len(devices):
                    self.trigger_command("info", devices)

    def give_sequence(self, phase):
        if phase == 1:
            return [
                0,
                self.selected_arms[0],
                0,
                self.selected_arms[1],
                0,
                self.selected_arms[2],
                0,
                self.selected_arms[3],
            ]
        elif phase == 2:
            return self._non_selected_arms

    @property
    def phase(self) -> int:
        return self._phase

    @phase.setter
    def phase(self, value):
        self._phase = value
        self.log_emit(f"Active phase: {self._phase}")

    @property
    def use_food_devices(self) -> List[str]:
        """Use F1, F2, F3 as food devices.
        F0 = center of the maze - when we can open/close door
        Needs to be added as virtual id in the config file.
        """
        return ["F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8"]

    @property
    def use_door_devices(self) -> List[str]:
        """doors are automatically used in this protocol"""
        return ["D1", "D2", "D3", "D4", "D5", "D6", "D7", "D8"]

    def visit_arm(self, visit_arm: int):
        if visit_arm not in self._next_expected_arm:
            return

        super().visit_arm(visit_arm)

    def give_food(self, visit_arm: int, result: SequenceStepResult):

        if self.phase == 1:
            if visit_arm == 0:
                if result.next_arm is None:
                    self.door_control.set_door(None, state=False)

                    self.log_emit("Start sleeping between phase")

                    self.timer.add_timer(
                        "sleep_change_phase" + str(self.trial_number),
                        self.wait_time_btw_phase,
                        "one_shot",
                    )

                    self.detection_enabled = False
                    self._next_expected_arm = (
                        self._selected_arms + self._non_selected_arms
                    )

                    self.timer["sleep_change_phase" + str(self.trial_number)].start()

                else:
                    self.door_control.set_door(None, state=False)
                    self.door_control.set_door(f"D{result.next_arm}", state=True)
                    self._next_expected_arm = [result.next_arm]

            elif result.success:
                self.food_control.give_food(f"F{visit_arm}", reset=True)
                self._next_expected_arm = [0]

        elif self.phase == 2:
            if visit_arm == 0:
                self.door_control.set_door(self.close_door, state=False)
                self._next_expected_arm = self._selected_arms + self._non_selected_arms

                if self._stop:
                    self.stop_task(self._stop_with_success)
            else:
                if result.success:
                    self._s += 1
                    self.food_control.give_food(f"F{visit_arm}", reset=True)

                if not result.success or self._s == 4:
                    self._stop = True
                    self._stop_with_success = result.success
                    for d in self.use_door_devices:
                        if d != f"D{visit_arm}":
                            self.door_control.set_door(d, state=False)

                self.close_door = f"D{visit_arm}"
                self._next_expected_arm = [0]

    def stop_task(self, success=True):
        self.trial_number += 1
        if success:
            self.log_emit(f"Stop task - end of trial {self.trial_number} -  Success")
        else:
            self.log_emit(f"Stop task - end of trial {self.trial_number} -  Fail")
        self.door_control.set_door(None, state=False)
        self._state = self.STATE_INIT

        if self._control_gen is not None:
            self._control_gen.close()

    def new_seq_control(self) -> SequenceControl:
        return TwoPWSControl(self.give_sequence(1), self.give_sequence(2))

    def _cmd_start(self):
        if self._state is not self.STATE_INIT:
            self.log_emit("task has started")
            return
        self.phase = 1
        self.selected_arms = random.sample(self._arms, 4)
        self._sequence = self.give_sequence(1)
        super()._cmd_start()

        if self._state is self.STATE_RUN:
            self.door_control.set_door(None, state=False)

        self._s = 0
        self._stop = False
        self._next_expected_arm = [0]


class TwoPWSControl(SequenceControl):
    """PWS protocol on W-maze.

    phase 1: no error - open 1 arm at the time and give food in this arm.
    Arm moved only when the animal in the centrum of the maze

    phase 2: all doors are opened - give food in the 4 unselected arms - close the door once it has been visited
    """

    def __init__(
        self, sequence_phase1: List[int], sequence_phase2: List[int], total_arms: int = 8
    ):
        """

        Parameters
        ----------
        sequence_phase1
            arm IDs: [0, arm1, 0, arm2, 0, arm3, 0, arm4, 0]

        sequence_phase2
            arm IDs: [arm5, arm6, amr7, arm8]

        total_arms: int, 8
        """
        self.sequence = sequence_phase1
        self.sequence_phase1 = sequence_phase1
        self.sequence_phase2 = sequence_phase2
        self.phase = 1
        super().__init__(self.sequence, total_arms)

    def protocol(self) -> Generator[SequenceStepResult, int, None]:
        """2 phase window shift protocol on W-maze.

        Returns
        -------
        generator
            generator of SequenceStepResult
        """

        possible = None
        next_arm = None

        # noinspection PyTypeChecker
        next_visit: int = (yield SequenceStepResult(None, next_arm, possible))
        visit_history = []
        visit_cycle = []

        while True:
            if not (0 <= next_visit <= self.total_arms):
                success = None
            else:
                visit_history.append(next_visit)
                visit_seq = visit_cycle.copy()
                visit_seq.append(next_visit)

                if self.phase == 1:

                    if len(visit_history) == 9:
                        self.sequence = self.sequence_phase2
                        self.phase = 2
                        next_arm = None
                    elif next_visit == 0:
                        next_arm = self.sequence[len(visit_history)]
                    else:
                        next_arm = 0

                    success = True

                elif self.phase == 2:
                    if next_visit in self.sequence:
                        success = True
                        next_arm = 0
                    elif next_visit == 0:
                        success = True
                        next_arm = None
                    else:
                        success = False
                        next_arm = 0

            # noinspection PyTypeChecker
            next_visit = (yield SequenceStepResult(success, next_arm, possible))
