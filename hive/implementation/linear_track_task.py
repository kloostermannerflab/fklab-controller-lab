

from typing import Dict

from typing import Generator

from typing import List

from typing import Optional

from ..trigger import Trigger

from .abstract_task import *

from .task_protocol import SequenceControl

from .task_protocol import SequenceStepResult

__all__ = ["LinearTrackTask"]

class LinearTrackTask(AbstractSequenceTask):

    """Protocol for linear Track task

    In this protocol, we put an animal on a linear maze called with two food wells on each side, called L and R. 

    Valid sequences are:

    - R-L

    - L-R

    **Fail conditions and consequences on the next steps**

    - Re-enter in the same arm

      Example :  R-R  = fail + next expected steps: R-R]-L

    **Use in config file**

    .. code-block::

        Task:

          mode : LinearTrackTask

          options:

            max_trial: 250                 # (int) maximal number the animal can visit.

            time_out: 3600 # 1hr           # (int): maximal duration the task can perform.

            time_stay: 600 # 10 min        # (int): maximal duration the animal can stay on a platform.

            sequence:

                - 1 # Left arm id is 1

                - 2 # Right arm id is 2

          trigger :

            - name: Keyboard or Tracking

            - name: Timer

            - name: Server

    **Devices used**

    `Food_delivery` devices named in `F1`, index from 1 to 3.

    .. image:: ../image/W_alternation_task.png

    """

    def __init__(self, action, trigger, sequence: Optional[List[int]] = None):

        super().__init__(action, trigger)

        if sequence is None:

            self._sequence = [1, 2]

        else:

            self._sequence = sequence

    @property

    def sequence(self) -> List[int]:

        """sequence [Left, Right]"""

        return self._sequence

    @sequence.setter

    def sequence(self, sequence: Optional[List[int]]):

        if sequence is not None and len(sequence) == 2:

            self._sequence = sequence

        self.log_emit(

            f"Sequence : [Left: F{self._sequence[0]}  Right: F{self._sequence[1]}]"

        )

    @property

    def use_food_devices(self) -> List[str]:

        """Use F1, F2 as food devices.

        Needs to be added as virtual id in the config file.

        """

        return ["F1", "F2"]

    @property

    def use_door_devices(self) -> List[str]:

        """No doors are automatically used in this protocol"""

        return []

    def new_seq_control(self) -> SequenceControl:

        """Alternation protocol on Linear track.

       Check the local history for valid sequence. If it fails, the local history is changed to match the future valid

       sequence.

       The first step is always a success. 

        """

        shortcut = ["", "", ""]

        for i, value in enumerate(["l", "r"]):

            shortcut[self._sequence[i] - 1] = value

        self.process_command("shortcut " + "".join(shortcut))

        self.log_emit(

            f"Sequence : [Left: F{self._sequence[0]}, Right: F{self._sequence[1]}]"

        )

        return LinTrackSeqControl(self._sequence)

    def give_food(self, visit_arm: int, result: SequenceStepResult):

        if result.success:

            self.food_control.give_food(f"F{visit_arm}", reset=True)

        # no other device control.

    # Override the logging method

    def logging_result_visit_arm(self, visit_arm):

        # logging

        if visit_arm > 0:

            if self._control_step.success is None:

                correct = "pass"

            elif self._control_step.success:

                correct = "correct"

            else:

                correct = "incorrect"

            self.log_emit(

                f"animal visits[{len(self._visit)}] = {self._keyboard_shortcut[visit_arm-1].upper()} "

                f"(time: {(self._get_time_str(self.task_duration))}) "

                f"({correct})"

            )

    def _cmd_start(self):

        if self._state is not self.STATE_INIT:

            self.log_emit("task has started")

            return

        super()._cmd_start()

#        if self._state is self.STATE_RUN:
#
#            # fill all food devices
#
#            self.food_control.fill()
#
LinearTrackTask._cmd_seq.__doc__ = """\

show sequence [Left, Right]. (sequence cannot be set in this task)

"""

class LinTrackSeqControl(SequenceControl):

    """Linear track protocol on a linear track.

       Check the local history for valid sequence. If it fails, the local history is changed to match the future valid

       sequence.

       The first step is always a success. Then the other side of the linear track is the correct entry.

    """

    def __init__(self, sequence: List[int], total_arms: int = 2):

        """

        Parameters

        ----------

        sequence

            arm IDs: [left_arm, right_arm]

        total_arms

        """

        self.sequence = sequence

        super().__init__(sequence, total_arms)

    def protocol(self) -> Generator[SequenceStepResult, int, None]:

        """Linear track protocol on a linear track.

        Returns

        -------

        generator

            generator of SequenceStepResult

        """

        L, R = self.sequence

        possible = list(self.sequence)

        next_arm = None

        # noinspection PyTypeChecker

        next_visit: int = (yield SequenceStepResult(None, next_arm, possible))

        visit_history = []

        visit_cycle = []

        while True:

            if not (0 < next_visit <= self.total_arms):

                success = None

            else:

                visit_history.append(next_visit)

                visit_seq = visit_cycle.copy()

                visit_seq.append(next_visit)

                if len(visit_seq) == 1:

                    # at the beginning, the animal be be place in any place.

                    # no matter what, it always success at the first visit.

                    success = True

                else: 

                    # the second visit

                    visit_seq = visit_seq[-2:]

                    success = (

                        visit_seq == [L, R]

                        or visit_seq == [R, L]

                    )

                if success:

                    visit_cycle.append(next_visit)

                elif next_visit == visit_cycle[-1]:  # Re-entering arm case

                    # do nothing - ignore it in the local history

                    pass

                else:

                    # erase history to keep only the new sequence starting from the error

                    visit_cycle = [next_visit]

            # noinspection PyTypeChecker

            next_visit = (yield SequenceStepResult(success, next_arm, list(possible)))

