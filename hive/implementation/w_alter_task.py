import logging
from typing import Dict
from typing import Generator
from typing import List
from typing import Optional

from ..trigger import Trigger
from .abstract_task import *
from .task_protocol import SequenceControl
from .task_protocol import SequenceStepResult

__all__ = ["WAlterTask"]


class WAlterTask(AbstractSequenceTask):
    """Protocol for W Alternation Task

    In this protocol, we put an animal in 3-arms maze called Center, Right and Left arms. Each experiment arm changed their
    position in the sequence.

    Valid sequences are:
    - C-R-C-L
    - C-L-C-R
    - R-C-L-C
    - L-C-R-C

    **Fail conditions and consequences on the next steps**

    - Re-enter in the same arm
      Example :  C-R-R  = fail + next expected steps: C-R-R]-C-L

    - Enter in wrong arm instead of C
      Example : R-C-L-R = fail + next expected steps: R-C-L-R]-C-L

    **Use in config file**

    .. code-block::

        Task:
          mode : WAlterTask
          options:
            max_trial: 250                 # (int) maximal number the animal can visit.
            time_out: 3600 # 1hr           # (int): maximal duration the task can perform.
            time_stay: 600 # 10 min        # (int): maximal duration the animal can stay on a platform.
            circ_behav_detect: 4           #(int): maximal redundant iterations where to stop giving food in the center
            sequence:
                - 1 # Left arm id is 1
                - 2 # Center arm id is 2
                - 3 # Right arm id 3

          trigger :
            - name: Keyboard or Tracking
            - name: Timer
            - name: Server

    **Devices used**

    `Food_delivery` devices named in `F1`, index from 1 to 3.

    .. image:: ../image/W_alternation_task.png

    """

    def __init__(self, action, trigger, sequence: Optional[List[int]] = None):
        super().__init__(action, trigger)

        if sequence is None:
            self._sequence = [1, 2, 3]
        else:
            self._sequence = sequence

        self._circ_behav_detect = 0
        self._arduino_stim = False

    @property
    def sequence(self) -> List[int]:
        """sequence [Left, Center, Right]"""
        return self._sequence

    @sequence.setter
    def sequence(self, sequence: Optional[List[int]]):
        if sequence is not None and len(sequence) == 3:
            self._sequence = sequence

        self.log_emit(
            f"Sequence : [Left: F{self._sequence[0]} Center: F{self._sequence[1]} Right: F{self._sequence[2]}]"
        )

    def get_para(self, para: str = None):
        possible_para = ["sequence", "circ_behav_detect", "arduino_stim"]
        if para is None:
            ret = super().get_para()
            ret.extend(possible_para)
            return ret
        else:
            return super().get_para(para)

    @property
    def circ_behav_detect(self) -> int:
        """sequence [Left, Center, Right]"""
        return self._circ_behav_detect

    @circ_behav_detect.setter
    def circ_behav_detect(self, circ_behav_detect: int):

        self._circ_behav_detect = circ_behav_detect

        if circ_behav_detect == 0:
            self.log_emit(f"Detection of circular fail behavior is deactivated.")
        else:
            self.log_emit(
                f"Circular fail behavior is detected after {self.circ_behav_detect} times to do (same arm - C)."
            )

    @property
    def arduino_stim(self) -> int:
        """sequence [Left, Center, Right]"""
        return self._arduino_stim

    @arduino_stim.setter
    def arduino_stim(self, value: bool):

        self._arduino_stim = value

        if self._arduino_stim:
            self.log_emit(f"Activate arduino recording")
        else:
            self.log_emit(f"Deactivate arduino recording")

    @property
    def use_food_devices(self) -> List[str]:
        """Use F1, F2, F3 as food devices.

        Needs to be added as virtual id in the config file.
        """
        return ["F1", "F2", "F3"]

    @property
    def use_door_devices(self) -> List[str]:
        """doors are automatically used in this protocol"""
        return ["D1", "D2", "D3"]

    def process_detection(self, device_id):
        if device_id in "0123456789":  # visiting event
            self.visit_arm(int(device_id))
            if self._arduino_stim:
                self.trigger_command("start_pulse", "A1")

        elif (  # device shortcut
            self._keyboard_shortcut is not None and device_id in self._keyboard_shortcut
        ):
            self.visit_arm(self._keyboard_shortcut.index(device_id) + 1)
            if self._arduino_stim:
                self.trigger_command("start_pulse", "A1")

        if device_id == "othing" and self._arduino_stim:
            self.trigger_command("stop_pulse", "A1")

    def new_seq_control(self) -> SequenceControl:
        """Alternation protocol on W-maze.

       Check the local history for valid sequence. If it fails, the local history is changed to match the future valid
       sequence.
       The first step is always a success. If the first step is C, the second step can be R or L. After that the sequence
       is to switch between R and L with always passing by C between.

        """
        shortcut = ["", "", ""]
        for i, value in enumerate(["l", "c", "r"]):
            shortcut[self._sequence[i] - 1] = value

        self.process_command("shortcut " + "".join(shortcut))
        self.log_emit(
            f"Sequence : [Left: F{self._sequence[0]}, Center: F{self._sequence[1]}, Right: F{self._sequence[2]}]"
        )
        return WAlterSeqControl(self._sequence, circ_behav_detect=self._circ_behav_detect)

    def give_food(self, visit_arm: int, result: SequenceStepResult):
        if result.success:
            self.food_control.give_food(f"F{visit_arm}", reset=True)

        # no other device control.

    # Override the logging method
    def logging_result_visit_arm(self, visit_arm):
        # logging
        if visit_arm > 0:
            if self._control_step.success is None:
                correct = "pass"
            elif self._control_step.success:
                correct = "correct"
            else:
                correct = "incorrect"
            self.log_emit(
                f"animal visits[{len(self._visit)}] = {self._keyboard_shortcut[visit_arm-1].upper()} "
                f"(time: {(self._get_time_str(self.task_duration))}) "
                f"({correct})"
            )

    def _cmd_start(self):
        if self._state is not self.STATE_INIT:
            self.log_emit("task has started")
            return
        super()._cmd_start()
        if self._state is self.STATE_RUN:
            # fill all food devices
            self.door_control.set_door(None, state=True)


WAlterTask._cmd_seq.__doc__ = """\
show sequence [Left, Center, Right]. (sequence cannot be set in this task)
"""


class WAlterSeqControl(SequenceControl):
    """Alternation protocol on W-maze.

       Check the local history for valid sequence. If it fails, the local history is changed to match the future valid
       sequence.
       The first step is always a success. If the first step is C, the second step can be R or L. After that the sequence
       is to switch between R and L with always passing by C between.
    """

    def __init__(
        self, sequence: List[int], total_arms: int = 3, circ_behav_detect: int = 0
    ):
        """

        Parameters
        ----------
        sequence
            arm IDs: [left_arm, center_arm, right_arm]
        total_arms
        """
        self.sequence = sequence
        self.circ_behav_detect = circ_behav_detect
        super().__init__(sequence, total_arms)

    def protocol(self) -> Generator[SequenceStepResult, int, None]:
        """Alternation protocol on W-maze.

        Returns
        -------
        generator
            generator of SequenceStepResult
        """
        L, C, R = self.sequence
        possible = list(self.sequence)
        next_arm = None

        # noinspection PyTypeChecker
        next_visit: int = (yield SequenceStepResult(None, next_arm, possible))
        visit_history = []
        visit_cycle = []

        while True:
            circular_behavior_detected = False
            if not (0 < next_visit <= self.total_arms):
                success = None
            else:
                visit_history.append(next_visit)
                visit_seq = visit_cycle.copy()
                visit_seq.append(next_visit)
                if len(visit_seq) == 1:
                    # at the beginning, the animal be be place in any place.
                    # no matter what, it always success at the first visit.
                    success = True
                elif len(visit_seq) == 2:
                    # the second visit

                    circular_behavior_detected = self._detect_fail_circular_behavior(
                        visit_history
                    )

                    success = (
                        (visit_seq == [L, C] and not circular_behavior_detected)
                        or (visit_seq == [R, C] and not circular_behavior_detected)
                        or visit_seq == [C, R]
                        or visit_seq == [C, L]
                    )

                else:
                    # the third visit
                    visit_seq = visit_seq[-3:]
                    success = (
                        visit_seq == [C, L, C]
                        or visit_seq == [C, R, C]
                        or visit_seq == [L, C, R]
                        or visit_seq == [R, C, L]
                    )

                if success:
                    visit_cycle.append(next_visit)
                elif next_visit == visit_cycle[-1]:  # Re-entering arm case
                    # do nothing - ignore it in the local history
                    pass
                else:
                    if circular_behavior_detected:
                        visit_cycle = [visit_cycle[-1], next_visit]
                    else:
                        # erase history to keep only the new sequence starting from the error
                        visit_cycle = [next_visit]

                if next_visit == C:
                    if len(visit_cycle) == 1:
                        next_arm = None
                    elif visit_history[-2] == L:
                        next_arm = R
                    else:
                        next_arm = L
                else:
                    next_arm = C
            # noinspection PyTypeChecker
            next_visit = (yield SequenceStepResult(success, next_arm, list(possible)))

    def _detect_fail_circular_behavior(self, visit_history):
        if self.circ_behav_detect == 0:
            return False

        i = 0
        if len(visit_history) < self.circ_behav_detect * 2:
            return False

        while i < self.circ_behav_detect - 1:
            if (
                visit_history[-(i + 2) * 2 : -(i + 1) * 2] != visit_history[-2:]
            ):  # for [L,C,L,C,L,C] verify if  [L,C] ==  [L,C]

                return False
            i = i + 1

        return True
