import collections
from typing import Dict
from typing import Generator
from typing import List
from typing import Optional

from ..trigger import Trigger
from .abstract_task import *
from .task_protocol import SequenceControl
from .task_protocol import SequenceStepResult

__all__ = ["SequenceTaskErrorCorrection"]


class SequenceTaskErrorCorrection(AbstractSequenceTask):
    """Protocol for Sequence Task with Error Correction.

    In this protocol, we put an animal in 8-arms maze. Giving a random 4-length sequence, the animal needs to learning
    and remember this visiting sequence in order to get the food reward. At the beginning, we open the door (according to
    mode **open-selected** or  **open-all**), and the animal can free move to any arm (food give according to
    **start-first** or **start-any**). The next expected correct arm is according to the sequence. Once the animal made
    a mistake, if under **with-correction** , the animal needs to find the correct one, and any visit before correct
    won't change the correct arms. If under **without-correction**. any visiting will change to the next expected arm.

    **Protocol conditions (mode).**

    1.  **without-door-close** (`MODE 1-`) - We do nothing when the animal made a mistake.
    2.  **with-door-close** (`MODE 1`, `MODE 1+`) - We close the previous door which left from the un-expected arm,
        and reopen when the animal find the correct arm.

    3.  **open-selected** (`MODE 2-`) - we only open the selected doors (set in the sequence) at the beginning.
    4.  **open-all** (`MODE 2`, `MODE 2+`) - we open all of the doors at the beginning.

    5.  **start-first** (`MODE 3-`) - the first success visit should be the first arm in the sequence.
    6.  **start-any** (`MODE 3`, `MODE 3+`) - the first success visit can be any arm in the sequence.

    7.  **without-correction** (`MODE 4-`) - the next expected arm will change when the animal enter to any arms.
    8.  **with-correction** (`MODE 4`, `MODE 4+`) - the next expected arm will not change until the animal visit the correct arms.

    7.  **food-consist** (`MODE 5-`) - the mount of food given do not change over all session.
    8.  **food-increase** (`MODE 5`, `MODE 5+`) - the mount of food given increase when the animal visit the correct arm continuous.

    **Stopping conditions**

    1.  the animal visited over `max_trials` arms.
    2.  the animal spent `time_limit` in the maze.
    3.  the animal stay on a platform over `timeout`. In this case, the animal does not have enough motivation to explore the maze.

    **Measurement of how good the animal's performance**

    1.  total successful visiting number over total visiting.
    2.  continuous successful visiting over 12~15 times.
    3.  extra mathematical analyze.


    **Use in config file**

    .. code-block::

        Task:
          mode : SequenceTaskErrorCorrection
          options:
            max_trial: 250                      # (int) maximal number the animal can visit.
            time_out: 3600 # 1hr                # (int) maximal duration the task can perform.
            time_stay: 600 # 10 min             # (int) maximal duration the animal can stay on a platform.
            keyboard_shortcut: FDSAQWER
            mode:                               # See Protocol Conditions
              - without-correction
              - start-any
              - open-selected
              - without-door-close
          trigger :
            - name: Keyboard
            - name: Timer
            - name: Server

    **Devices used**

    `Door` devices named in `D1`, index from 1 to 8, and
    `Food_delivery` devices named in `F1`, index from 1 to 8.


    .. image:: ../image/sequence_task_err_corr.png
    """

    MODE_DOOR_CLOSE = "with-door-close"
    MODE_DOOR_NOTHING = "without-door-close"

    MODE_OPEN_SELECTED = "open-selected"
    MODE_OPEN_ALL = "open-all"

    MODE_START_FIRST = "start-first"
    MODE_START_ANY = "start-any"

    MODE_WITH_CORR = "with-correction"
    MODE_WITHOUT_CORR = "without-correction"

    def __init__(self, action, trigger: Dict[str, Trigger]):
        super().__init__(action, trigger)

        # mode
        self._mode1 = self.MODE_DOOR_NOTHING
        """about how door to react with the animal behavior"""
        self._mode2 = self.MODE_OPEN_SELECTED
        """about which doors need to open at the beginning"""
        self._mode3 = self.MODE_START_ANY
        """about which element in the sequence does the animal need to visit first"""
        self._mode4 = self.MODE_WITHOUT_CORR
        """about the door control"""

    def get_mode(self) -> List[str]:
        return [self._mode1, self._mode2, self._mode3, self._mode4, *super().get_mode()]

    def set_mode(self, mode: str = None, verbose=True):
        if mode is None:
            self.log_emit(
                # protocol, open door, start sequence, food control, door control
                f"mode : {self._mode4},{self._mode2},{self._mode3},{self.mode_food},{self._mode1}"
            )

        elif mode in ("1-", self.MODE_DOOR_NOTHING):
            self._mode1 = self.MODE_DOOR_NOTHING
            if verbose:
                self.log_emit(f"set mode : {self._mode1}")

        elif mode in ("1", "1+", self.MODE_DOOR_CLOSE):
            self._mode1 = self.MODE_DOOR_CLOSE
            if verbose:
                self.log_emit(f"set mode : {self._mode1}")

        elif mode in ("2-", self.MODE_OPEN_SELECTED):
            self._mode2 = self.MODE_OPEN_SELECTED
            self.mode_visit_arm_outside_sequence = False
            if verbose:
                self.log_emit(f"set mode : {self._mode2}")

        elif mode in ("2", "2+", self.MODE_OPEN_ALL):
            self._mode2 = self.MODE_OPEN_ALL
            self.mode_visit_arm_outside_sequence = True
            if verbose:
                self.log_emit(f"set mode : {self._mode2}")

        elif mode in ("3-", self.MODE_START_FIRST):
            self._mode3 = self.MODE_START_FIRST
            if verbose:
                self.log_emit(f"set mode : {self._mode3}")

        elif mode in ("3", "3+", self.MODE_START_ANY):
            self._mode3 = self.MODE_START_ANY
            if verbose:
                self.log_emit(f"set mode : {self._mode3}")

        elif mode in ("4-", self.MODE_WITHOUT_CORR):
            self._mode4 = self.MODE_WITHOUT_CORR
            if verbose:
                self.log_emit(f"set mode : {self._mode4}")

        elif mode in ("4", "4+", self.MODE_WITH_CORR):
            self._mode4 = self.MODE_WITH_CORR
            if verbose:
                self.log_emit(f"set mode : {self._mode4}")

        elif mode in ("5-", self.MODE_FOOD_CONSIST):
            super().set_mode(self.MODE_FOOD_CONSIST, verbose=verbose)

        elif mode in ("5", "5+", self.MODE_FOOD_INCREASE):
            super().set_mode(self.MODE_FOOD_INCREASE, verbose=verbose)

        else:
            super().set_mode(mode, verbose=verbose)

    def new_seq_control(self) -> SequenceControl:
        """Create the protocol.

        - Mode with correction: ``ErrCorrSeqControl``
          The animal needs to follow a certain visiting sequence to get the rewards. Once the animal make a mistake
          by visiting un-expected arm, the animal needs to correct its choose by visiting the correct arm according
          to its last correct visited arm.

        - Mode without correction: ``DefaultSeqControl``
          The animal needs to follow a certain visiting sequence to get the rewards.

        """
        if self._mode4 == self.MODE_WITH_CORR:
            return ErrCorrSeqControl(
                self._sequence,
                total_arms=8,
                start_any_seq=self._mode3 == self.MODE_START_ANY,
                all_door_open=self._mode2 == self.MODE_OPEN_ALL,
                with_door_close=self._mode1 == self.MODE_DOOR_CLOSE,
            )
        else:
            return DefaultSeqControl(
                self._sequence,
                total_arms=8,
                start_any_seq=self._mode3 == self.MODE_START_ANY,
                all_door_open=self._mode2 == self.MODE_OPEN_ALL,
                with_door_close=self._mode1 == self.MODE_DOOR_CLOSE,
            )


SequenceTaskErrorCorrection._cmd_mode.__doc__ = f"""\
set mode
1- : mode {SequenceTaskErrorCorrection.MODE_DOOR_NOTHING}
1+ : mode {SequenceTaskErrorCorrection.MODE_DOOR_CLOSE}
2- : mode {SequenceTaskErrorCorrection.MODE_OPEN_SELECTED}
2+ : mode {SequenceTaskErrorCorrection.MODE_OPEN_ALL}
3- : mode {SequenceTaskErrorCorrection.MODE_START_FIRST}
3+ : mode {SequenceTaskErrorCorrection.MODE_START_ANY}
4- : mode {SequenceTaskErrorCorrection.MODE_WITHOUT_CORR}
4+ : mode {SequenceTaskErrorCorrection.MODE_WITH_CORR}
5- : mode {SequenceTaskErrorCorrection.MODE_FOOD_CONSIST}
5+ : mode {SequenceTaskErrorCorrection.MODE_FOOD_INCREASE}
"""


class DefaultSeqControl(SequenceControl):
    """Sequence task protocol.

    The animal needs to follow a certain visiting sequence to get the rewards.

    Attributes
    ----------

    start_any_seq
        Does animal can start the sequence from any arms?
    all_door_open
        Do the doors which is not included in sequence open?
    with_door_close
        Use door-close protocol? It will close one of the un-expected door according to the animal current
        visiting.

    """

    def __init__(
        self,
        sequence: List[int],
        total_arms: int = 8,
        start_any_seq=False,
        all_door_open=False,
        with_door_close=False,
    ):
        super().__init__(sequence, total_arms)
        self.start_any_seq = start_any_seq
        self.all_door_open = all_door_open
        self.with_door_close = with_door_close

    def protocol(self) -> Generator[SequenceStepResult, int, None]:
        """Sequence task protocol.

        Returns
        -------
        generator
            generator of SequenceStepResult

        """
        # setup doors state
        if not self.all_door_open:
            door = [(it + 1) in self.sequence for it in range(self.total_arms)]
        else:
            door = [True] * self.total_arms

        def _possible():
            return [it + 1 for it in range(self.total_arms) if door[it]]

        # init states
        # noinspection PyTypeChecker
        next_visit: int = (yield SequenceStepResult(None, None, _possible()))
        prev_visit: Optional[int] = None

        if self.start_any_seq:
            next_arm = None
        else:
            next_arm = self.sequence[0]

        index = 0
        visit_count = collections.Counter()

        while True:
            if not (0 <= next_visit <= self.total_arms):
                success = None
            elif next_visit == 0:
                # back to center platform
                success = None
                if self.with_door_close and prev_visit is not None:
                    door[prev_visit - 1] = False
            else:
                # exam correct or not
                if next_arm is None:
                    try:
                        index = self.sequence.index(next_visit)
                        success = True
                    except ValueError:
                        success = False
                        index = None
                else:
                    success = next_visit == next_arm

                # update next correct arm
                if success:
                    index += 1
                    next_arm = self.sequence[index % len(self.sequence)]
                else:
                    try:
                        index = self.sequence.index(next_visit)
                    except ValueError:
                        pass
                    else:
                        index += 1
                        next_arm = self.sequence[index % len(self.sequence)]

                if not door[next_visit - 1]:
                    # force enter
                    door[next_visit - 1] = True

                if self.with_door_close:
                    # random close one of the future incorrect doors.
                    # in practice, there are only tow possible choice for each visiting (no matter correct or not),
                    # we do not randomize, but switching the closing door.
                    visit_count[next_visit] += 1
                    if next_arm is not None and next_visit in self.sequence:
                        door_open = [
                            it
                            for it in self.sequence
                            if it != next_visit and it != next_arm
                        ]
                        door_open = door_open[
                            visit_count[next_visit] % (len(self.sequence) - 2)
                        ]

                        for it in self.sequence:
                            door[it - 1] = (
                                it == next_visit or it == next_arm or it == door_open
                            )
                # close the door at previous arm, which the arm should not be opened.
                if prev_visit is not None and prev_visit not in self.sequence:
                    door[prev_visit - 1] = False

                prev_visit = next_visit

            # noinspection PyTypeChecker
            next_visit = (yield SequenceStepResult(success, next_arm, _possible()))


class ErrCorrSeqControl(SequenceControl):
    """Sequence task protocol with error correction protocol.

    The animal needs to follow a certain visiting sequence to get the rewards. Once the animal make a mistake
    by visiting un-expected arm, the animal needs to correct its choose by visiting the correct arm according
    to its last correct visited arm.

    Attributes
    ----------

    start_any_seq
        Does animal can start the sequence from any arms?
    all_door_open
        Do the doors which is not included in sequence open?
    with_door_close
        Use door-close protocol? It will close the incorrect arm when the animal leaves from.

    """

    def __init__(
        self,
        sequence: List[int],
        total_arms: int = 8,
        start_any_seq=False,
        all_door_open=False,
        with_door_close=False,
    ):
        super().__init__(sequence, total_arms)
        self.start_any_seq = start_any_seq
        self.all_door_open = all_door_open
        self.with_door_close = with_door_close

    def protocol(self) -> Generator[SequenceStepResult, int, None]:
        """Sequence task protocol with error correction protocol.

        Returns
        -------
        generator
            generator of SequenceStepResult

        """
        door = [False] * self.total_arms

        def _reset():
            if not self.all_door_open:
                for i in range(self.total_arms):
                    door[i] = (i + 1) in self.sequence
            else:
                for i in range(self.total_arms):
                    door[i] = True

        def _possible():
            return [it + 1 for it in range(self.total_arms) if door[it]]

        _reset()

        # init states
        # noinspection PyTypeChecker
        next_visit: int = (yield SequenceStepResult(None, None, _possible()))
        prev_visit: Optional[int] = None

        if self.start_any_seq:
            next_arm = None
        else:
            next_arm = self.sequence[0]

        index = 0

        while True:
            if not (0 <= next_visit <= self.total_arms):
                success = None
            elif next_visit == 0:
                # back to center platform
                success = None
                if self.with_door_close and prev_visit is not None:
                    door[prev_visit - 1] = False
            else:
                if next_arm is None:
                    try:
                        index = self.sequence.index(next_visit)
                        success = True
                    except ValueError:
                        success = False
                        index = None
                else:
                    success = next_visit == next_arm

                # update next correct arm
                if success:
                    index += 1
                    next_arm = self.sequence[index % len(self.sequence)]
                    _reset()
                else:
                    if not door[next_visit - 1]:
                        # force enter
                        door[next_visit - 1] = True

                    if self.with_door_close:
                        if prev_visit is not None and prev_visit != next_visit:
                            door[prev_visit - 1] = False

                # close the door at previous arm, which the arm should not be opened.
                if prev_visit is not None and prev_visit not in self.sequence:
                    door[prev_visit - 1] = False

                prev_visit = next_visit

            # noinspection PyTypeChecker
            next_visit = (yield SequenceStepResult(success, next_arm, _possible()))


class LinearTrackSeqControl(SequenceControl):
    """**Sequence control on linear track. The animal needs to alter the visiting arm to get the rewards.**

    """

    def __init__(self):
        super().__init__([1, 2], 2)

    def protocol(self) -> Generator[SequenceStepResult, int, None]:
        """Linear track protocol which the animal needs to alter the visiting arm to get the rewards.

        Returns
        -------
        generator
            generator SequenceStepResult

        """
        # init state
        possible = [1, 2]

        # noinspection PyTypeChecker
        next_visit: int = (yield SequenceStepResult(None, None, possible))
        index = 0
        next_arm = None

        while True:
            if not (0 < next_visit <= self.total_arms):
                success = None
            else:
                # exam correct or not
                if next_arm is None:
                    try:
                        index = self.sequence.index(next_visit)
                    except ValueError:
                        success = False
                    else:
                        success = True
                else:
                    success = next_visit == next_arm

                if success:
                    index = (index + 1) % 2
                    next_arm = self.sequence[index]

            # noinspection PyTypeChecker
            next_visit = (yield SequenceStepResult(success, next_arm, possible))
