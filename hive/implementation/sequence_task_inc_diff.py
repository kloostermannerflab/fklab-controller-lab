from typing import Dict
from typing import Generator
from typing import List
from typing import Optional
from typing import Union

from ..trigger import Trigger
from .abstract_task import *
from .task_protocol import SequenceControl
from .task_protocol import SequenceStepResult

__all__ = ["SequenceTaskIncrementalDifficulty"]


class IncDefSeqStepResult(SequenceStepResult):
    def __init__(
        self,
        success: Optional[bool],
        next_arm: Optional[int],
        possible: List[int],
        difficulty: int,
    ):
        super().__init__(success, next_arm, possible)
        self.difficulty = difficulty


class SequenceTaskIncrementalDifficulty(AbstractSequenceTask):
    """Protocol for Sequence Task with Incremental Difficulty.

    In this protocol, we put an animal in 8-arms maze. Giving a random 4-length sequence, the animal needs to learning
    and remember this visiting sequence in order to get the food reward. At the beginning, we open the door the first 2
    sequence (MODE **open-selected**), which is the beginning level of the difficulty, and the animal can free move to
    any arm (MODE **start-any**). The next expected correct arm is according to the sequence. Any arm the animal visited,
    no matter whether correct or not, will change the next expected arm (MODE **without-correction**). When the performance
    of the animal reach certain criteria, the difficulty will increase by opening the additional door.

    **Protocol conditions (mode)**

    1.  **food-consist** - the mount of food given do not change over all session.
    2.  **food-increase** - the mount of food given increase when the animal visit the correct arm continuous.

    **Stopping conditions**

    1.  the animal visited over `max_trials` arms.
    2.  the animal spent `time_limit` in the maze.
    3.  the animal stay on a platform over `timeout`. In this case, the animal does not have enough motivation to explore the maze.

    **Measurement of how good the animal's performance**

    1.  total successful visiting number over total visiting.
    2.  continuous successful visiting over 12~15 times.
    3.  extra mathematical analyze.


    **Use in config file**

    .. code-block::

        Task:
          mode : SequenceTaskIncrementalDifficulty
          options:
            max_trial: 250                 # (int) maximal number the animal can visit.
            time_out: 3600 # 1hr           # (int): maximal duration the task can perform.
            time_stay: 600 # 10 min        # (int): maximal duration the animal can stay on a platform.
            seq_start_diff: 2              # (int): start difficulty. (default 2)
            seq_inc_time: 600  # 10 min    # ('None' or int or list[int]): increase the difficulty when over the time spent.
                                           # use `None` to disable this feature.
            seq_inc_perf: 0.8              # (float or list[float]): increase the difficulty when the performance over the criteria
            seq_inc_window: 25             # (int): the at least number of trial for calculate the mean performance
            keyboard_shortcut: FDSAQWER    # (str): shortcut for detection of zone based on the sequence specified.

          trigger :
            - name: Keyboard
            - name: Timer
            - name: Server

    **Devices used**

    `Door` devices named in `D1`, index from 1 to 8, and
    `Food_delivery` devices named in `F1`, index from 1 to 8.

    .. image:: ../image/sequence_task_inc_diff.png

    """

    def __init__(self, action, trigger: Dict[str, Trigger]):
        super().__init__(action, trigger)

        self._seq_start_diff = 2
        self._seq_inc_time: Union[None, int, List[int]] = None
        self._seq_inc_perf: Union[float, List[float]] = 0.8
        self._seq_inc_window = 25
        self._seq_curt_diff = 2

    @property
    def seq_start_diff(self) -> int:
        """start difficulty"""
        return self._seq_start_diff

    @seq_start_diff.setter
    def seq_start_diff(self, value):
        self._seq_start_diff = int(value)
        self.log_emit(f"set start difficulty : {self._seq_start_diff}")

    @property
    def seq_inc_time(self) -> Union[None, int, List[int]]:
        """increase the difficulty when over the time spent"""
        return self._seq_inc_time

    @seq_inc_time.setter
    def seq_inc_time(self, value: Union[None, str, int, List[str], List[int]]):
        if value is None or len(value) == 0:
            self._seq_inc_time = None
            self.log_emit(f"inc difficulty time : (unset)")
        else:
            if isinstance(value, str) and "," in value:
                value = value.split(",")

            if isinstance(value, list):
                self._seq_inc_time = list(map(int, value))
            else:
                self._seq_inc_time = int(value)

            self.log_emit(f"inc difficulty time : {self._seq_inc_time}")

    @property
    def seq_inc_perf(self) -> Union[float, List[float]]:
        """increase the difficulty when the performance over the criteria"""
        return self._seq_inc_perf

    @seq_inc_perf.setter
    def seq_inc_perf(self, value: Union[str, float, List[str], List[float]]):
        if isinstance(value, str) and "," in value:
            value = value.split(",")

        if isinstance(value, list):
            self._seq_inc_perf = list(map(float, value))
        else:
            self._seq_inc_perf = float(value)

        self.log_emit(f"inc difficulty perf : {self._seq_inc_perf}")

    @property
    def seq_inc_window(self) -> int:
        """the at least number of trial for calculate the mean performance"""
        return self._seq_inc_window

    @seq_inc_window.setter
    def seq_inc_window(self, value: int):
        self._seq_inc_window = int(value)

        self.log_emit(f"inc difficulty perf window : {self._seq_inc_window}")

    def get_para(self, para: str = None):
        possible_para = [
            "seq_start_diff",
            "seq_inc_time",
            "seq_inc_perf",
            "seq_inc_window",
        ]
        if para is None:
            ret = super().get_para()
            ret.extend(possible_para)
            return ret
        else:
            return super().get_para(para)

    def new_seq_control(self) -> SequenceControl:
        """Sequence task protocol with incremental difficulty protocol.

        The task difficulty (how many doors open) will increase when the animal's performance reaches certain criteria.

        """
        return IncrementalDifficultySeqControl(
            self._sequence,
            total_arms=8,
            start_difficulty=self.seq_start_diff,
            inc_time_limit=self._seq_inc_time,
            inc_perf_value=self._seq_inc_perf,
            inc_perf_window=self._seq_inc_window,
        )

    def start_task(self):
        super().start_task()
        self._seq_curt_diff = self.seq_start_diff
        self.log_emit(f"start difficulty {self._seq_curt_diff}")

    def give_food(self, visit_arm: int, result: IncDefSeqStepResult):
        if self._seq_curt_diff != result.difficulty:
            self._seq_curt_diff = result.difficulty
            self.set_visit_note("difficulty increase")

        super().give_food(visit_arm, result)


class IncrementalDifficultySeqControl(SequenceControl):
    """Sequence task protocol with incremental difficulty protocol.

    The task difficulty (how many doors open) will increase when the animal's performance reaches certain criteria.

    Attributes
    ----------
    start_difficulty
        the initial difficulty. It determines how many doors open at the beginning of the task.
    inc_time_limit
        increase the difficulty when over the time spent. use `None` to disable this feature.
    inc_perf_value
        increase the difficulty when the performance over the criteria
    inc_perf_window
        the at least number of trial for calculate the mean performance
    """

    def __init__(
        self,
        sequence: List[int],
        total_arms: int = 8,
        start_difficulty=2,
        inc_time_limit: Union[None, float, List[float]] = 15 * 60,
        inc_perf_value: Union[float, List[float]] = 0.8,
        inc_perf_window: int = 25,
    ):
        super().__init__(sequence, total_arms)

        if not (2 <= start_difficulty <= total_arms):
            raise ValueError(
                f"start_difficult = {start_difficulty} not in [2, {total_arms}]"
            )
        if not (0.1 <= inc_perf_value <= 1.0):
            raise ValueError(f"inc_perf_value = {inc_perf_value} not in [0.1, 1.0]")
        if inc_perf_window < 5:
            raise ValueError(f"inc_perf_window = {inc_perf_window} too small")

        self.start_difficulty = start_difficulty
        self.inc_time_limit = inc_time_limit
        self.inc_perf_value = inc_perf_value
        self.inc_perf_window = inc_perf_window

    def reach_criteria(
        self, history: List[bool], difficulty: int, time_spend: float = None
    ) -> bool:
        """Does the performance value reach the criteria?

        Parameters
        ----------
        history: list of bool
            performance history.
        difficulty
            current difficulty
        time_spend: float, optional
            time spent in this difficulty.

        Returns
        -------
        bool
            Does the performance value reach the criteria?

        """
        i = difficulty - self.start_difficulty
        window = (
            self.inc_perf_window[i]
            if isinstance(self.inc_perf_window, list)
            else self.inc_perf_window
        )
        perf = (
            self.inc_perf_value[i]
            if isinstance(self.inc_perf_value, list)
            else self.inc_perf_value
        )

        if self.inc_time_limit is not None:
            time = (
                self.inc_time_limit[i]
                if isinstance(self.inc_time_limit, list)
                else self.inc_time_limit
            )

            if time_spend is not None and time_spend > time:
                return True

        if len(history) < window:
            return False

        c = 0
        for i in range(len(history) - window, len(history)):
            if history[i]:
                c += 1

        return c >= perf * window

    def protocol(self) -> Generator[IncDefSeqStepResult, int, None]:
        """Sequence task protocol with incremental difficulty protocol.

        Returns
        -------
        generator
            generator of SequenceStepResult

        """
        from time import time

        difficulty = self.start_difficulty
        possible = self.sequence[:difficulty]
        history = []

        # init states
        # noinspection PyTypeChecker
        next_visit: int = (
            yield IncDefSeqStepResult(None, None, list(possible), difficulty)
        )
        t_start = time()
        next_arm = None

        index = 0

        while True:
            if not (0 < next_visit <= self.total_arms):
                success = None
            else:
                # exam correct or not
                if next_arm is None:
                    try:
                        index = possible.index(next_visit)
                        success = True
                    except ValueError:
                        success = False
                        index = None
                else:
                    success = next_visit == next_arm

                # update next correct arm
                history.append(success)

                t_spent = time() - t_start
                if self.reach_criteria(history, difficulty, t_spent) and difficulty < len(
                    self.sequence
                ):
                    difficulty += 1
                    possible = self.sequence[:difficulty]
                    history = []
                    t_start = time()

                if success:
                    index = (index + 1) % len(possible)
                    next_arm = possible[index]
                else:
                    try:
                        index = possible.index(next_visit)
                    except ValueError:
                        pass
                    else:
                        index = (index + 1) % len(possible)
                        next_arm = possible[index]

            # noinspection PyTypeChecker
            next_visit = (
                yield IncDefSeqStepResult(success, next_arm, list(possible), difficulty)
            )
