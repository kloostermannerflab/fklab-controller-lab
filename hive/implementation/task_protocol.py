import abc
from typing import Generator
from typing import Iterable
from typing import List
from typing import NamedTuple
from typing import Optional
from typing import Tuple

__all__ = ["SequenceControl"]

VisitSequence = List[int]


class SequenceStepResult:
    """The result during each the visit step.

    Attributes
    ----------

    success: bool, optional
        Indicate this visiting is correct or not. If None, it means this visiting do not
        affect the correctness score.
    next_arm: int, optional
        The next next correct arm which the animal might get the reward.
    possible: list of int
        The next possible arms which the animal can visit. It is used for the task which
        needs to close the future incorrect arms.

    """

    def __init__(
        self, success: Optional[bool], next_arm: Optional[int], possible: List[int]
    ):
        self.success = success
        self.next_arm = next_arm
        self.possible = possible


class SequenceControl(metaclass=abc.ABCMeta):
    """Abstract class for sequence task controlling.

    **Implement node**

    1.  keep the internal state in the protocol function instead of class attributes.

        because not only the Task would use this sequence control, but other function would
        use during the session. We need to keep the state isolated.

    2.  sequence control is not the first object to keep the visiting history.

        Task is the first object to keep the visiting history (not only the arm visiting history).
        The only reason why sequence control keep a visiting history is to infer the future correct arm.

    3.  time depend task

        The sequence control can keep a timer as a variable in the protocol function, which should be
        independent with the Task. If you want the make more complex behavior, you can specialize the
        sequence control and overwrite the type of the generator to Generator[SequenceStepResult, int, None].

    Attributes
    ----------

    sequence: list of int
        list of arm ID. According to the sequence, it means the correct visiting sequence in general.
    total_arms : int
        total used arms, which indicate the maxima number of the arms the animal can visit. It does
        not required the actual number of used arms, it is only used to validate the input.

    """

    def __init__(self, sequence: List[int], total_arms: int):
        if total_arms <= 0:
            raise ValueError(f"negative total_arms value : {total_arms}")

        self.sequence = sequence
        self.total_arms = total_arms

        if len(sequence) == 0:
            raise ValueError("empty seq")

        if total_arms is not None:
            if not all([0 <= it <= total_arms for it in sequence]):
                raise ValueError("Sequence arms upper than total arms")

    @property
    def sequence_str(self) -> str:
        """present sequence in str"""
        return "".join(map(str, self.sequence))

    @abc.abstractmethod
    def protocol(self) -> Generator[SequenceStepResult, int, None]:
        """generate a sequence control generator. You can see LinearTrackSeqControl as example to learn
        how to create a generator.

        **Example to create a protocol**

        .. code-block::

            def protocol(self) -> Generator[SequenceStepResult, int, None]:
                # we use all arms
                # arm 0 is a special case, it indicate that the animal back to the center platform.
                possible = list(range(1, self.total_arms))

                # first, yield the initial state
                visit: int = (yield SequenceStepResult(None, None, possible))

                while True: # during the task
                    # validate the input
                    if not (0 <= visit <= self.total_arms): # not a valid input
                        success = None
                    elif next_visit == 0: # the animal back to center platform
                        success = None
                    else:
                        # handle visit, get the result of success and update the internal states
                        success = ...

                    # yield the current state, and get the next visit
                    visit: int = (yield SequenceStepResult(None, None, possible))

        **Example to use the protocol**

        .. code-block::

            # get a sequence control
            control = LinearTrackSeqControl()

            # create a protocol
            gen = control.protocol()

            # get the initial state
            state = gen.send(None)

            # and update the device according to the initial state
            update_state(state)

            while True: # during the task
                # waiting the animal enter the platform
                visit = wait_for_next_visit()

                # get the visit result
                result = gen.send(visit)

                # give food if the animal made a correct visiting.
                if result.success:
                    # give food to where the animal stand
                    give_food(visit)

                # update the device according to the current state
                update_state(result)

            # end of task. close the generator
            gen.close()

        Parameters
        ----------

        Returns
        -------
        generator
            generator of SequenceStepResult

        References
        ----------
        LinearTrackSeqControl
        DefaultSeqControl
        ErrCorrSeqControl
        IncrementalDifficultySeqControl


        """
        pass

    def foreach_steps(
        self,
        visit: VisitSequence,
        start: int = 0,
        stop: int = None,
        pass_initial_step=False,
    ) -> Iterable[Tuple[int, int, SequenceStepResult]]:
        """go through the visit history.

        **example of using**

        .. code-block::

            # get a sequence control
            control = LinearTrackSeqControl()

            # count how many correct visiting
            success = 0

            for trial, visit, result in control.foreach_steps(visit):
                if result.success:
                    success += 1


        Parameters
        ----------
        visit
            visit history
        start
            from trial
        stop
            stop at trial
        pass_initial_step
            pass initial step to consumer function

        Returns
        -------

        """
        if stop is None:
            stop = len(visit)

        seq = self.protocol()
        try:
            # skip init step
            step = seq.send(None)
            if pass_initial_step:
                yield -1, None, step

            for t in range(min(len(visit), stop)):
                v = int(visit[t])
                step = seq.send(v)
                if start <= t:
                    yield t, v, step
        finally:
            seq.close()


def get_next(
    control: SequenceControl, visit: VisitSequence, index: int = None
) -> Optional[int]:
    """get next/future (index + 1) correct arm

    Parameters
    ----------
    control
        sequence control
    visit
        visit history
    index
        current trial

    Returns
    -------
    arm: int optional

    """
    i = None

    for t, v, s in control.foreach_steps(visit, stop=index):
        i = s.next_arm

    return i


def get_score(
    control: SequenceControl, visit: VisitSequence, start: int = 0, stop: int = None
) -> Tuple[int, int]:
    """count how many correct and incorrect visiting.

    Parameters
    ----------
    control
        sequence control
    visit
        visit history. This history should not contain invalid visit, as well as arm 0.
    start
        start from trial
    stop
        stop at trial

    Returns
    -------
    (success, tail) : tuple of int

    """
    c = 0
    e = 0

    for t, v, s in control.foreach_steps(visit, start=start, stop=stop):
        if s.success:
            c += 1
        else:
            e += 1

    return c, e


def get_binary_score(control: SequenceControl, visit: VisitSequence) -> List[bool]:
    """get the binary score of the visiting history according to the sequence control.

    Parameters
    ----------
    control
        sequence control
    visit
        visit history. This history should not contain invalid visit, as well as arm 0.

    Returns
    -------
    binary_score : list of bool

    """
    ret = []

    for t, v, s in control.foreach_steps(visit):
        ret.append(s.success)

    return ret


def format_visit(
    visit: VisitSequence,
    line_block=25,
    group_block=5,
    visit_time: List[float] = None,
    time_format: str = "%H%M) ",
) -> str:
    """Formatting the visiting history.

    ### example output

    ```
    12:00) 12341 23412 34123 41234 12341
    12:10) 234
    ```

    Parameters
    ----------
    visit
        visiting history
    line_block
        how many trials show in one line
    group_block
        how many trials group together.
    visit_time
        the time stamp of each visiting trials
    time_format
        time formatting string

    Returns
    -------
    formatting result

    """
    import time

    if line_block <= 0:
        raise ValueError("Error during formatting")
    if group_block <= 0:
        raise ValueError("Error during formatting")

    group_block = min(group_block, line_block)

    ret = []

    for i in range(0, len(visit), line_block):
        line = []
        for j in range(i, i + line_block, group_block):
            for k in range(j, min(j + group_block, len(visit))):
                if visit_time is not None and i == j == k:
                    line.append(time.strftime(time_format, time.localtime(visit_time[k])))
                line.append(str(visit[k]))

            line.append(" ")
        ret.append("".join(line).strip())
    return "\n".join(ret)
