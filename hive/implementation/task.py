import time
from abc import abstractmethod
from time import time

import PyQt5.QtCore
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtCore import pyqtSlot

__all__ = ["Task"]


class Task(PyQt5.QtCore.QObject):
    """Base class for automatic mode inherited from the listenCommunication class taking care of receiving event from the maze.

    Parameters
    ----------
    call_action : :obj:`fklab.hive.registry.CallAction`
            Registry class with all call_action methods listed no matter their categories (servor, arduino... etc.)

    triggers : :obj:`Trigger`
            Any class derive from Trigger. It emits the peira_event signal through peira_event (pyqtsignal)
            and will trigger the process_detection method.


    See also
    --------
    Use :`fklab.hive.registry.CallAction.get_action` to call an call_action with the call_action object.
    """

    log = pyqtSignal(str)
    """str:  display log in the view + log file.

    string is send to the view with the emit function.

    .. code-block:: python

        log.emit("this sentence will be display in the app")
    """
    score = pyqtSignal(bool)
    """:obj:`bool`:  update the score - True = success or False = Fail

    .. code-block:: python

        score.emit(False)
    """

    def __init__(self, call_action, triggers):
        PyQt5.QtCore.QObject.__init__(self)

        self._call_action = call_action
        self._sequence = []
        self._max_trial = 0
        self._time_out = 0

        for trigger in triggers:
            signal, state = triggers[trigger].who_to_connect_where()

            fct = getattr(self, state, None)
            if fct:
                signal.connect(fct)

        # user command
        self._user_command = triggers.get("UserCommand", None)

    def setup(self, config):
        """setup this Task from config.

        Parameters
        ----------
        config
            section of Task.options in config file

        """
        pass

    def setup_devices(self, hardware, element_id):
        self._call_action.setup(hardware, element_id)

    @property
    def time_out(self):
        """:obj:`int`: Manage the time where successive events in the same zone are ignored."""
        return self._time_out

    @time_out.setter
    def time_out(self, value):
        self._time_out = int(value)

    @property
    def max_trial(self):
        """:obj:`int`: Manage the maximal number of successive trials.

        Automatically turn off the auto mode when this number is reach.

        .. note:: To enable the feature, set the parameter to "inf" (it will be transformed as None in the setter).
        """
        return self._max_trial

    @max_trial.setter
    def max_trial(self, value):
        if value.isdigit():
            self._max_trial = int(value)
        elif value == "inf":
            self._max_trial = None
        elif isinstance(value, int):
            self._max_trial = value
        else:
            raise (ValueError("max_trial should be either a number or inf."))

    @property
    def count(self):
        """:obj:`dict` of :obj:`int`: Count the score.

        dictionary with two keys : success, fail

        """
        return {"success": 0, "fail": 0}

    @property
    def sequence(self):
        """:obj:`list` of :obj:`list` of 2 :obj`int`: List every successful sequence.

        .. code-block:: python

            sequence =[[start,stop],[start,stop]]

        The list of all used devices is also updated each time.
        """
        return self._sequence

    @sequence.setter
    def sequence(self, sequences):
        self._sequence = sequences

    @property
    def running(self):
        """:obj:`bool`: Give the state of the automatic mode.

        In this task, we only have the manual mode and no automatic mode. It stay set to None to enable this mode.
        """
        return None

    @running.setter
    def running(self, _running):
        pass

    @pyqtSlot(dict)
    def process_user_event(self, command):
        if "command" in command:
            self.process_command(command["command"])
            return

        action = command["action"]
        targets = command["targets"]
        options = command["options"]

        if isinstance(targets, str):
            targets = [targets]

        for target in targets:
            if not options:
                self._call_action.get_action(action, str(target))
            else:
                self._call_action.get_action(action, str(target), *options)

    @pyqtSlot(str)
    def process_event(self, event):
        event_type, event_data = event.split(":")
        self.filter_event(event_type, event_data)

    def filter_event(self, event_type, event_data):
        """filter the incoming event.

        **event type**

        there are common used event types/groups in hive.

        1.  `detection DEVICE`

            detection event from trigger like the keyboard or tracking

        2.  `battery level DEVICE,LEVEL`

            device battery level callback event

        Parameters
        ----------
        event_type
            event type or group
        event_data
            event data

        """
        if "detection" in event_type:
            self.process_detection(event_data.strip())
        elif "battery level" in event_type:
            device_id, percentage = event_data.strip().split(",")
            self.process_low_battery(device_id.strip(), percentage.strip())

    def process_detection(self, device_id):
        pass

    def process_command(self, command):
        """Process the detect keyboard event."""
        self.log.emit(
            "Received command: " + command + " but nothing is implemented in the task."
        )

    def process_low_battery(self, element_id, percentage):
        self.log.emit("Device {}: battery level = {}\n".format(element_id, percentage))

    def trigger_command(self, action, target=None, *options):
        """trigger a user defined command.

        Parameters
        ----------
        action : str
            action/command name which is defined at Command part of the config file.
        target : str or list of str, optional
            a device ID or a list of device IDs of output targets. The device ID could be
            virtual device ID which are defined at Hardware part of the config file.
        options : any
            action options

        """

        self._user_command.trigger_internal_command(
            {"action": action, "targets": target, "options": options}
        )
