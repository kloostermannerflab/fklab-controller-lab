import abc
import logging
import re
import time
from typing import Any
from typing import Callable
from typing import Dict
from typing import Generator
from typing import List
from typing import Optional
from typing import overload
from typing import Tuple
from typing import Union

from ..trigger import Timer
from ..trigger import Trigger
from .task import Task
from .task_protocol import format_visit
from .task_protocol import get_score
from .task_protocol import SequenceControl
from .task_protocol import SequenceStepResult

__all__ = ["AbstractSequenceTask", "DoorControl", "FoodControl"]


class AbstractSequenceTask(Task):
    """Task couple with SequenceControl.

    **Personalize your task**

    You can override some functions to custom your task protocol.

    1.  Setup the task

    - `reset`:  Initiate the state machine. Invoked when the *SEQUENCE AUTO MODE* enable or start command is used.
    - declare which devices you plan to use:
        + use_food_devices
        + use_door_devices

    2.  Start task (workflow defined in `_cmd_start`)

    - `start_task`: setup devices.
    - `new_seq_control`: get  the Sequence Control to use (Protocol definition of the task).

    3.  During task (workflow defined in `visit_arm`)

    - `give_food`: send commands to the devices according the animal visiting result.

    4.  Stop the task (workflow defined in `_cmd_end`)

    - `stop_task`:  print task summary


    **Command line interface to interact with Task.**

    Command are case-insensitive. These commands can be personalised at the task implementation level.

    - `help`:  print all commands
    - `mode`: list current mode list
    - `mode MODE`: set mode to MODE
    - `shortcut`: show keyboard shortcut keyboard
    - `shortcut CHARS`: set keyboard shortcut keyboard, start from index 1. For example, map "QWER" to arm "1234"
    - `get`: list all parameters name
    - `get *`: print all parameter value
    - `get PARA`: get parameter PARA value
    - `set SEQ...`:  set sequence (SEQ should be numbers). For example `set 1 2 3 4`.
    - `set PARA VALUE...`: set parameter PARA with VALUE.
    - `seq`: show sequence used
    - `seq SEQ...`: set sequence (SEQ should be numbers). For example `seq 1 2 3 4`.
    - `visit`:  print visit history.
    - `note`: print visiting note
    - `note TEXT...`: set a TEXT note to current visiting record.
    - `note TRIAL TEXT...`: set a TEXT note to trial TRIAL's visiting record. Use negative value to set last TRIAL's visiting record.
    - `status`: print current task status.
    - `score`: print current correct/incorrect count.
    - `start`: start task
    - `end`: stop task
    - `ARM` (arm number): trigger animal vising event by invoking `visit_arm`. ARM should be a single digit number or
      the character in keyboard shortcut.
    - `+NUM`: temporary increase the max trial limit. It is used when the user trigger animal vising event
      by mistake. By increasing the limit, it prevent the Task from stopping the task when the trial
      number over the original limit.
    - other commands defined in config file.

    **Add new command in your task**

    Define a function whose name starts with '_cmd_' and following the command name.

    .. code-block::

        def _cmd_CUSTOM(self, args:str):
            pass

    **Handle custom event type (when adding new triggers)**

    Event type already handle:

    - detection -> ``process_detection``
    - command -> ``process_command``
    - battery level -> ``process_battery_level``

    .. code-block::

        def filter_event(self, event_type: str, event_data: str):
            if event_type == "your_custom_event_type":
                # put your custom processing here
                pass
            else:
                super.filter_event(event_type, event_data)


    **Add a custom mode in your task**

    .. code-block::

        def get_mode(self) -> List[str]:
            return [your_mode_list, *super().get_mode()]

        def set_mode(self, mode: str = None, verbose=True):
            if mode is None: # print mode status
                self.log_emit("mode " + ",".join(self.get_mode()))

            elif mode == your_mode_name:
                ...

            else:
                super().set_mode(mode, verbose=verbose)

    **Add a custom parameter**

    .. code-block::

        @property
        def your_parameter(self):
            pass

        @your_parameter.setter
        def your_parameter(self, value):
            pass

        def get_para(self, para: str = None):
            if para is None:
                return ["your_parameter", *super().get_para()]
            else:
                return super().get_para(para)


    Attributes
    ----------

    timer : Timer
        timer trigger
    door_control : DoorControl
        door devices control
    food_control : FoodControl
        food devices control
    mode_food : str
        mode of food giving. one of MODE_FOOD_* .
    """

    # task states

    STATE_INIT = "STATE_INIT"
    STATE_RUN = "STATE_RUN"

    @classmethod
    def all_states(cls) -> List[str]:
        ret = []
        for attr in dir(cls):
            if attr.startswith("STATE_") and isinstance(getattr(cls, attr, None), str):
                ret.append(attr)
        return ret

    # task modes

    MODE_FOOD_CONSIST = "food-consist"
    MODE_FOOD_INCREASE = "food-increase"

    @classmethod
    def all_modes(cls) -> List[str]:
        ret = []
        for attr in dir(cls):
            if attr.startswith("MODE_"):
                mode = getattr(cls, attr, None)
                if isinstance(mode, str):
                    ret.append(mode)
        return ret

    # typing annotation for attribute from super class
    _sequence: Optional[List[int]]

    # noinspection PyTypeChecker
    def __init__(self, action, trigger: Dict[str, Trigger]):
        Task.__init__(self, action, trigger)

        self.timer = Timer()
        self.door_control = DoorControl(self)
        self.food_control = FoodControl(self)

        # mode
        self.mode_food = self.MODE_FOOD_CONSIST
        self.mode_visit_arm_outside_sequence = False
        self._routine_battery_check: Optional[int] = 5 * 60

        # internal state
        self._running = False
        self._state = self.STATE_INIT
        self._keyboard_shortcut = None

        # session control
        self._max_trial: Optional[int] = 250
        self._max_trial_inc: Optional[int] = 0
        self._time_limit = 3600  # 1 hr
        self._time_out = 600  # 10 min
        self._no_detection_period = 10  # sec
        self._previous_visit_time = 0

        # task control
        self._control: SequenceControl = None
        self._control_gen: Generator[SequenceStepResult, int, None] = None
        self._control_step: SequenceStepResult = None
        self._control_prev: SequenceStepResult = None

        # session data
        self._visit: List[int] = []
        self._visit_time: List[float] = []
        self._visit_note: Dict[int, str] = {}

    # property getter and setter

    @property
    def state(self) -> str:
        """current state of this Task. one of STATE_* ."""
        return self._state

    @property
    def current_trial(self) -> int:
        return len(self._visit)

    @property
    def visit_history(self) -> List[int]:
        """the animal visiting history"""
        return self._visit

    @property
    def visit_timestamp(self) -> List[float]:
        """the animal visiting timestamps"""
        return self._visit_time

    @property
    def visit_notes(self) -> Dict[int, str]:
        """visiting record note

        Returns
        -------
        dict
            trial number map to note text

        """
        return self._visit_notes

    def set_visit_note(self, message: str, trial: int = None, replace=False):
        """set message to visiting record note.

        Parameters
        ----------
        message
            message text
        trial
            trial number, None for current trial
        replace
            replace the text. Otherwise, append to previous text.

        """
        if trial is None:
            trial = self.current_trial

        prev = self._visit_note.get(trial)

        if prev is None or replace:
            self._visit_note[trial] = message
            self.log_emit(f"note {trial} = {message}")
        else:
            self._visit_note[trial] = f"{prev}; {message}"
            self.log_emit(f"note {trial} += {message}")

    # parameter property

    @property
    def max_trial(self) -> int:
        """maximal number the animal can visit.

        Once the animal over this value, Task will invoke 'END' command.
        """
        return self._max_trial

    @max_trial.setter
    def max_trial(self, value):
        if value == "inf":
            self._max_trial = None
            self.log_emit("set max trial = inf")
        else:
            self._max_trial = int(value)
            self.log_emit(f"set max trial = {self._max_trial:d}")

    @property
    def time_out(self) -> int:
        """maximal duration the task can perform.

        Once the animal over this value, Task will invoke 'END' command.
        """
        return self._time_limit

    @time_out.setter
    def time_out(self, value):
        self._time_limit = int(value)
        self.log_emit(f"set time out = {self._get_time_str(self._time_limit)}")

    @property
    def time_stay(self) -> int:
        """maximal duration the animal can stay on a platform.

        Once the animal over this value, Task send a message to log window, let user decide what to do next.
        """
        return self._time_out

    @time_stay.setter
    def time_stay(self, value):
        self._time_out = int(value)
        self.log_emit(f"set time stay = {self._get_time_str(self._time_out)}")

    @property
    def no_detection_period(self) -> int:
        """Lock-out period to avoid redundant/successive detections
        """
        return self._no_detection_period

    @no_detection_period.setter
    def no_detection_period(self, value):
        self._no_detection_period = int(value)
        self.log_emit(f"set no detection period = {value}")

    @property
    def running(self) -> bool:
        return self._running

    @running.setter
    def running(self, _running: bool):
        self._running = _running

        if _running:
            self.reset()

    @property
    def sequence(self) -> Optional[List[int]]:
        """the sequence used in this Task"""
        return self._sequence

    @sequence.setter
    def sequence(self, sequences: Optional[List[int]]):
        if sequences is None or len(sequences) == 0:
            self._sequence = None
            self.log_emit("Sequence : (unset)")
        else:
            self._sequence = sequences
            self.log_emit(f"Sequence : {str(sequences)}")

    # task relate property

    @property
    def count(self):
        """the score of the visiting so far"""
        s, f = get_score(self._control, self._visit)
        return {"success": s, "fail": f}

    @property
    def task_start(self) -> bool:
        """Does task start?"""
        return self._state == self.STATE_RUN

    @property
    def task_start_time(self) -> Optional[float]:
        """Time stamp of the task start"""
        try:
            return self.timer["time_limit"].start_time
        except KeyError:
            return None

    @property
    def task_stop_time(self) -> Optional[float]:
        """Time stamp of the previous task stop.

        None if the task still running.
        """
        try:
            return self.timer["time_limit"].stop_time
        except KeyError:
            return None

    @property
    def task_duration(self) -> Optional[float]:
        """Time duration from the task start"""
        try:
            return self.timer["time_limit"].duration
        except KeyError:
            return None

    @property
    def stay_duration(self) -> Optional[float]:
        """Time duration from the animal last visiting"""
        try:
            return self.timer["time_out"].duration
        except KeyError:
            return None

    # device property

    @property
    def keyboard_shortcut(self) -> Optional[str]:
        """current keyboard layout used"""
        return self._keyboard_shortcut

    @keyboard_shortcut.setter
    def keyboard_shortcut(self, value: str):
        self._keyboard_shortcut = value.lower()

    @property
    def use_food_devices(self) -> List[str]:
        """declare which food devices plan to use.

        It is used to reset the FoodControl.
        By default, Task only use the food devices which is related to the sequence.

        Returns
        -------
        name list
            food devices plan to use

        """
        if self._sequence is None:
            return []

        return [f"F{i}" for i in self._sequence]

    @property
    def use_door_devices(self) -> List[str]:
        """declare which door devices plan to use.

        It is used to reset the DoorControl.
        By default, Task only use the food devices which is related to the sequence.

        Returns
        -------
        name list
            door devices plan to use

        """
        if self._sequence is None:
            return []

        return [f"D{i}" for i in self._sequence]

    @property
    def door_forbidden_period(self) -> int:
        """the period which the door device reject the coming commands when it is perform the action. """
        return self.door_control.DOOR_FORBIDDEN_PERIOD

    @door_forbidden_period.setter
    def door_forbidden_period(self, value):
        self.door_control.DOOR_FORBIDDEN_PERIOD = int(value)

    @property
    def food_basic_mount(self) -> int:
        """The basic/minima mount of food giving. The value represent the food delivery time. """
        return self.food_control.RESET_DELIVERY_TIME

    @food_basic_mount.setter
    def food_basic_mount(self, value: int):
        self.food_control.RESET_DELIVERY_TIME = value

    @property
    def food_inc_mount(self) -> int:
        """The increase mount of food giving when the animal made the correct visiting continuously.
        The value represent the food delivery time.
        """
        return self.food_control.INC_DELIVERY_TIME

    @food_inc_mount.setter
    def food_inc_mount(self, value: int):
        self.food_control.INC_DELIVERY_TIME = value

    @property
    def food_max_mount(self) -> int:
        """The maxima mount of food giving. The value represent the food delivery time."""
        return self.food_control.MAX_DELIVERY_TIME

    @food_max_mount.setter
    def food_max_mount(self, value: int):
        self.food_control.MAX_DELIVERY_TIME = value

    @property
    def routine_battery_check(self) -> Optional[int]:
        """interval of devices battery level checking routine"""
        return self._routine_battery_check

    @routine_battery_check.setter
    def routine_battery_check(self, value: Optional[int]):
        """set interval of devices battery level checking routine.

        Every time setting will restart/cancel the routine.

        Parameters
        ----------
        value
            seconds, None for disabling the routine.

        """
        if value is None:
            self._routine_battery_check = None
        else:
            self._routine_battery_check = int(value)

        if self._routine_battery_check is None:
            try:
                self.timer["battery_check"].cancel(force=True)
            except KeyError:
                pass
        else:
            # get device battery information from server regularly.
            # It not only work for this, but also keep the connection with server,
            # and try to avoid any network connection problem caused by no interact for a long time.
            h = self.timer.add_timer(
                "battery_check", self._routine_battery_check, "continue", replace=True
            )
            h.start()

    # override super function

    def setup(self, config: Dict[str, Any]):
        for k, v in config.items():
            if isinstance(v, str):
                v = v.lower()
            self.set_para(k.lower(), v)

        # log MODE
        self._cmd_mode()

    def filter_event(self, event_type: str, event_data: str):
        logging.debug("%s %s", event_type, event_data)

        if event_type in ("command", "detection") and self._state is self.STATE_RUN:
            event_data = event_data.strip()

            try:
                self.process_detection(event_data)
            except BaseException as e:
                logging.debug(f"ERROR %s %s", event_type, event_data, exc_info=e)
                self.log_emit(f"ERROR {event_type} {event_data}: {str(e)}")

        elif event_type == "battery level":
            device_id, percentage = event_data.strip().split(",")
            self.process_low_battery(device_id.strip(), percentage.strip())

        elif event_type == "timer":
            if event_data == "time_limit":
                self._cmd_end()
            elif event_data == "time_out":
                self.set_visit_note(
                    f"Animal stay timeout (time: {self._get_time_str(self.task_duration)})"
                )
            elif event_data == "battery_check":
                devices = [*self.use_door_devices, *self.use_food_devices]
                if len(devices):
                    self.trigger_command("info", devices)

    # override super event handle function

    def process_low_battery(self, element_id: str, percentage: str):
        self.log_emit(f"Device {element_id}: battery level = {percentage}")
        # XXX record the message to visiting note when percentage = 0%
        #   but I don't known what format the percentage is.

    def process_detection(self, device_id):

        if device_id in "0123456789":  # visiting event
            self.visit_arm(int(device_id))
        elif (  # device shortcut
            self._keyboard_shortcut is not None and device_id in self._keyboard_shortcut
        ):
            self.visit_arm(self._keyboard_shortcut.index(device_id) + 1)

    def process_command(self, command: str):
        """Process the detect keyboard event."""
        cmd, *args = re.split(r"\s+", command)

        # use lower command to find the command.
        # and also keep the original user input for error message
        cmd_low: str = cmd.lower()

        # find command function
        func = self.get_command(cmd_low)
        if func is not None:
            # try:
            func(*args)
            # except BaseException as e:
            #    self.log_emit(f"command {cmd} : {e}")

        elif cmd_low.startswith("+"):  # command for increasing the trial limit temporary
            try:
                value = int(cmd_low[1:])
            except ValueError:
                pass
            else:
                if self._max_trial_inc is not None:
                    self._max_trial_inc += value
                    self.log_emit(
                        f"change max_trial to {self._max_trial + self._max_trial_inc}"
                    )

    # commands

    def all_commands(self) -> List[Tuple[str, Callable[..., None]]]:
        """list all support commands

        Returns
        -------
        list of (command name, command callable)

        """
        ret = []
        for attr in dir(self):
            if attr.startswith("_cmd_"):
                func = getattr(self, attr)
                if callable(func):
                    ret.append((attr[5:], func))
        return ret

    def get_command(self, command: str) -> Optional[Callable[..., None]]:
        """get command callable/function.

        Parameters
        ----------
        command
            command name

        Returns
        -------
        callable
            None if not found.

        """
        command = command.lower()
        func = getattr(self, f"_cmd_{command}", None)
        if callable(func):
            return func
        return None

    def invoke_command(self, command: str, *args: str):
        """invoke command.

        Parameters
        ----------
        command
            command name
        args
            command arguments

        Returns
        -------
        command callable return

        """
        func = self.get_command(command)
        if func is None:
            raise ValueError(f"command {command} not found")
        else:
            return func(*args)

    def _cmd_help(self):
        """print help"""

        import inspect

        # get command name, parameters and the documents
        doc: List[Tuple[str, str, str]] = []
        for name, func in self.all_commands():
            sig = inspect.signature(func)
            text = [name.upper()]
            for i, p in enumerate(sig.parameters):
                text.append(f"<{p}>")

            desp = func_doc_brief(func)

            doc.append((name, " ".join(text), desp))

        # build help content
        line = ["Commands:"]
        max_name_len = max([len(it[1]) for it in doc])
        format_head = f"  %-{max_name_len}s %s %s"

        for i in range(len(doc)):
            name, text, desp = doc[i]
            desp = desp.split("\n")
            line.append(format_head % (text, ":", desp[0]))

            for _desp in desp[1:]:
                line.append(format_head % (" ", " ", _desp))

            # special case for command GET, list all possible parameters
            if name == "GET":
                line.append(format_head % (" ", " ", "Available parameters : "))
                for para in self.get_para():
                    prop = getattr(type(self), para, None)
                    if isinstance(prop, property):
                        desp = func_doc_brief(prop.fget)
                    else:
                        desp = None

                    text = f"  %-16s" % para
                    if desp is not None:
                        text += " : " + desp

                    line.append(format_head % (" ", " ", text))

        # other commands which is not part of command function (_cmd_*) family.
        line.append(format_head % ("<num>", ":", "visiting arm"))
        line.append(format_head % ("+<num>", ":", "increase max trials"))
        line.append(format_head % ("<cmd>", ":", "use defined command"))

        self.log_emit("\n".join(line))

    def _cmd_mode(self, *mode: str):
        """show/set task mode, use '?' to list all mode value"""
        if len(mode) == 0:  # print mode list
            self.set_mode(None, True)

        elif len(mode) == 1:  # set mode value
            if mode[0] == "?":
                self.log_emit(str(self.all_modes()))
            elif self._state is self.STATE_INIT:
                self.set_mode(mode[0].lower())
            else:
                self.log_emit(f"WARN: cannot change setting during task")
        else:
            self.log_emit("WARN: MODE <mode>")

    def _cmd_shortcut(self, keys: str = None):
        """show/set keyboard shortcut keyboard"""
        if keys is None:  # print shortcut
            self.log_emit(f"keyboard shortcut : {keys}")

        else:  # set print shortcut
            self._keyboard_shortcut = keys
            self.log_emit(f"set keyboard shortcut : {keys}")

    def _cmd_get(self, *argv: str):
        """get parameter list or its value (use '*' for all parameters)"""
        if len(argv) == 0:  # print all parameters name
            self.log_emit(f"possible : {self.get_para()}")

        elif (  # print all parameters name with its value
            len(argv) == 1 and argv[0] == "*"
        ):
            for para in self.get_para():
                self.log_emit(f"{para} = {self.get_para(para)}")

        else:  # print parameters value
            for para in argv:
                para = para.lower()
                self.log_emit(f"{para} = {self.get_para(para)}")

    def _cmd_set(self, *argv: str):
        """set sequence or parameter"""
        if len(argv) == 0:  # error
            self.log_emit(f"need a sequence, or one of {self.get_para()}")
        else:
            try:
                int(argv[0])
                # set sequence
                self._cmd_seq(*argv)
            except ValueError:
                # set parameter
                argv = list(map(str.lower, argv))
                if len(argv) == 1:
                    self.set_para(argv[0], None)
                elif len(argv) == 2:
                    self.set_para(argv[0], argv[1])
                else:
                    self.set_para(argv[0], argv[1:])

    def _cmd_seq(self, *seq: str):
        """show/set sequence"""
        if len(seq) == 0:
            # trigger logging
            self.sequence = self.sequence
        else:
            if self._state is self.STATE_INIT:
                self.sequence = list(map(int, seq))
            else:
                self.log_emit(f"WARN: cannot change setting during task")

    def _cmd_visit(self):
        """show visited arms"""
        _max_trial = str(self._max_trial) if self._max_trial is not None else "Inf"
        time_format = "%H%M "
        text = [
            f"Visited : {len(self._visit):d}, limit: {_max_trial}",
            format_visit(
                self._visit, visit_time=self._visit_time, time_format=time_format
            ),
        ]

        # current time or task stop time
        start_time = self.task_start_time
        stop_time = self.task_stop_time

        if stop_time is not None:
            text.append(time.strftime(time_format, time.localtime(stop_time)))
        elif len(self._visit) > 0:
            text.append(time.strftime(time_format, time.localtime(self._visit_time[-1])))
        elif start_time is not None:
            text.append(time.strftime(time_format, time.localtime(start_time)))
        else:
            text.append("????")

        # visiting note
        for trial in range(len(self._visit)):
            note = self._visit_note.get(trial + 1)
            if note is not None:
                text.append(f"# {trial + 1} {note}")

        self.log_emit("\n".join(text))

    def _cmd_note(self, *msg):
        """print set visiting note

        Parameters
        ----------
        msg
            note message. The first argument can be a number to indicate which
            trial's visiting record needs to be set.

        """
        if self.task_start and len(msg) > 0:  # set visiting note
            try:
                trial = int(msg[0])
            except ValueError:
                # set note to current visiting record
                self.set_visit_note(" ".join(msg))
            else:
                if trial < 0:
                    self.set_visit_note(" ".join(msg[1:]), self.current_trial + trial)
                else:
                    self.set_visit_note(" ".join(msg[1:]), trial)

        else:  # print visiting note
            ret = []
            for trial in sorted(self._visit_note):
                ret.append(f"# {trial} {self._visit_note[trial]}")
            self.log_emit("\n".join(ret))

    def _cmd_status(self):
        """show task status to log window"""
        if self._state is self.STATE_INIT:
            self.log_emit("Task not start")
        else:
            # the order of the message is reversed.

            # 3. print score
            self._cmd_score()

            # 2. information of visiting history and next expect arm
            if self._control_step is not None:
                next_expect = (
                    str(self._control_step.next_arm)
                    if isinstance(self._control_step.next_arm, int)
                    else "ANY"
                )
            else:
                next_expect = "??"

            self.log_emit(
                f"Visited trial : {len(self._visit)}, next expect : {next_expect}"
            )

            # 1. task time information
            task_duration = self._get_time_str(self.task_duration)

            stay_duration = self.stay_duration
            if stay_duration is None:
                stay_duration = "00:00"
            else:
                stay_duration = self._get_time_str(stay_duration)

            self.log_emit(
                f"Start duration : {task_duration}, Stay duration : {stay_duration}"
            )

    def _cmd_score(self):
        """ print current correct/incorrect count."""
        if self._control is not None:
            s, f = get_score(self._control, self._visit)
            self.log_emit(f"Success: {s}, Fail: {f}")

    def _cmd_start(self):
        """start task"""
        if self._state is not self.STATE_INIT:
            self.log_emit("task has started")
            return

        if self._sequence is None or len(self._sequence) == 0:
            self.log_emit("Sequence : (not set)")
            return

        self.log_emit("Start task")
        self._state = self.STATE_RUN
        self._max_trial_inc = 0
        self._previous_visit_time = 0

        # reset visiting records
        self._visit.clear()
        self._visit_time.clear()
        self._visit_note.clear()

        try:
            # setup devices
            self.start_task()

            # setup sequence control
            self._control = self.new_seq_control()
            self._control_gen = self._control.protocol()
            self._control_step = None
            self._control_prev = None
        except BaseException:
            # fail to start task
            self._state = self.STATE_INIT
            raise
        # start timer
        self.timer.add_timer("time_limit", self._time_limit, replace=True).start()
        self.timer.add_timer("time_out", self._time_out, replace=True)

        # reassign to trigger battery checking routine
        self.routine_battery_check = self.routine_battery_check

        # set initial state
        self._control_step: SequenceStepResult = self._control_gen.send(None)
        if self._control_step.possible:
            self.door_control.set_door([f"D{d}" for d in self._control_step.possible])

    # noinspection PyPep8Naming
    def _cmd_end(self):
        """end task"""
        if self._state is not self.STATE_RUN:
            self.log_emit("task not start")
            return

        self.log_emit("Stop task")
        self._state = self.STATE_INIT

        if self._control_gen is not None:
            self._control_gen.close()

        # close all door devices
        self.door_control.set_door(None, False)

        # empty all food devices
        self.food_control.empty()

        # stop timer
        self.timer["time_limit"].cancel(force=True)
        self.timer["time_out"].cancel(force=True)

        self.stop_task()

    # abstract/predefine function

    def reset(self):
        """Initiate the state machine."""
        logging.info("reset")
        self.sequence = None
        self._state = self.STATE_INIT

    def get_mode(self) -> List[str]:
        """get current modes list"""
        return [self.mode_food]

    def set_mode(self, mode: str = None, verbose=True):
        """print mode list or set mode"""
        if mode is None:
            self.log_emit(f"mode : {self.mode_food}")

        elif mode == self.MODE_FOOD_CONSIST:
            self.mode_food = self.MODE_FOOD_CONSIST
            if verbose:
                self.log_emit(f"set mode : {self.mode_food}")

        elif mode == self.MODE_FOOD_INCREASE:
            self.mode_food = self.MODE_FOOD_INCREASE
            if verbose:
                self.log_emit(f"set mode : {self.mode_food}")

        else:
            self.log_emit(f"WARN: unknown mode : {mode}")

    @overload
    def get_para(self) -> List[str]:
        pass

    @overload
    def get_para(self, para: str) -> Any:
        pass

    def get_para(self, para: str = None):
        """get supported parameters (para is None) or parameter value"""

        possible_para = [
            "max_trial",
            "time_out",
            "time_stay",
            "mode",
            "keyboard_shortcut",
            "door_forbidden_period",
            "food_basic_mount",
            "food_inc_mount",
            "food_max_mount",
            "routine_battery_check",
            "no_detection_period",
        ]

        if para is None:
            return possible_para
        elif para == "mode":
            return self.get_mode()
        elif para in self.get_para():
            return getattr(self, para)
        else:
            self.log_emit(f"unknown parameter {para}")

    def set_para(self, para: str, value: Any = None):
        """set parameter value"""
        if para == "mode":
            if value is not None:
                if isinstance(value, str):
                    value = value.split(",")

                if not isinstance(value, list):
                    self.log_emit(f"mode not string or list : {value}")
                else:
                    for mode in value:
                        self.set_mode(mode.lower(), verbose=False)

        elif para in self.get_para():
            try:
                setattr(self, para, value)
            except (ValueError, TypeError) as e:
                self.log_emit(f"{para} = {value} : {e}")

        else:
            self.log_emit(f"unknown parameter {para}")

    def start_task(self):
        """invoked when the task start. this function is used to reset the door/food control."""
        # reset device control
        self.door_control.reset(self.use_door_devices)
        self.food_control.reset(self.use_food_devices)

    @abc.abstractmethod
    def new_seq_control(self) -> SequenceControl:
        """get the sequence control."""
        pass

    def stop_task(self):
        """invoked when the task stop."""
        self.show_task_summary()

    def visit_arm(self, visit_arm: int):
        """process animal visiting event.

        Parameters
        ----------
        visit_arm : int
            where the animal visiting

        """
        if not self.mode_visit_arm_outside_sequence and visit_arm not in self._sequence:
            # user input mistake? It is quiet often happen.
            return

        current_time = time.time()

        if len(self._visit) > 0 and self.no_detection_period is not None:
            previous_visit = self._visit[-1]
            if previous_visit == visit_arm and (
                current_time - self._previous_visit_time < int(self.no_detection_period)
            ):
                # multi detection events on same location in a short period
                return

        self._previous_visit_time = current_time

        if self.timer is not None:
            self.timer["time_out"].start(force=True)

        # next step
        self._control_prev = self._control_step
        self._control_step = self._control_gen.send(visit_arm)

        # do not record center arm into visit history.
        if visit_arm > 0:
            self._visit.append(visit_arm)
            self._visit_time.append(time.time())

        # device control
        self.give_food(visit_arm, self._control_step)

        self.logging_result_visit_arm(visit_arm)

        # check max trials. Does not the animal visited over max trials?
        if self._check_trials():
            self._cmd_end()

    def logging_result_visit_arm(self, visit_arm):
        # logging
        if visit_arm > 0:
            if self._control_step.success is None:
                correct = "pass"
            elif self._control_step.success:
                correct = "correct"
            else:
                correct = "incorrect"

            self.log_emit(
                f"animal visits[{len(self._visit) - self._max_trial_inc}+{self._max_trial_inc}] = {visit_arm} "
                f"(time: {(self._get_time_str(self.task_duration))}) "
                f"({correct})"
            )

    def give_food(self, visit_arm: int, result: SequenceStepResult):
        """invoked when the animal visit the arm.

        This function purpose to send commands to food/door devices.

        Parameters
        ----------
        visit_arm
            where the animal visiting
        result
            the result of this visiting
        """
        # food control
        if result.success:
            food_device = f"F{visit_arm}"
            if self.mode_food == self.MODE_FOOD_CONSIST:
                self.food_control.give_food(food_device, reset=True)
            elif self.mode_food == self.MODE_FOOD_INCREASE:
                if self._control_prev is not None and self._control_prev.success is True:
                    self.food_control.give_food(food_device, increase_mount=True)
                else:
                    self.food_control.give_food(food_device, reset=True)

        # door control
        if getattr(self._control, "with_door_close", False):
            self.door_control.set_door(None, False, update=True, key_action=True)

        self.door_control.set_door([f"D{d}" for d in self._control_step.possible])

    def show_task_summary(self):
        """show task information to log window """
        self._cmd_mode()

        if self._max_trial is None:
            max_trial_str = "inf"
        else:
            max_trial_str = f"{self._max_trial} (+{self._max_trial_inc})"

        self.log_emit(
            f"max trials : {max_trial_str}, "
            f"time out : {self._get_time_str(self._time_out)}, "
            f"time limit : {self._get_time_str(self._time_limit)}"
        )

        # trigger logging
        self.sequence = self._sequence

        #
        start_time = self.task_start_time
        if start_time is not None:
            start_time = time.strftime("%H%M", time.localtime(start_time))
        else:
            start_time = "???"

        stop_time = self.task_stop_time
        if stop_time is not None:
            stop_time = time.strftime("%H%M", time.localtime(stop_time))
        else:
            stop_time = "???"

        self.log_emit(
            f"Duration : {self._get_time_str(self.task_duration)}, time {start_time} ~ {stop_time}"
        )

        self._cmd_visit()
        self._cmd_score()

    # help function

    def log_emit(self, message: str):
        """logging. type safe version for self.log.emit"""
        logging.info(message)
        self.log.emit(message)

    def _check_trials(self):
        x = self._max_trial
        return x is not None and len(self._visit) >= x + self._max_trial_inc

    def _get_time_str(self, duration: float = None) -> str:
        """formatting time duration.

        Parameters
        ----------
        duration : float
            current time. If None, use stop time or current time according whether task is ended.

        Returns
        -------
        str
            duration expression in format MM:SS

        """
        if duration is None:
            duration = time.time() - self.task_start_time

        duration = int(duration)
        return f"{(duration // 60):02d}:{(duration % 60):02d}"


class FoodControl:
    """food devices control. """

    RESET_DELIVERY_TIME = 150
    INC_DELIVERY_TIME = 50
    MAX_DELIVERY_TIME = 400

    def __init__(self, task: Task, increase_mode=False):
        self._task = task
        self._food: Dict[str, int] = {}  # food delivery time
        self._delivery_time = self.RESET_DELIVERY_TIME
        self.increase_mode = increase_mode

    @property
    def devices(self) -> List[str]:
        """IDs of used food devices"""
        return list(self._food)

    def reset(self, use_device: List[str]):
        """reset food device list

        Parameters
        ----------
        use_device
            food device name list.

        """
        self._food = {device: self.RESET_DELIVERY_TIME for device in use_device}
        self._delivery_time = self.RESET_DELIVERY_TIME

        # reset delivery time
        for device in self._food:
            self.change_delivery_time(device, self.RESET_DELIVERY_TIME, force=True)

    def empty(self, device: str = None):
        """empty food device

        Parameters
        ----------
        device
            device ID, None for all

        """
        if device is None:
            for device in self._food:
                self.empty(device)
            return

        self._task.trigger_command("empty", device)

    def fill(self, device: str = None):
        """fill food device

        Parameters
        ----------
        device
            device ID, None for all

        """
        if device is None:
            for device in self._food:
                self.fill(device)
            return

        self._task.trigger_command("fill", device)

    def read(self, device: str = None):
        """read parameter from food device

        Parameters
        ----------
        device
            device ID, None for all

        """
        if device is None:
            for device in self._food:
                self.read(device)
            return

        self._task.trigger_command("read", device)

    def give_food(
        self, device: str, *, delivery: int = None, increase_mount=False, reset=False
    ):
        """dispense food.

        Parameters
        ----------
        device
            device id
        delivery
            change change delivery time temporarily
        increase_mount
            increase delivery time
        reset
            reset delivery time

        """
        if delivery is None:
            if reset:
                delivery = self.RESET_DELIVERY_TIME
            elif increase_mount:
                delivery = min(
                    self.MAX_DELIVERY_TIME, self._delivery_time + self.INC_DELIVERY_TIME
                )
            else:
                # do not change
                delivery = self._delivery_time

            self._delivery_time = delivery

        self.change_delivery_time(device, delivery)
        self._task.trigger_command("dispense", device)

    def change_delivery_time(self, device: str, delivery: int, force=False):
        """change the food delivery for food device

        This control will cache the delivery setting and only update the device
        when necessary.

        Parameters
        ----------
        device
            device id, None for all
        delivery
            new delivery time.
        force
            force update.

        """
        if device is None:
            for device in self._food:
                self.read(device)
            return

        current_delivery = self._food[device]
        if force or current_delivery != delivery:
            self._food[device] = delivery
            self._task.trigger_command("delivery_setting", device, "all", delivery)


class DoorControl:
    """door devices control"""

    # door forbidden period
    DOOR_FORBIDDEN_PERIOD = 4

    def __init__(self, task: AbstractSequenceTask):
        self._task = task
        self._timer = Timer().add_timer(
            "door-control", 1, "continue", replace=True, callback=self._callback
        )
        self._door: Dict[str, Tuple[bool, float]] = {}  # door open or not
        self._queue: List[Dict[str, bool]] = [{}]

    @property
    def devices(self) -> List[str]:
        """IDs of used door devices"""
        return list(self._door)

    def reset(self, use_device: List[str]):
        """reset door device list

        Parameters
        ----------
        use_device
            door device name list

        """
        self._door = {device: (False, 0.0) for device in use_device}
        self._queue = [{}]
        self._timer.cancel(force=True)

    # noinspection PyUnusedLocal
    def _callback(self, event_name: str, counter: int) -> bool:
        self.dequeue()

        return self._task.task_start or not self.is_empty()

    def is_empty(self) -> bool:
        """is command pending queue empty?"""
        return len(self._queue) == 1 and len(self._queue[0]) == 0

    def set_door(
        self,
        device: Union[None, str, List[str]],
        state=False,
        *,
        update=False,
        key_action=False,
    ):
        """set door state.

        Parameters
        ----------
        device
            door device. could be the device ID, list of device ID,
            or None for all
        state
            door open or close
        update
            update the last change time, no matter the state of the door had been changed
        key_action
            does this setting a key action? which means this action must be done and
            are unable to be overwrote by following commands.

        """
        if not self._timer.is_started:
            self._timer.start()

        if device is None:
            if state:
                door_open = list(self._door)
            else:
                door_open = []
        elif isinstance(device, str):
            return self._door_control(device, state, update=update)
        elif isinstance(device, list):
            door_open = device
        else:
            raise TypeError()

        for door in self._door:
            self._door_control(door, door in door_open, update=update)

        if key_action:
            self._queue.append({})

    def _door_control(self, device: str, state: bool, *, update=False):
        """change door state. If the device is acting and during the forbidden period, the command
        will be put into command pending queue."""
        try:
            # print("open" if state else "close", device)
            door_state, last_update = self._door[device]
        except KeyError as e:
            logging.warning("door control", exc_info=e)
            return
        else:
            t = time.time()

            if self._is_door_enqueue(device):
                # always pending device which has already in the waiting queue
                self._queue[-1][device] = state

            elif door_state != state or update:
                if t - last_update < self.DOOR_FORBIDDEN_PERIOD:
                    # If open and close command came less then DOOR_FORBIDDEN_PERIOD,
                    # server will raise FORBIDDEN warning and the second command will be ignored.
                    # Therefore we wait a period of time and then do it again.
                    # However, we cannot sleep here because it will suspend current thread and
                    # block the following command, so we put the command into a queue and
                    # use routine event to check the queue.
                    self._queue[-1][device] = state
                else:
                    self._door[device] = (state, t)

                    if door_state != state:
                        self._door_send_command(device, state)

    def _is_door_enqueue(self, device: str) -> bool:
        for q in self._queue:
            if device in q:
                return True
        return False

    def dequeue(self):
        """consume pending door commands. called by _check_routine"""

        # print("dequeue", self._queue)

        if len(self._queue) == 0:
            self._queue.append({})
        else:
            t = time.time()
            queue = self._queue[0]
            devices = list(queue)

            for device in devices:
                state = queue[device]
                door_state, last_update = self._door[device]

                if t - last_update < self.DOOR_FORBIDDEN_PERIOD:
                    continue

                del queue[device]

                self._door[device] = (state, t)
                if door_state != state:
                    self._door_send_command(device, state)

            if len(queue) == 0 and len(self._queue) > 1:
                del self._queue[0]

    def _door_send_command(self, device: str, state: bool):
        """send command to door directly."""
        if state:
            self._task.trigger_command("open_door", device)
        else:
            self._task.trigger_command("close_door", device)


def func_doc_brief(func) -> str:
    desp = func.__doc__
    if desp is not None:
        desp = desp.split("\n")
        while len(desp[0]) == 0:
            del desp[0]

        if len(desp) == 0:
            desp = None
        else:
            desp = desp[0]

    if desp is None:
        desp = func.__name__

    return desp
