import logging
from time import time

import serial

__all__ = ["PeiraDevices", "ArduinoDevices"]


class HardwareFailureError(Exception):
    def __init__(self, hdw_type, message):
        self.hdw_type = hdw_type
        super().__init__(message)


class ArduinoDevices:
    """List all arduino devices with for element id the connexion port.

    Device type: Pulse

    """

    def __init__(self):
        self._activate_stim = False
        self.arduinos = {}

    def pulse_setup(self, element_id):
        logging.info(f"Activate communication with the arduino on port {element_id}")
        self.arduinos[element_id] = serial.Serial(
            port=element_id, baudrate=11200, timeout=0.1
        )

    def start_pulse(self, element_id):
        if self._activate_stim:
            logging.info(f"Stimulation already activated on arduino {element_id}.")
            return

        self._activate_stim = True
        self.arduinos[element_id].write(bytes("d", "utf-8"))
        logging.info(f"start sending disruption on arduino {element_id}.")

    def stop_pulse(self, element_id):
        if not self._activate_stim:
            logging.info(f"Stimulation already deactivated on arduino {element_id}.")
            return

        self._activate_stim = False

        self.arduinos[element_id].write(bytes("s", "utf-8"))
        logging.info(f"stop sending disruption  on arduino {element_id}.")


class PeiraDevices:
    """List every existing call_action and the message to send to the server implemented by Peira.

    Device type : Door, Food delivery system

    Attributes
    ----------
    communication : :obj:`Communication`
            The communication class is taking care of connecting, sending and receiving information.
    """

    def __init__(self, communication):
        self.communication = communication

    def set_user(self, username):
        self.communication.user = username

    def door_setup(self, element_id):
        """Claim, check the battery level, and home the door device.

        Parameters
        ----------
        element_id : :obj:`int`
                where to send the command

        Returns
        -------
        message: :obj:`str`
            log messages
        """
        self._setup(element_id)
        self.home(element_id)

    def food_delivery_setup(self, element_id):
        """Claim, check information in the food delivery device.

        Information :

        - the battery level
        - the activated delivery mode
        - the food delivery time in this mode
        - the interval between delivery food
        - the sensor peira_event level.

        Parameters
        ----------
        element_id : :obj:`int`
                where to send the command

        Returns
        -------
        message: :obj:`str`
            log messages
        """
        mode = self._setup(element_id)
        self.read(element_id, mode.lower())
        self.read(element_id, "interval")
        self.read(element_id, "detect_level")

    def _setup(self, element_id):
        # Claimed all devices
        self.claim(element_id)
        # Checked the battery level
        info = self.info(element_id)
        mode = info.split(":")[-1].strip().split(" ")[0]
        return mode

    def claim(self, element_id):
        """Claim the device.

        Parameters
        ----------
        element_id : :obj:`int`
                where to send the command

        Returns
        -------
        message: :obj:`str`
            "Device id : CLAIMED" or an error if already claimed by someone else or device turn off
        """
        message = "CLAIM\r\n"
        status = self.communication.send_message_to_hardware(message, element_id)
        logging.info("Device {} : {} \n".format(element_id, status.lower()))

    def release(self, element_id):
        """Release the claimed device

        Parameters
        ----------
        element_id : :obj:`int`
                where to send the command

        Returns
        -------
        message: :obj:`str`
            "Device id : RELEASED" or an error
        """
        message = "RELEASE\r\n"
        status = self.communication.send_message_to_hardware(message, element_id)
        logging.info("Device {} : {} \n".format(element_id, status))

    def home(self, element_id):
        """Reset the position of the door.

        Parameters
        ----------
        element_id : :obj:`int`
                where to send the command

        Returns
        -------
        message: :obj:`str`
            "Device id : HOME" or an error

        """
        message = "HOME\r\n"
        status = self.communication.send_message_to_hardware(message, element_id)
        logging.info("Door {} : {} \n".format(element_id, status))

    def info(self, element_id):
        """Obtain info from the hardware.

        Parameters
        ----------
        element_id : :obj:`int`
                where to send the command

        Returns
        -------
        message: :obj:`str`
            "Device id : battery level = number%" or an error
        """
        message = "RDIF\r\n"
        status = self.communication.send_message_to_hardware(message, element_id)
        mode = None
        if "INFO" in status:
            battery = status.split(" ")[6]
            if battery[1] == "0":
                battery = "".join(battery[2:])
            else:
                battery = "".join(battery[1:])
            message = "Device {}: battery level = {}\n".format(element_id, battery)

            if "REWARD" in status.split(" ")[3]:
                if status.split(" ")[10] == "DM0":
                    mode = "Manual"
                elif status.split(" ")[10] == "DM1":
                    mode = "Auto"
                elif status.split(" ")[10] == "DM2":
                    mode = "One short"

                message += "Pump {}: {} mode ON\n".format(element_id, mode)
        else:
            message = "Device {} : {} \n".format(element_id, status)

        logging.info(message)
        return message

    def sleep(self, time_ms):
        """Freeze the app during x ms.

        Parameters
        ----------
        time_ms: :obj:`int`

        Returns
        -------
        message: :obj:`str`
            log message
        """
        time.sleep(time_ms * 0.001)
        logging.info("Wait for " + str(time_ms) + " ms\n")

    def identify(self, element_id):
        """Identify the element with a blue led blinking.

        Parameters
        ----------
        element_id : :obj:`int`
                where to send the command

        Returns
        -------
        message: :obj:`str`
            "Device id is the one with a blue led blinking" or an error
        """
        message = "IDEN 10000\r\n"
        status = self.communication.send_message_to_hardware(message, element_id)

        if status == message:
            logging.info(
                "Device {} is the one with a blue led blinking\n".format(element_id)
            )
        else:
            logging.info("Device {} : {} \n".format(element_id, status))

    def move_door(self, element_id):
        """Look the state of the door and open or close the door following its state.

        Parameters
        ----------
        element_id : :obj:`int`
                where to send the command

        Returns
        -------
        message: :obj:`str`
            message from open or close call_action or error
        """
        message = "RDMO\r\n"
        status = self.communication.send_message_to_hardware(message, element_id)

        if status == "MOTOR_STATE 0 POS_OPEN\r\n":
            self.close_door(element_id)
        elif (
            status == "MOTOR_STATE 0 POS_CLOSED\r\n"
            or status == "MOTOR_STATE 0 POS_HOME\r\n"
        ):
            self.open_door(element_id)
        else:
            raise HardwareFailureError(
                "Door", "ERROR first home the door {}\n".format(element_id)
            )

    def close_door(self, element_id):
        """Close the door.

        Parameters
        ----------
        element_id : :obj:`int`
                where to send the command

        Returns
        -------
        message: :obj:`str`
            "Door id: CLOSE" or error
        """
        message = "DCLS 60 250 30\r\n"
        status = self.communication.send_message_to_hardware(message, element_id)
        if status == message:
            logging.info("Door {}: CLOSE\n".format(element_id))
        elif status == "MOTOR FORBIDDEN":
            raise HardwareFailureError(
                "Door", "ERROR Door {} already closed\n".format(element_id)
            )
        else:
            logging.info("Door {} : {} \n".format(element_id, status))

    def open_door(self, element_id):
        """Open the door.

        Parameters
        ----------
        element_id : :obj:`int`
                where to send the command

        Returns
        -------
        message: :obj:`str`
            "Door id: OPEN" or error
        """
        message = "DOPN 60 250 800\r\n"
        status = self.communication.send_message_to_hardware(message, element_id)
        if status == message:
            logging.info("Door {} : OPEN\n".format(element_id))
        elif status == "MOTOR FORBIDDEN":
            raise HardwareFailureError(
                "Door", "ERROR Door {} already opened\n".format(element_id)
            )
        else:
            logging.info("Door {} : {} \n".format(element_id, status))

    def mode_auto(self, element_id):
        """Set the delivery food in automatic mode in a release timer.

        Parameters
        ---------
        element_id : :obj:`int`
                where to send the command

        Returns
        -------
        message: :obj:`str`
            log message
        """
        message = "SAVE DM 1\r\n"
        status = self.communication.send_message_to_hardware(message, element_id)
        if status == message:
            logging.info("Pump {}: Automatic mode ON\n".format(element_id))
        else:
            raise HardwareFailureError(
                "Food_delivery", "Pump {} : {} \n".format(element_id, status)
            )

    def mode_manual(self, element_id):
        """Set the delivery food in manual mode triggered by the pump call_action.

        Parameters
        ----------
        element_id : :obj:`int`
                where to send the command

        Returns
        -------
        message: :obj:`str`
            log message
        """
        message = "SAVE DM 0\r\n"
        status = self.communication.send_message_to_hardware(message, element_id)
        if status == message:
            logging.info("Pump {}: Manual mode ON\n".format(element_id))
        else:
            raise HardwareFailureError(
                "Food_delivery", "Pump {} : {} \n".format(element_id, status)
            )

    def mode_one_shot(self, element_id):
        """Set the delivery food in one-shot mode.

        When a peira_event occur, the food is automatically deliver (like in auto mode) and then go in manual mode.
        The food deliver is then deliver automatically only one time.

        Parameters
        ----------
        element_id : :obj:`int`
                where to send the command

        Returns
        -------
        message: :obj:`str`
            log message
        """
        message = "SAVE DM 2\r\n"
        status = self.communication.send_message_to_hardware(message, element_id)
        if status == message:
            logging.info("Pump {}: One shot mode ON\n".format(element_id))
        else:
            raise HardwareFailureError(
                "Food_delivery", "Pump {} : {} \n".format(element_id, status)
            )

    def dispense(self, element_id):
        """Deliver food when in manual mode.

        Parameters
        ----------
        element_id : :obj:`int`
                where to send the command

        Returns
        -------
        message: :obj:`str`
            log message
        """
        message = "PUMP 0\r\n"
        status = self.communication.send_message_to_hardware(message, element_id)
        if status == message:
            logging.info("Pump {} is dispensing food\n".format(element_id))
        else:
            raise HardwareFailureError(
                "Food_delivery", "Pump {} : {} \n".format(element_id, status)
            )

    def fill(self, element_id):
        """Fill tube with chocolate sirup.

        Parameters
        ----------
        element_id : :obj:`int`
                where to send the command

        Returns
        -------
        message: :obj:`str`
            log message
        """
        message = "PUMP 11000\r\n"
        status = self.communication.send_message_to_hardware(message, element_id)
        if status == message:
            logging.info(
                "Pump {} is filling the tube with chocolate sirup\n".format(element_id)
            )
        else:
            raise HardwareFailureError(
                "Food_delivery", "Pump {} : {} \n".format(element_id, status)
            )

    def empty(self, element_id):
        """Empty tube.

        Parameters
        ----------
        element_id : :obj:`int`
                where to send the command

        Returns
        -------
        message: :obj:`str`
            log message
        """
        message = "RINS 10 11000 1\r\n"
        status = self.communication.send_message_to_hardware(message, element_id)
        if status == message:
            logging.info("Pump {} is emptying the tube\n".format(element_id))
        else:
            raise HardwareFailureError(
                "Food_delivery", "Pump {} : {} \n".format(element_id, status)
            )

    def rinse(self, element_id):
        """Rinse the tubes.

        Parameters
        ----------
        element_id : :obj:`int`
                where to send the command

        Returns
        -------
        message: :obj:`str`
            log message
        """
        message = "RINS 22000 10 4\r\n"
        status = self.communication.send_message_to_hardware(message, element_id)
        if status == message:
            logging.info("Pump {} is rinsing the tube\n".format(element_id))
        else:
            raise HardwareFailureError(
                "Food_delivery", "Pump {} : {} \n".format(element_id, status)
            )

    def delivery_setting(self, element_id, delivery_mode, delivery_time):
        """Update time parameter in the delivering food setting.

        Parameters
        ----------
        element_id : :obj:`int`
                where to send the command
        delivery_mode : :obj:`str`

                - *auto*: save dispensing time for automatic mode (ms) in the setting
                - *button* : save dispensing time for button 1 (ms) in the setting
                - *manual* : save dispensing time for command mode (ms) in the setting
                - *all* :  save dispensing time for all modes (ms) in the setting

        delivery_time : :obj:`int`
                time to save in the setting


        Returns
        -------
        message: :obj:`str`
            log message
        """
        element_message = "Pump " + str(element_id) + ": "

        if delivery_mode in ["auto", "DTA"]:
            # save dispensing time for automatic mode (ms) in the setting
            command = "SAVE DTA {}".format(delivery_time)
            text_output = "Save dispensing time for automatic mode to {}".format(
                delivery_time
            )
        elif delivery_mode in ["button", "DTB"]:
            # save dispensing time for button 1 (ms) in the setting
            command = "SAVE DTB {}".format(delivery_time)
            text_output = "Save dispensing time for button mode to {}".format(
                delivery_time
            )
        elif delivery_mode in ["manual", "DTC"]:
            # save dispensing time for command mode (ms) in the setting
            command = "SAVE DTC {}".format(delivery_time)
            text_output = "Save dispensing time for manual mode to {}".format(
                delivery_time
            )
        elif delivery_mode in ["all", "DT"]:
            # save dispensing time for all command mode (ms) in the setting
            command = "SAVE DT {}".format(delivery_time)
            text_output = "Save dispensing time for all modes to {}".format(delivery_time)
        else:
            command = "SAVE {} {}".format(delivery_mode, delivery_time)
            text_output = "Save {} mode to {}".format(delivery_mode, delivery_time)

        message = command + " 1\r\n"
        status = self.communication.send_message_to_hardware(message, element_id)
        if status == message:
            logging.info(element_message + text_output + "\n")
        else:
            raise HardwareFailureError(
                "Food_delivery", "Pump {} : {} \n".format(element_id, status)
            )

    def sensor_setting(self, element_id, sensor_setting, quantity):
        """Update the sensor settings on the food pumps.

        Parameters
        ----------
        element_id : :obj:`int`
                where to send the command

        sensor_setting:  :obj:`str`

                - *interval* : save dispensing interval (ms) in the setting. To prevent constant repeating of dispensing when the animal moves around a minimum interval can be defined.  The time starts counting when the animal is AWAY from the sensor.
                - *detect_level* : save proximity sensor detect level. this parameter sets the maximum distance at which an animal can be detected.
                - *diode_current* : set the diode current of the LED

        quantity :  :obj:`int`
                the value to change in the parameter

        Returns
        -------
        message: :obj:`str`
            log message

        """

        element_message = "Pump " + str(element_id) + ": "
        if sensor_setting in ["interval", "DI"]:
            # save dispensing interval in the setting.
            # To prevent constant repeating of dispensing when the animal moves around a
            # minimum interval can be defined. Time in ms. The time starts counting when the
            # animal is AWAY from the sensor.
            command = "SAVE DI {}".format(quantity)
            text_output = "Save dispensing interval to {}".format(quantity)
        elif sensor_setting in ["detect_level", "DL"]:
            # save proximity sensor detect level
            # this parameter sets the maximum distance at which an animal can be detected.
            command = "SAVE DL {}".format(quantity)
            text_output = "Save proximity sensor peira_event level to {}".format(quantity)
        elif sensor_setting in ["diode_current", "DC"]:
            # set the diode current of the LED
            command = "SAVE DC {}".format(quantity)
            text_output = "Save diode current to {}".format(quantity)
        else:
            command = "SAVE {} {}".format(sensor_setting, quantity)
            text_output = "Save {} to {}".format(sensor_setting, quantity)

        message = command + " 1\r\n"
        status = self.communication.send_message_to_hardware(message, element_id)
        if status == message:
            logging.info(element_message + text_output + "\n")
        else:
            raise HardwareFailureError(
                "Food_delivery", "Pump {} : {} \n".format(element_id, status)
            )

    def read(self, element_id, parameter):
        """Read saved parameter on device.

        Parameters
        ----------
        element_id : :obj:`int`
                where to send the command
        parameter : :obj:`str`

                - *auto* : read dispensing time for automatic mode (ms) in the setting
                - *button* : read dispensing time for button 1 (ms) in the setting
                - *manual* : read dispensing time for command mode (ms) in the setting
                - *interval* : read dispensing interval (ms) in the setting.
                - *detect_level* : read proximity sensor detect level.
                - *diode_current* : read the diode current of the LED

        Returns
        -------
        message: :obj:`str`
            log message
        """
        element_message = "Pump " + str(element_id) + ": "

        if parameter in ["auto", "DTA"]:
            # read dispensing time for automatic mode (ms) in the setting
            command = "READ DTA"
            text_output = "Dispensing time for automatic mode = "
        elif parameter in ["button", "DTB"]:
            # read dispensing time for button 1 (ms) in the setting
            command = "READ DTB"
            text_output = "Dispensing time for button mode = "
        elif parameter in ["manual", "DTC"]:
            # read dispensing time for command mode (ms) in the setting
            command = "READ DTC"
            text_output = "Dispensing time for manual mode = "
        elif parameter in ["diode_current", "DC"]:
            command = "READ DC"
            text_output = "Diode current = "
        elif parameter in ["detect_level", "DL"]:
            command = "READ DL"
            text_output = "Proximity sensor detect level = "
        elif parameter in ["interval", "DI"]:
            command = "READ DI"
            text_output = "Dispensing minimal interval = "
        else:
            command = "READ {}".format(parameter)
            text_output = str(parameter) + " = "

        message = command + " 1\r\n"
        status = self.communication.send_message_to_hardware(message, element_id)
        if "READ" in status:
            _, _, state = status.split(" ")
            logging.info(element_message + text_output + str(state) + "\n")
        else:
            raise HardwareFailureError(
                "Food_delivery", "Pump {} : {} \n".format(element_id, status)
            )
