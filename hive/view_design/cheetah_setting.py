# -*- coding: utf-8 -*-
# Form implementation generated from reading ui file 'cheetah_setting.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!
from PyQt5 import QtCore
from PyQt5 import QtGui
from PyQt5 import QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(1256, 244)
        self.gridLayout = QtWidgets.QGridLayout(Dialog)
        self.gridLayout.setObjectName("gridLayout")
        self.connexion = QtWidgets.QHBoxLayout()
        self.connexion.setObjectName("connexion")
        self.groupBox = QtWidgets.QGroupBox(Dialog)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Ignored
        )
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.groupBox.sizePolicy().hasHeightForWidth())
        self.groupBox.setSizePolicy(sizePolicy)
        self.groupBox.setObjectName("groupBox")
        self.horizontalLayoutWidget = QtWidgets.QWidget(self.groupBox)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(10, 40, 591, 41))
        self.horizontalLayoutWidget.setObjectName("horizontalLayoutWidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label_2 = QtWidgets.QLabel(self.horizontalLayoutWidget)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout.addWidget(self.label_2)
        self.hostname = QtWidgets.QLineEdit(self.horizontalLayoutWidget)
        self.hostname.setObjectName("hostname")
        self.horizontalLayout.addWidget(self.hostname)
        self.label_3 = QtWidgets.QLabel(self.horizontalLayoutWidget)
        self.label_3.setObjectName("label_3")
        self.horizontalLayout.addWidget(self.label_3)
        self.port = QtWidgets.QLineEdit(self.horizontalLayoutWidget)
        self.port.setObjectName("port")
        self.horizontalLayout.addWidget(self.port)
        self.connexion_button = QtWidgets.QPushButton(self.horizontalLayoutWidget)
        self.connexion_button.setObjectName("connexion_button")
        self.horizontalLayout.addWidget(self.connexion_button)
        self.connexion.addWidget(self.groupBox)
        self.gridLayout.addLayout(self.connexion, 0, 0, 1, 2)
        self.Logs = QtWidgets.QGroupBox(Dialog)
        self.Logs.setObjectName("Logs")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.Logs)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.revertFilters = QtWidgets.QPushButton(self.Logs)
        self.revertFilters.setObjectName("revertFilters")
        self.gridLayout_2.addWidget(self.revertFilters, 2, 2, 1, 1)
        self.detection_filter = QtWidgets.QCheckBox(self.Logs)
        self.detection_filter.setObjectName("detection_filter")
        self.gridLayout_2.addWidget(self.detection_filter, 1, 2, 1, 1)
        self.applyFilters = QtWidgets.QPushButton(self.Logs)
        self.applyFilters.setObjectName("applyFilters")
        self.gridLayout_2.addWidget(self.applyFilters, 2, 0, 1, 1)
        self.state_filter = QtWidgets.QCheckBox(self.Logs)
        self.state_filter.setObjectName("state_filter")
        self.gridLayout_2.addWidget(self.state_filter, 1, 1, 1, 1)
        self.actions_filter = QtWidgets.QCheckBox(self.Logs)
        self.actions_filter.setChecked(True)
        self.actions_filter.setObjectName("actions_filter")
        self.gridLayout_2.addWidget(self.actions_filter, 1, 0, 1, 1)
        self.gridLayout.addWidget(self.Logs, 1, 0, 1, 1)
        self.command = QtWidgets.QGroupBox(Dialog)
        self.command.setObjectName("command")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.command)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.startAcquisition = QtWidgets.QPushButton(self.command)
        self.startAcquisition.setObjectName("startAcquisition")
        self.gridLayout_3.addWidget(self.startAcquisition, 0, 0, 1, 1)
        self.stopAcquisition = QtWidgets.QPushButton(self.command)
        self.stopAcquisition.setEnabled(False)
        self.stopAcquisition.setObjectName("stopAcquisition")
        self.gridLayout_3.addWidget(self.stopAcquisition, 0, 1, 1, 1)
        self.startRecording = QtWidgets.QPushButton(self.command)
        self.startRecording.setObjectName("startRecording")
        self.gridLayout_3.addWidget(self.startRecording, 1, 0, 1, 1)
        self.stopRecording = QtWidgets.QPushButton(self.command)
        self.stopRecording.setEnabled(False)
        self.stopRecording.setObjectName("stopRecording")
        self.gridLayout_3.addWidget(self.stopRecording, 1, 1, 1, 1)
        self.gridLayout.addWidget(self.command, 1, 1, 1, 1)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.groupBox.setTitle(_translate("Dialog", "Cheetah setting"))
        self.label_2.setText(_translate("Dialog", "IP address / hostname"))
        self.hostname.setText(_translate("Dialog", "localhost"))
        self.label_3.setText(_translate("Dialog", "Port"))
        self.port.setText(_translate("Dialog", "5997"))
        self.connexion_button.setText(_translate("Dialog", "Connect"))
        self.Logs.setTitle(_translate("Dialog", "Logs"))
        self.revertFilters.setToolTip(
            _translate("Dialog", "Undo changes to filter settings.")
        )
        self.revertFilters.setStatusTip(
            _translate("Dialog", "Undo changes to filter settings.")
        )
        self.revertFilters.setText(_translate("Dialog", "Revert Filters"))
        self.detection_filter.setToolTip(
            _translate("Dialog", "Display/hide update messages.")
        )
        self.detection_filter.setStatusTip(
            _translate("Dialog", "Display/hide update messages.")
        )
        self.detection_filter.setText(_translate("Dialog", "Animal detection"))
        self.applyFilters.setToolTip(
            _translate("Dialog", "Apply filters to all received messages.")
        )
        self.applyFilters.setStatusTip(
            _translate("Dialog", "Apply filters to all received messages.")
        )
        self.applyFilters.setText(_translate("Dialog", "Apply Filters"))
        self.state_filter.setToolTip(_translate("Dialog", "Display/hide state messages."))
        self.state_filter.setStatusTip(
            _translate("Dialog", "Display/hide state messages.")
        )
        self.state_filter.setText(_translate("Dialog", "Devices state"))
        self.actions_filter.setToolTip(
            _translate("Dialog", "Display/hide event messages.")
        )
        self.actions_filter.setStatusTip(
            _translate("Dialog", "Display/hide event messages.")
        )
        self.actions_filter.setText(_translate("Dialog", "Devices action"))
        self.command.setTitle(_translate("Dialog", "Commands"))
        self.startAcquisition.setText(_translate("Dialog", "Start Acquisition"))
        self.stopAcquisition.setText(_translate("Dialog", "Stop Acquisition"))
        self.startRecording.setText(_translate("Dialog", "Start Recording"))
        self.stopRecording.setText(_translate("Dialog", "Stop Recording"))
