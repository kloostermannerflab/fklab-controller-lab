# -*- coding: utf-8 -*-
# Form implementation generated from reading ui file 'lab_app_view.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!
from PyQt5 import QtCore
from PyQt5 import QtGui
from PyQt5 import QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1261, 733)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.logs = QtWidgets.QTextEdit(self.centralwidget)
        self.logs.setObjectName("logs")
        self.gridLayout_2.addWidget(self.logs, 4, 0, 1, 4)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setObjectName("label_3")
        self.horizontalLayout.addWidget(self.label_3)
        spacerItem = QtWidgets.QSpacerItem(
            40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum
        )
        self.horizontalLayout.addItem(spacerItem)
        self.cheetah_setting = QtWidgets.QPushButton(self.centralwidget)
        self.cheetah_setting.setObjectName("cheetah_setting")
        self.horizontalLayout.addWidget(self.cheetah_setting)
        self.gridLayout_2.addLayout(self.horizontalLayout, 3, 0, 1, 4)
        self.release_button = QtWidgets.QPushButton(self.centralwidget)
        self.release_button.setObjectName("release_button")
        self.gridLayout_2.addWidget(self.release_button, 5, 1, 1, 1)
        self.door_table = QtWidgets.QTableWidget(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum
        )
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.door_table.sizePolicy().hasHeightForWidth())
        self.door_table.setSizePolicy(sizePolicy)
        self.door_table.setSizeAdjustPolicy(
            QtWidgets.QAbstractScrollArea.AdjustToContents
        )
        self.door_table.setColumnCount(3)
        self.door_table.setObjectName("door_table")
        self.door_table.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        self.door_table.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.door_table.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.door_table.setHorizontalHeaderItem(2, item)
        self.gridLayout_2.addWidget(self.door_table, 2, 0, 1, 4)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setObjectName("label_4")
        self.gridLayout.addWidget(self.label_4, 0, 0, 1, 1)
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 2, 0, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 1, 0, 1, 1)
        self.config_file = QtWidgets.QComboBox(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed
        )
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.config_file.sizePolicy().hasHeightForWidth())
        self.config_file.setSizePolicy(sizePolicy)
        self.config_file.setObjectName("config_file")
        self.gridLayout.addWidget(self.config_file, 0, 1, 1, 1)
        self.user_label = QtWidgets.QLineEdit(self.centralwidget)
        self.user_label.setObjectName("user_label")
        self.gridLayout.addWidget(self.user_label, 1, 1, 1, 1)
        self.user_input = QtWidgets.QLineEdit(self.centralwidget)
        self.user_input.setObjectName("user_input")
        self.gridLayout.addWidget(self.user_input, 2, 1, 1, 1)
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setEnabled(False)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed
        )
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_5.sizePolicy().hasHeightForWidth())
        self.label_5.setSizePolicy(sizePolicy)
        self.label_5.setObjectName("label_5")
        self.gridLayout.addWidget(self.label_5, 3, 0, 1, 1)
        self.detection_input = QtWidgets.QLineEdit(self.centralwidget)
        self.detection_input.setEnabled(False)
        self.detection_input.setObjectName("detection_input")
        self.gridLayout.addWidget(self.detection_input, 3, 1, 1, 1)
        self.verticalLayout.addLayout(self.gridLayout)
        self.gridLayout_2.addLayout(self.verticalLayout, 0, 0, 1, 5)
        self.pump_table = QtWidgets.QTableWidget(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum
        )
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pump_table.sizePolicy().hasHeightForWidth())
        self.pump_table.setSizePolicy(sizePolicy)
        self.pump_table.setSizeAdjustPolicy(
            QtWidgets.QAbstractScrollArea.AdjustToContents
        )
        self.pump_table.setColumnCount(7)
        self.pump_table.setObjectName("pump_table")
        self.pump_table.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        self.pump_table.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.pump_table.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.pump_table.setHorizontalHeaderItem(2, item)
        item = QtWidgets.QTableWidgetItem()
        self.pump_table.setHorizontalHeaderItem(3, item)
        item = QtWidgets.QTableWidgetItem()
        self.pump_table.setHorizontalHeaderItem(4, item)
        item = QtWidgets.QTableWidgetItem()
        self.pump_table.setHorizontalHeaderItem(5, item)
        item = QtWidgets.QTableWidgetItem()
        self.pump_table.setHorizontalHeaderItem(6, item)
        self.gridLayout_2.addWidget(self.pump_table, 1, 0, 1, 4)
        self.command_list = QtWidgets.QTextEdit(self.centralwidget)
        self.command_list.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.command_list.setFrameShadow(QtWidgets.QFrame.Plain)
        self.command_list.setObjectName("command_list")
        self.gridLayout_2.addWidget(self.command_list, 1, 4, 4, 1)
        self.sequence_layout = QtWidgets.QGridLayout()
        self.sequence_layout.setObjectName("sequence_layout")
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.label_7 = QtWidgets.QLabel(self.centralwidget)
        self.label_7.setObjectName("label_7")
        self.horizontalLayout_4.addWidget(self.label_7)
        self.trials = QtWidgets.QLineEdit(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Ignored, QtWidgets.QSizePolicy.Fixed
        )
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.trials.sizePolicy().hasHeightForWidth())
        self.trials.setSizePolicy(sizePolicy)
        self.trials.setObjectName("trials")
        self.horizontalLayout_4.addWidget(self.trials)
        self.label_6 = QtWidgets.QLabel(self.centralwidget)
        self.label_6.setObjectName("label_6")
        self.horizontalLayout_4.addWidget(self.label_6)
        self.success = QtWidgets.QLineEdit(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Ignored, QtWidgets.QSizePolicy.Fixed
        )
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.success.sizePolicy().hasHeightForWidth())
        self.success.setSizePolicy(sizePolicy)
        self.success.setObjectName("success")
        self.horizontalLayout_4.addWidget(self.success)
        self.label_8 = QtWidgets.QLabel(self.centralwidget)
        self.label_8.setObjectName("label_8")
        self.horizontalLayout_4.addWidget(self.label_8)
        self.fail = QtWidgets.QLineEdit(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Ignored, QtWidgets.QSizePolicy.Fixed
        )
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.fail.sizePolicy().hasHeightForWidth())
        self.fail.setSizePolicy(sizePolicy)
        self.fail.setObjectName("fail")
        self.horizontalLayout_4.addWidget(self.fail)
        self.sequence_layout.addLayout(self.horizontalLayout_4, 4, 1, 1, 1)
        self.graph = PlotWidget(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding
        )
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.graph.sizePolicy().hasHeightForWidth())
        self.graph.setSizePolicy(sizePolicy)
        self.graph.setObjectName("graph")
        self.sequence_layout.addWidget(self.graph, 5, 1, 1, 1)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.SEQ_mode = QtWidgets.QCheckBox(self.centralwidget)
        self.SEQ_mode.setEnabled(False)
        self.SEQ_mode.setObjectName("SEQ_mode")
        self.horizontalLayout_2.addWidget(self.SEQ_mode)
        spacerItem1 = QtWidgets.QSpacerItem(
            40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum
        )
        self.horizontalLayout_2.addItem(spacerItem1)
        self.sequence_layout.addLayout(self.horizontalLayout_2, 0, 1, 1, 1)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.sequence_layout.addLayout(self.horizontalLayout_3, 1, 1, 1, 1)
        self.grid_layout_3 = QtWidgets.QGridLayout()
        self.grid_layout_3.setObjectName("grid_layout_3")
        self.sequence = QtWidgets.QLineEdit(self.centralwidget)
        self.sequence.setObjectName("sequence")
        self.grid_layout_3.addWidget(self.sequence, 0, 1, 1, 1)
        self.sequence_label = QtWidgets.QLabel(self.centralwidget)
        self.sequence_label.setEnabled(False)
        self.sequence_label.setObjectName("sequence_label")
        self.grid_layout_3.addWidget(self.sequence_label, 0, 0, 1, 1)
        self.sequence_layout.addLayout(self.grid_layout_3, 3, 1, 1, 1)
        self.gridLayout_2.addLayout(self.sequence_layout, 0, 5, 5, 1)
        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setObjectName("pushButton_2")
        self.gridLayout_2.addWidget(self.pushButton_2, 5, 5, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label_3.setText(_translate("MainWindow", "Logs"))
        self.cheetah_setting.setText(_translate("MainWindow", "Cheetah setting"))
        self.release_button.setText(_translate("MainWindow", "Release devices"))
        item = self.door_table.horizontalHeaderItem(0)
        item.setText(_translate("MainWindow", "Door id"))
        item = self.door_table.horizontalHeaderItem(1)
        item.setText(_translate("MainWindow", "Battery"))
        item = self.door_table.horizontalHeaderItem(2)
        item.setText(_translate("MainWindow", "Status"))
        self.label_4.setText(_translate("MainWindow", "App config file"))
        self.label.setText(_translate("MainWindow", "Input"))
        self.label_2.setText(_translate("MainWindow", "User name"))
        self.label_5.setText(_translate("MainWindow", "Trigger detection manually"))
        item = self.pump_table.horizontalHeaderItem(0)
        item.setText(_translate("MainWindow", "Pump ID"))
        item = self.pump_table.horizontalHeaderItem(1)
        item.setText(_translate("MainWindow", "Battery"))
        item = self.pump_table.horizontalHeaderItem(2)
        item.setText(_translate("MainWindow", "Status"))
        item = self.pump_table.horizontalHeaderItem(3)
        item.setText(_translate("MainWindow", "Mode"))
        item = self.pump_table.horizontalHeaderItem(4)
        item.setText(_translate("MainWindow", "Delivery time"))
        item = self.pump_table.horizontalHeaderItem(5)
        item.setText(_translate("MainWindow", "Interval"))
        item = self.pump_table.horizontalHeaderItem(6)
        item.setText(_translate("MainWindow", "Distance"))
        self.command_list.setHtml(
            _translate(
                "MainWindow",
                '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">\n'
                '<html><head><meta name="qrichtext" content="1" /><style type="text/css">\n'
                "p, li { white-space: pre-wrap; }\n"
                "</style></head><body style=\" font-family:'Noto Sans'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
                '<p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=" font-family:\'Ubuntu\'; font-size:11pt;">Command : </span></p></body></html>',
            )
        )
        self.label_7.setText(_translate("MainWindow", "Trials"))
        self.trials.setText(_translate("MainWindow", "0"))
        self.label_6.setText(_translate("MainWindow", "Success"))
        self.success.setText(_translate("MainWindow", "0"))
        self.label_8.setText(_translate("MainWindow", "Fail"))
        self.fail.setText(_translate("MainWindow", "0"))
        self.SEQ_mode.setText(_translate("MainWindow", "SEQUENCE AUTO MODE"))
        self.sequence_label.setText(_translate("MainWindow", "Sequence :"))
        self.pushButton_2.setText(_translate("MainWindow", "Save graph figure"))


from pyqtgraph import PlotWidget
