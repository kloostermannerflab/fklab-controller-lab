import logging
import os

import yaml

from hive.pyqt import QtWidgets
from hive.view_design.cheetah_setting import Ui_Dialog


class Cheetah(QtWidgets.QDialog):
    def __init__(self, parent, config=None):
        # set up the view
        self.parent = parent
        QtWidgets.QDialog.__init__(self, parent)
        self.config_path = os.path.expanduser("~/.config/hive/cheetah.yaml")
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.CheetahLogger = logging.getLogger("").handlers[1]
        self.ui.connexion_button.pressed.connect(self.update_connexion)
        self.ui.applyFilters.pressed.connect(self.set_filter)
        self.ui.revertFilters.pressed.connect(self.revert_filter)

        self.ui.startAcquisition.pressed.connect(self.startAcquisition)
        self.ui.startRecording.pressed.connect(self.startRecording)

        self.ui.stopAcquisition.pressed.connect(self.stopAcquisition)
        self.ui.stopRecording.pressed.connect(self.stopRecording)

        if os.path.exists(self.config_path):
            with open(self.config_path, "r") as file:
                config = yaml.safe_load(file)

            if "hostname" in config:
                self.ui.hostname.setText(config["hostname"])

            if "port" in config:
                self.ui.port.setText(config["port"])

        self.update_connexion()
        self.set_filter()

    def update_connexion(self):
        hostname = self.ui.hostname.text()
        port = self.ui.port.text()

        self.CheetahLogger.cheetah.ip_address = hostname
        self.CheetahLogger.cheetah.port = port
        self.CheetahLogger.connect = True

        self.update_config()

    def set_filter(self):
        checkList = []
        if self.ui.actions_filter.isChecked():
            checkList.append(
                lambda msg: msg.startswith("Door")
                or (msg.startswith("Pump") and not ":" in msg)
            )
        if self.ui.state_filter.isChecked():
            checkList.append(lambda msg: msg.startswith("Pump") and ":" in msg)

        if self.ui.detection_filter.isChecked():
            checkList.append(lambda msg: msg.startswith("detection"))

        self.CheetahLogger.checkList = checkList

    def revert_filter(self):
        self.ui.actions_filter.setChecked(True)
        self.ui.state_filter.setChecked(False)
        self.ui.detection_filter.setChecked(False)
        self.set_filter()

    def startAcquisition(self):
        print("Not yet implemented")

    def stopAcquisition(self):
        print("Not yet implemented")

    def startRecording(self):
        print("Not yet implemented")

    def stopRecording(self):
        print("Not yet implemented")

    def update_config(self):

        config = {"hostname": self.ui.hostname.text(), "port": self.ui.port.text()}
        if not os.path.exists(self.config_path):
            os.makedirs(os.path.dirname(self.config_path), exist_ok=True)

        with open(self.config_path, "w") as file:
            yaml.safe_dump(config, file)


def create_information_box(msg, title):
    """ create a information box in the ui

    Parameters:
    ----------
    msg : str
    title : str
    """
    msg_box = QtWidgets.QMessageBox()
    if title == "Information":
        msg_box.setIcon(QtWidgets.QMessageBox.Information)
    elif title == "Warning":
        msg_box.setIcon(QtWidgets.QMessageBox.Warning)
    msg_box.setText(msg)
    msg_box.setWindowTitle(title)
    msg_box.setStandardButtons(QtWidgets.QMessageBox.Ok)
    msg_box.exec()
