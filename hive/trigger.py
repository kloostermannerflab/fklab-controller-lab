import logging
import os
import re
import sched
import socket
import time
import typing
from abc import abstractmethod

import numpy as np
import zmq
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtCore import QObject
from PyQt5.QtCore import QThread

__all__ = ["UserCommand", "Server", "Keyboard", "Tracking", "Timer"]


class Trigger:
    @abstractmethod
    def who_to_connect_where(self):
        pass


class UserCommand(QObject, Trigger):
    """Trigger to listen user events from the ui.

    Two type of user command :

    - commands directly for the maze: configured in the config file.
    - command for the task: configured in the task.

    No need to add it to the config file. The trigger is always linked to the task.

    Parameters
    ----------
    command : dict
        All command list in the config file.

    hardware : dict
        All hardware used in the maze with a mapping between virtual id and real id.
    """

    user_event = pyqtSignal(dict)
    """:obj:`dict`: trigger the user input process."""

    def __init__(self, command, hardware):
        QObject.__init__(self)
        self._command = command
        self._hardware = hardware  # type: dict[str, dict[str, str]]

    def who_to_connect_where(self):
        return self.user_event, "process_user_event"

    def trigger_internal_command(self, command):
        default_command = {"action": "", "targets": [], "options": []}
        default_command.update(command)

        if isinstance(default_command["targets"], str):
            default_command["targets"] = [default_command["targets"]]

        default_command["targets"] = self._switch_virtual2real_id(
            default_command["targets"]
        )
        self.user_event.emit(default_command)

    def trigger_user_command(self, key):
        code, *options = re.split(r"\s+", key)

        if code not in self._command:
            self.user_event.emit({"command": key})
            return

        if not isinstance(self._command[code], list):
            self._command[code] = [self._command[code]]

        for config in self._command[code]:
            action, targets, options = self._parse_action_dict(config, code, options)
            self.user_event.emit(
                {"action": action, "targets": targets, "options": options}
            )

    def _parse_action_dict(self, config, code, options_value):
        code_dict = {"action": None, "output target": [], "options": []}
        code_dict.update(config)
        action = code_dict["action"]
        targets = code_dict["output target"]
        options_name = code_dict["options"]

        self._option_error_check(code, options_name, options_value)

        sleep_command = self._detect_sleep_command(action)
        if sleep_command:
            action = sleep_command["action"]
            options_value = sleep_command["option value"]
            options_name = sleep_command["option name"]

        targets, options = self._merge_all_targets(targets, options_name, options_value)

        return action, targets, options

    def _option_error_check(self, code, option_description, options):

        if len(option_description) < len(options):
            raise RuntimeError(
                "ERROR for the command "
                + str(code)
                + " there is not enough options. - See your config file.\n"
            )

        elif len(option_description) > len(options):
            raise RuntimeError(
                "ERROR for the command "
                + str(code)
                + " there is too many options. - See your config file.\n"
            )

    def _detect_sleep_command(self, action):
        if isinstance(action, int) or action.isdigit():
            return {"action": "sleep", "option name": "time_ms", "option value": action}
        return None

    def _merge_all_targets(self, targets, option_name, options_value):
        if isinstance(targets, int) or isinstance(targets, str):
            targets = [str(targets)]

        if "output_target" in option_name:
            output_target_i = option_name.index("output_target")
            if targets:
                targets.append(options_value[output_target_i])
            else:
                targets = [options_value[output_target_i]]
            del options_value[output_target_i]

        targets = self._switch_virtual2real_id(targets)
        return targets, options_value

    def _switch_virtual2real_id(self, virtual_id):
        real_id = []
        for target in virtual_id:
            if target in self._hardware:
                real_id.extend(self._hardware[target].values())
            else:
                for group in self._hardware.values():
                    if target in group:
                        real_id.append(group[target])
                        break
                else:
                    real_id.append(target)
        return real_id


class Server(QThread, Trigger):
    """Trigger to listen events from the maze.

    Two type of events :

    - detection events: process in the task in method process_detection
    - battery level events: process in the task in method process_battery_level

    To use it add in the config file the keyword = server

    .. code-block::

        Task:
            mode : ...
            trigger :
                - name : server

    Parameters
    ----------
    test : :obj:`bool`, opt
        automatically set the address ip to 127.0.0.1 and the port to 65430

    address : :obj:`str`, optional
        ip address of the Peira server

    port: int, optional
        port address of the Peira server

    Note
    ----

    The server continuously listen on the selected port in a second thread.
    """

    peira_event = pyqtSignal(str)
    """:obj:`str`: trigger the peira_event process."""

    def __init__(self, test=False, address="192.168.0.100", port=3001):
        QThread.__init__(self)

        if test:
            address = "127.0.0.1"
            port = 65430

        self._listen = socket.socket()
        self._listen.connect((address, port))
        self._listen.settimeout(None)  # blocking mode, always listen
        self.start()

    def who_to_connect_where(self):
        return self.peira_event, "process_event"

    def __del__(self):
        self.wait()

    def run(self):
        """Run in a separate thread waiting for new event send by the maze.

        Event process for the moment is :

         - when an animal is detected (code=31 with output=on)
         - when a device has a too low battery level (code=60)
        """
        while True:
            try:
                event = self._listen.recv(1024)
            except (ConnectionAbortedError, ConnectionResetError) as e:
                logging.warning("server recv fail", exc_info=e)
            else:
                if event != b"":
                    message = str(event.decode("utf-8")).split(" ")
                    if len(message) < 3:
                        return
                    if message[1] == "31" and message[-1] == "on\r\n":
                        device_id = str(message[2])
                        self.peira_event.emit("detection:" + device_id)
                    elif message[1] == "60":
                        percentage = str(message[-1])
                        device_id = str(message[2])
                        self.peira_event.emit(
                            "battery level:" + device_id + "," + percentage
                        )
                else:
                    self.msleep(10)


class Keyboard(QObject, Trigger):
    """Commands in the view can trigger actual animal detection (from the view, from the code).

    **Use in the config file**

    .. code-block::

        Task:
            mode : ...
            trigger :
                - name: Keyboard

    **Use in task**

    See the process_detection method in your task
    """

    detection = pyqtSignal(str)
    """:obj:`str`: trigger the detection process."""

    def __init__(self, **command):
        QObject.__init__(self)

    def who_to_connect_where(self):
        return self.detection, "process_event"

    def create_detection(self, key):
        """Emit for the task a detection signal with the device id detected."""
        self.detection.emit("detection:" + key)


class Tracking(QThread, Trigger):
    """Position in pixels received from the tracking device and processed to extract the ROI with the hive_environment.npy file.

    This file is generated by the tracking ui of the fklab module before launching the tracking on the external device.
    You could also do it manually by generating a numpy array with for each pixel a label.

    **Use in config file**

    .. code-block::

        Task:
            mode : ...
            trigger :
                - name: Tracking
                  options:
                    ip: 127.0.0.1   #Mandatory to add
                    port: 12345     #Mandatory to add
                    source id: 1  # source id of the message received - default 1
                    environment: path/hive_mask.npy # environment created during configuration of the tracking
                    downsample: 2 # send only one position of two


    **Use in task**

    See the process_detection method in your task

    Parameters
    ----------
    ip : :obj:`str`, * (localhost)
        Ip address of the tracking device
    port: :obj:`str` or :obj:`int`, 12345
        Port where the tracking device send positions
    environment:  :obj:`str`, None
        Path of the ROI file named hive_mask.npy
    source_id: :obj:`string`, 1
        source identifier of the tracking device

    """

    ROI = pyqtSignal(str)
    """:obj:`str`: send an event based on the ROI detection"""

    def __init__(
        self,
        test=False,
        ip="*",
        port=12345,
        environment=None,
        downsample=None,
        source_id="1",
    ):
        QThread.__init__(self)

        self.ip = ip
        self.port = port
        self.downsample = downsample

        self.environment = None
        if environment:
            if not os.path.exists(environment):
                raise ValueError(
                    "The environment file path in the Tracking trigger does not exist: "
                    + environment
                )
            if os.path.basename(environment) != "hive_mask.npy":
                raise ValueError(
                    "The environment file path in the Tracking trigger is not the file hive_mask.npy generated "
                    "by the Tracking control ui in fklab package: " + environment
                )

            self.environment = np.load(environment, allow_pickle=True)

        context = zmq.Context()
        self.subscriber = context.socket(zmq.SUB)
        self.subscriber.subscribe(source_id)

        ip_address = "tcp://" + str(self.ip) + ":" + str(self.port)
        self.subscriber.connect(ip_address)

        logging.debug(
            "Connection to the tracking device "
            + ip_address
            + "\nReady to receive position data"
        )

        self.start()

    def who_to_connect_where(self):
        return self.ROI, "process_event"

    def run(self):
        """Run in a separate thread waiting for receiving new position from the tracking device.
        """
        prev_ROI = None

        while 1:
            msg = self.subscriber.recv().decode()
            source_id, msg = msg.split(" ")
            position = msg.split(",")[0:2]
            if "nan" not in position:
                if self.environment is not None:
                    ROI = self.environment[int(position[1]), int(position[0])]

                    if prev_ROI is None or prev_ROI != ROI:
                        self.ROI.emit(
                            "detection:" + ROI[1:]
                        )  # Remove first letter as we only want the id number of the zone.
                        prev_ROI = ROI


class Timer(QThread, Trigger):
    """Timer Trigger.

    **Use in the Config file**

    .. code-block::

       Task:
         mode: ...
         trigger:
           - name: Timer
             options:
               event_name_1: # routine timer
                 interval: 1 # 1 seconds           # (float) time interval.
                 event_type: "routine"             # (str) 'routine', 'one_shot', 'continue'.
               event_name_2: # one shot timer
                 interval: 600 # 10 minutes
                 event_type: "one_shot"
               event_name_3: # continue timer
                 interval: 1
                 event_type: "continue"
                 repeat_times: 10                 # (int) repeat firing times. None for infinite times. Only used in 'continue' timer.

    **Use in task**

    Code implementation:

    .. code-block::

        # get timer trigger from somewhere
        timer: Timer

        # create a timer handle
        h = timer.add_timer('event_name_1', 60, "routine")

        # start a timer handle
        h.start()

    Read the event in the task:

    .. code-block::

        class MyTask(Task):
            def filter_event(self, event_type: str, event_data: str):
                if event_type == "timer" and event_data == "event_name_1":
                    ...
                elif event_type == "timer" and event_data == "event_name_2":
                    ...

    **Event Type**

    *   **routine**: routine firing timer event regularly (fixed interval).
    *   **one_shot**:  firing timer event once.
    *   **continue**: continuous firing timer event (vary interval).

    """

    # timer event type
    TYPE_ROUTINE = "routine"
    TYPE_ONE_SHOT = "one_shot"
    TYPE_CONTINUE = "continue"

    timer_event = pyqtSignal(str)

    class TimerHandle:
        def __init__(
            self,
            scheduler: sched.scheduler,
            event_name: str,
            interval: float = 1,
            event_type: str = "one_shot",
            repeat_times: int = None,
            callback: typing.Callable[[str, int], typing.Optional[bool]] = None,
        ):
            """

            Parameters
            ----------
            scheduler
                scheduler
            event_name : str
                event name
            interval : float
                event interval. unit second.
            event_type : {'routine', 'one_shot', 'continue'}
                event type
            repeat_times
                repeat times when event_type == 'continue'. None for infinite.
            callback
                timer callback with signature (event_name:str, counter:int) -> continue:bool?.
                If None, emit Timer.timer_event(f"timer:{event_name}").

            Raises
            ------
            ValueError
                if event_type not one of Timer.TYPE_*.

            """
            if event_type not in (
                Timer.TYPE_ROUTINE,
                Timer.TYPE_ONE_SHOT,
                Timer.TYPE_CONTINUE,
            ):
                raise ValueError()

            self._scheduler = scheduler
            self.event_name = event_name
            self.interval = interval
            self.event_type = event_type
            self.priority = 1
            self.repeat_times = repeat_times
            self.callback = callback

            self._event_id = None
            self._start_time: typing.Optional[float] = None
            self._stop_time: typing.Optional[float] = None

        @property
        def is_started(self) -> bool:
            """Did this timer start?

            Returns
            -------
            bool

            """
            return self._event_id is not None

        @property
        def start_time(self) -> typing.Optional[float]:
            """time stamp of this timer start.

            Returns
            -------
            start_time: float
                None if not is_started

            """
            return self._start_time

        @property
        def stop_time(self) -> typing.Optional[float]:
            """time stamp of this timer stop/canceled.

            Returns
            -------
            stop_time: float
                None if task is still alive.

            """
            return self._stop_time

        @property
        def duration(self) -> typing.Optional[float]:
            """time duration from this timer started.

            Returns
            -------
            duration : float
                None if not is_started

            """
            if self._start_time is None:
                return None

            if self._stop_time is not None and self._event_id is None:
                # timer has stopped
                return self._stop_time - self._start_time

            return time.time() - self._start_time

        def start(self, force=False):
            """start this timer.

            Parameters
            ----------
            force
                replace existed timer handler if it has started.

            Raises
            ------
            RuntimeError
                when timer has started if not force.
            """
            if self._event_id is not None:
                if not force:
                    raise RuntimeError()

                try:
                    self._scheduler.cancel(self._event_id)
                except ValueError:
                    pass

            self._start_time = time.time()
            self._stop_time = None
            self._event_id = self._scheduler.enterabs(
                self._start_time + self.interval,
                self.priority,
                self._timer_callback,
                argument=(1,),
            )
            logging.debug(f"timer[{self.event_name}] start")

        def cancel(self, force=False):
            """cancel this timer.

            Parameters
            ----------
            force
                 reserved use.

            """

            if self._event_id is not None:
                self._stop_time = time.time()
                try:
                    self._scheduler.cancel(self._event_id)
                    logging.debug(f"timer[{self.event_name}] cancel")
                except ValueError:
                    pass

        def _timer_callback(self, counter: int):
            """callback function"""
            if self.callback is None:
                Timer.INSTANCE.timer_event.emit(f"timer:{self.event_name}")
                cont = (
                    counter < self.repeat_times if self.repeat_times is not None else True
                )
            else:
                cont = self.callback(self.event_name, counter)
                if cont is None:
                    cont = (
                        counter < self.repeat_times
                        if self.repeat_times is not None
                        else True
                    )

            if self.event_type == Timer.TYPE_ROUTINE:
                self._event_id = self._scheduler.enterabs(
                    self.interval * (counter + 1) + self._start_time,
                    self.priority,
                    self._timer_callback,
                    argument=(counter + 1,),
                )

            elif self.event_type == Timer.TYPE_ONE_SHOT:
                # timer complete
                self._event_id = None
                self._stop_time = time.time()

            elif self.event_type == Timer.TYPE_CONTINUE:
                if cont:
                    self._event_id = self._scheduler.enter(
                        self.interval,
                        self.priority,
                        self._timer_callback,
                        argument=(counter + 1,),
                    )
                else:
                    # timer complete
                    self._event_id = None
                    self._stop_time = time.time()

    # singleton pattern
    INSTANCE: typing.ClassVar["Timer"] = None

    # Timer attribute
    _scheduler: sched.scheduler
    _timer: typing.Dict[str, TimerHandle]

    def __new__(cls, **event_config):
        # singleton pattern
        if cls.INSTANCE is None:
            logging.debug("Timer initialize")
            instance = QThread.__new__(cls)
            QThread.__init__(instance)

            # Timer attribute initialize
            instance._scheduler = sched.scheduler(time.time, instance.sleep)
            instance._timer = {}

            cls.INSTANCE = instance

            instance.start()
        else:
            instance = cls.INSTANCE

        return instance

    # noinspection PyMissingConstructor
    def __init__(self, **event_config):
        for name, options in event_config.items():
            if isinstance(options, dict):
                try:
                    self.add_timer(name, **options)
                except BaseException as error:
                    logging.debug("create timer", exc_info=error)

    def who_to_connect_where(self):
        return self.timer_event, "process_event"

    def __getitem__(self, item: str) -> TimerHandle:
        """get timer according its event name

        Parameters
        ----------
        item: str
            event name

        Returns
        -------
        timer handle: TimerHandle

        """
        return self._timer[item]

    def __del__(self):
        self.wait()

    def add_timer(
        self,
        event_name: typing.Optional[str],
        interval: float = 1,
        event_type: str = "one_shot",
        repeat_times: int = None,
        callback: typing.Callable[[str, int], typing.Optional[bool]] = None,
        replace=False,
    ) -> TimerHandle:
        """create a timer handle.

        Parameters
        ----------
        event_name : str
            event name
        interval : float
            event interval. unit second.
        event_type : {'routine', 'one_shot', 'continue'}
            event type
        repeat_times
            repeat times when event_type == 'continue'. None for infinite.
        callback
            timer callback with signature (event_name:str, counter:int) -> continue:bool?.
            If None, emit Timer.timer_event(f"timer:{event_name}").
        replace
            replace existed timer handle.

        Returns
        -------
        timer handle: TimerHandle

        """
        if event_name is not None:
            h = self._timer.get(event_name, None)

            if h is not None:
                if replace:
                    h.cancel(force=True)
                else:
                    raise ValueError()

        logging.debug(f"add timer[{event_name},{event_type},interval={interval}]")
        h = self.TimerHandle(
            self._scheduler, event_name, interval, event_type, repeat_times, callback
        )

        if event_name is not None:
            self._timer[event_name] = h

        return h

    def cancel_all(self):
        """cancel all running timers.

        """
        for e in list(self._scheduler.queue):
            self._scheduler.cancel(e)

    def clear_all(self):
        """cancel all running timers and remove all.

        """
        self.cancel_all()
        self._timer.clear()

    def run(self):
        logging.debug("Timer thread start")
        while True:
            try:
                self._scheduler.run(blocking=False)
            except BaseException as error:
                # catch all except, make timer thread always running.
                logging.warning("timer catch error", exc_info=error)

            self.msleep(100)
