"""GUI description for the hive app."""
import argparse
import glob
import logging
import os
import re
import sys

import yaml
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QMenuBar

from hive import custom_logging
from hive.controller import Controller
from hive.pyqt import QtWidgets
from hive.pyqt import UiExecutor
from hive.view_design.box_controller import Cheetah
from hive.view_design.box_controller import create_information_box
from hive.view_design.lab_app_view import Ui_MainWindow

__all__ = ["View"]


class View(QtWidgets.QMainWindow):
    def __init__(self, controller, config_path=None):
        """Description of the main view."""
        # set up the view
        QtWidgets.QMainWindow.__init__(self)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        logging.getLogger("").handlers[-1].set_ui(self.ui)
        self.ui.cheetah_setting.pressed.connect(self.launch_setting_ui)

        self.controller = controller
        self.controller.call_action.log.connect(self.process_log)

        self.log_message = ""
        self._events = []
        self.sequence_line = []
        self.plot_result = []
        self._success = 0
        self._fail = 0

        # List available config files
        self.ui.config_file.addItem("")

        for files in glob.glob(os.path.join("config_files", "*.yaml")):
            self.ui.config_file.addItem(files)

        if config_path is not None:
            for path in config_path:
                for files in glob.glob(os.path.join(path, "*.yaml")):
                    self.ui.config_file.addItem(files)

        self.ui.config_file.currentTextChanged.connect(self.select_config)
        self._enable_keyboard_field = False

        # Manual mode
        self.ui.user_label.returnPressed.connect(self.update_user)
        self.ui.user_input.returnPressed.connect(self.send_command)
        self.ui.release_button.pressed.connect(self.disconnect)

        # Sequence mode

        self.ui.fail.textChanged.connect(self.log_fail)
        self.ui.success.textChanged.connect(self.log_success)
        self.ui.SEQ_mode.stateChanged.connect(self.activate_task)
        self.ui.detection_input.returnPressed.connect(self.send_detection)

        self.setting_view = Cheetah(self)

    def launch_setting_ui(self):
        self.setting_view.show()

    def select_config(self):
        """Select and load the config file.

        The config parameters are given to the controller and are parse to update the view :
            - display log message
            - Hardware config : update the food delivery info grid and the door info grid
            - Command config : display all commands in the text box
            - Task config : display all sequences (if existing) in the sequence grid
        """
        self._cell = {}
        config_path = self.ui.config_file.currentText()
        if os.path.exists(config_path):
            with open(config_path, "r") as file:
                try:
                    config = yaml.safe_load(file)
                except Exception as error:
                    logging.warning("Error when loading the yaml file", exc_info=error)
                    create_information_box(
                        "Error when loading the yaml file: " + str(error), "Warning"
                    )
                    return
            self.controller.config = config

            logging.info("Connexion ON\n")
        else:
            logging.debug("No configuration file loaded\n")
            return

        try:
            self._device_map_check(config)
            self._create_table(config["Hardware"])
        except Exception as error:
            logging.warning(
                "Error when loading the Hardware configuration from the yaml file",
                exc_info=error,
            )
            create_information_box(
                "Error when loading the Hardware configuration from the yaml file: "
                + str(error),
                "Warning",
            )
            return

        try:
            self._display_commands(config["Command"])
        except Exception as error:
            logging.warning(
                "Error when loading the Command configuration from the yaml file",
                exc_info=error,
            )
            create_information_box(
                "Error when loading the Command configuration from the yaml file: "
                + str(error),
                "Warning",
            )
            return

        try:
            task_config = {"mode": "Task", "mode options": {}, "trigger": []}
            if "Task" in config:
                task_config.update(config["Task"])

            self.create_task(task_config)
            for trigger in task_config["trigger"]:
                if trigger["name"] == "Keyboard":
                    self._enable_keyboard_field = True
                    break
        except Exception as error:
            logging.warning(
                "Error when loading the Task configuration from the yaml file",
                exc_info=error,
            )
            create_information_box(
                "Error when loading the Task configuration from the yaml file: "
                + str(error),
                "Warning",
            )
            return

        logging.getLogger("").handlers[-1].set_cell(self._cell)  # configure LoggingView

    def _device_map_check(self, config):
        hardware = config["Hardware"]

        for device_group in list(hardware):
            device_list = hardware[device_group]

            if isinstance(device_list, list):
                device_list = {device_id: device_id for device_id in device_list}
                hardware[device_group] = device_list

            if not isinstance(device_list, dict):
                raise RuntimeError("incorrect Hardware declaration, type not dict")

    def _create_table(self, hardware):
        self.ui.door_table.setRowCount(0)
        self.ui.pump_table.setRowCount(0)

        if "Door" in hardware:
            group = hardware["Door"]
            for device in sorted(group):
                device_line = {
                    "name": QtWidgets.QTableWidgetItem(
                        "%s [%s]" % (device, group[device])
                    ),
                    "battery": QtWidgets.QTableWidgetItem(""),
                    "status": QtWidgets.QTableWidgetItem(""),
                }
                _add_line_in_table(self.ui.door_table, device_line)
                self._cell.update({group[device]: device_line})

        if "Food_delivery" in hardware:
            group = hardware["Food_delivery"]
            for device in sorted(group):
                device_line = {
                    "name": QtWidgets.QTableWidgetItem(
                        "%s [%s]" % (device, group[device])
                    ),
                    "battery": QtWidgets.QTableWidgetItem(""),
                    "status": QtWidgets.QTableWidgetItem(""),
                    "mode": QtWidgets.QTableWidgetItem(""),
                    "time": QtWidgets.QTableWidgetItem(""),
                    "interval": QtWidgets.QTableWidgetItem(""),
                    "distance": QtWidgets.QTableWidgetItem(""),
                }
                _add_line_in_table(self.ui.pump_table, device_line)
                self._cell.update({group[device]: device_line})

    def _display_commands(self, config):
        # Create the command list display in the graphic view
        commands = "COMMAND:\n"
        for code in config:
            if isinstance(config[code], list):
                commands += str(code) + ":\n"
                for step, conf in enumerate(config[code]):
                    commands += "    " + str(step)
                    commands += _parse_command(conf)
            else:
                commands += str(code) + _parse_command(config[code])
        self.ui.command_list.setText(commands)

    def create_task(self, config):
        """Activate the task with default value (manual mode) or with special mode defined in config file."""
        self.controller.create_task(config)
        self.controller.task.log.connect(self.process_log)

        options = config.get("options", None)
        if options is not None:
            self.controller.task.setup(options)
        if isinstance(self.controller.task.running, bool):
            self.ui.SEQ_mode.setEnabled(True)
            self.ui.sequence.setEnabled(True)

    def activate_task(self):
        """Trigger when automatic mode checkbox is checked/unchecked in the view."""
        if self.ui.SEQ_mode.isChecked():
            self.controller.send_task("start")
        else:
            self.controller.send_task("end")

    def _activate_task(self):
        if self.ui.SEQ_mode.isChecked():
            logging.info("Sequence automatic mode ON\n")

            if self._enable_keyboard_field:
                self.ui.detection_input.setEnabled(True)
                self.ui.label_5.setEnabled(True)
        else:
            if self._enable_keyboard_field:
                self.ui.detection_input.setEnabled(False)
                self.ui.label_5.setEnabled(False)
                self.ui.success.setText("0")
                self.ui.fail.setText("0")

            logging.info("Sequence automatic mode OFF\n")

    def update_user(self):
        """Change the username.

        Trigger the setup of all devices (claimed..etc.) with the new username.
        """
        username = self.ui.user_label.text()
        logging.info("Experimenter = " + username)
        self.controller.setup_devices(username)

    def send_command(self):
        """Send a command to the task logic layer."""
        try:
            self.controller.send_task(self.ui.user_input.text())
        except ValueError as error:
            logging.debug(str(error))
        self.ui.user_input.setText("")

    def send_detection(self):
        """Send a detection to the task logic layer."""
        self.controller.send_detection(self.ui.detection_input.text())
        self.ui.detection_input.setText("")

    def disconnect(self):
        """Disconnect the app / clear the user."""
        self.controller.disconnect()
        self.ui.user_label.setText("")

    def log_fail(self):
        """Log when the fail field changed."""
        self._fail = int(self.ui.fail.text())
        self.update_trial()

    def log_success(self):
        """Log success field changed.

        See Also
        --------
        ``log_fail``
        """
        self._success = int(self.ui.success.text())
        self.update_trial()

    def update_trial(self):
        """Update the trial field each time the success or the fail field changed."""
        self.ui.trials.setText(
            str(int(self.ui.fail.text()) + int(self.ui.success.text()))
        )
        self.update_graph()

    def update_graph(self):
        """Update the graph field each time the success or the fail field changed."""
        # Add a break line in the graph when the auto mode is reactivated

        self.ui.graph.plot(self.plot_result)

    @pyqtSlot(str)
    def process_log(self, logs):
        """Trigger when a log is sent from the task logic layer."""
        if logs:
            if "Start task" in logs:
                self.ui.SEQ_mode.stateChanged.disconnect()
                self.ui.SEQ_mode.setChecked(True)
                self.ui.SEQ_mode.stateChanged.connect(self.activate_task)
                self._activate_task()
                logging.debug(logs)
            elif "Stop task" in logs:
                self.ui.SEQ_mode.stateChanged.disconnect()
                self.ui.SEQ_mode.setChecked(False)
                self.ui.SEQ_mode.stateChanged.connect(self.activate_task)
                self._activate_task()
                logging.debug(logs)
            elif "(correct)" in logs:
                self.plot_result.append(1)
                self.ui.success.setText(str(self.controller.task.count["success"]))
                logging.debug(logs)
            elif "(incorrect)" in logs:
                self.plot_result.append(0)
                self.ui.fail.setText(str(self.controller.task.count["fail"]))
                logging.debug(logs)
            elif "Sequence : " in logs:
                self.ui.sequence.setText(re.sub("Sequence : [*?]", "", logs))
                logging.debug(logs)


def _add_line_in_table(table, device_line):
    line_index = table.rowCount()
    table.insertRow(line_index)
    for index, identifiant in enumerate(device_line):
        widget = device_line[identifiant]
        if isinstance(widget, QtWidgets.QTableWidgetItem):
            widget.setTextAlignment(0x0004 | 0x0080)
            table.setItem(line_index, index, widget)
        else:
            table.setCellWidget(line_index, index, widget)


def _parse_command(config):
    code_dict = {"call_action": " ", "output target": " ", "options": " "}
    code_dict.update(config)

    if isinstance(code_dict["call_action"], int):
        code_dict["call_action"] = str(code_dict["call_action"])

    if isinstance(code_dict["output target"], list):
        target = ", ".join(code_dict["output target"])
    else:
        target = str(code_dict["output target"])

    command = (
        " ".join(
            [" ", *code_dict["options"], ": ", "".join(code_dict["call_action"]), target]
        )
        + "\n"
    )
    return command


def main():
    parser = argparse.ArgumentParser(description="Launch an experiment")
    parser.add_argument(
        "--test",
        default=False,
        action="store_true",
        help="Enable test the ui without connection to the server",
    )

    parser.add_argument(
        "--debug",
        default=False,
        action="store_true",
        help="Enable debug output in log file",
    )

    parser.add_argument(
        "--log_path",
        type=str,
        nargs="?",
        default="~/Desktop/hive",
        help="Specify where to save the log",
    )

    parser.add_argument(
        "-C",
        "--config_path",
        action="append",
        dest="config_path",
        help="add extra config path",
    )

    parser.add_argument(
        "--ip", dest="ip", help="change ip address of the server", default=None
    )

    args = parser.parse_args()

    import socket

    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 1))  # connect() for UDP doesn't send packets
    local_ip = s.getsockname()[0]

    if local_ip.startswith("10.86.1"):
        ip = "10.86.1.244"
    elif local_ip.startswith("192.169.0"):
        ip = "192.168.0.100"
    else:
        ip = "127.0.0.1"

    if args.ip:
        ip = args.ip

    custom_logging.setup_logging(args.log_path, args.debug)

    # setup control
    main_controller = Controller(test=args.test, ip=ip)

    # setup GUI
    app = UiExecutor(View)

    # start GUI application
    sys.exit(app(kwds={"controller": main_controller, "config_path": args.config_path}))


if __name__ == "__main__":
    main()
