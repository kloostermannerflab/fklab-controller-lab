"""
Created on Tue Jun 07 00:19:55 2016

@author: admin
"""
import logging
import os
import re
import time

from PyQt5 import QtGui
from PyQt5.QtGui import QTextCursor

from hive.communication import Cheetah
from hive.view_design.box_controller import create_information_box


def setup_logging(log_path, debug=False):
    # setup logging
    localtime = time.localtime(time.time())
    folder_name = "%s-%s-%s_%s-%s-%s" % (
        str(localtime.tm_year),
        str(localtime.tm_mon),
        str(localtime.tm_mday),
        str(localtime.tm_hour),
        str(localtime.tm_min),
        str(localtime.tm_sec),
    )

    # Create a logger file based on the path given in option or by default in the desktop.
    if log_path != "-":
        # Default name of the file : ~/Desktop/hive/DATETIME/app.log
        log_path = os.path.expanduser(log_path)

        log_path = os.path.join(log_path, folder_name)
        os.makedirs(log_path, exist_ok=True)

        log_path = os.path.join(log_path, "app.log")
    else:
        log_path = None

    logging.basicConfig(
        filename=log_path,
        level=logging.DEBUG if debug else logging.INFO,
        filemode="w",
        format="%(asctime)s %(levelname)s - %(message)s",
        # keyword 'force' since python 3.8
        # force=True,
    )
    # define a Handler which writes INFO messages or higher to the sys.stderr
    cheetah = LogCheetah()
    logging.getLogger("").addHandler(cheetah)

    logView = LogGUI()
    logging.getLogger("").addHandler(logView)


class LogCheetah(logging.Handler):
    def __init__(self):
        logging.Handler.__init__(self)
        self.cheetah = Cheetah()
        self.checkList = []
        self.connect = True

    def emit(self, record):
        if not self.connect:
            return

        message = record.message.split("\n")
        for m in message:
            for check in self.checkList:
                if check(m):
                    try:
                        self.cheetah.event(m.encode())
                    except Exception as error:
                        msg = (
                            str(error)
                            + "\nTurn OFF cheetah log - see Cheetah setting to turn it ON again."
                        )
                        create_information_box(msg, "Warning")
                        self.connect = False
                        return
                    break


class LogGUI(logging.Handler):
    def __init__(self):
        logging.Handler.__init__(self)
        self.ui = None
        self._cell = {}

    def set_ui(self, ui):
        self.ui = ui
        log_path = logging.getLogger("").handlers[0].baseFilename
        self.log_display("Log file is saved in " + log_path)

    def emit(self, record):
        if self.ui:
            self.log_display(record.message)

    def set_cell(self, cell):
        self._cell = cell

    def log_display(self, message):
        """Display the log and update the device status grid.

        Parameters
        ----------
        message : str
            message format = Device_type Device_id : log_message \n
                             Device_type Device_id : log_message \n
                             ...

        .. note:: if ':' not in the message format, it should only display in the log box and not parse for the info grid.

        """
        # is_device_message = False

        message = re.sub(r"[\n\r]{2,}", "\n", message).strip()

        for line in message.split("\n"):  # type: str
            # parsing device message

            if ":" in line and "Unknown command" not in line:
                try:
                    device, state = line.split(":", maxsplit=2)
                    state = state.strip()
                    device_type, device_id = device.strip().split(" ", maxsplit=2)
                except Exception:
                    pass
                else:
                    if len(state) > 0 and device_id in self._cell:
                        self.update_device_status(device_type, device_id, state)

        cursor = QTextCursor(self.ui.logs.document())
        # set the cursor position (defaults to 0 so this is redundant)
        cursor.setPosition(0)
        self.ui.logs.setTextCursor(cursor)

        # insert text at the cursor
        color_format = self.ui.logs.currentCharFormat()
        if "ERROR" in message:
            color = QtGui.QColor(255, 0, 0)
        else:
            color = QtGui.QColor(0, 0, 0)
        color_format.setForeground(color)
        self.ui.logs.setCurrentCharFormat(color_format)
        self.ui.logs.insertPlainText(message + "\n")

    def update_device_status(self, device_type, device_id, state):
        # Battery level parsing
        if "battery" in state:  # state format: "battery level = level"
            _, level_battery = state.split("=")

            self._cell[device_id]["battery"].setText(level_battery)

            if int(level_battery[:-1]) < 25:
                self._cell[device_id]["battery"].setBackground(QtGui.QColor(255, 0, 0))
            else:
                self._cell[device_id]["battery"].setBackground(
                    QtGui.QColor(255, 255, 255)
                )

        elif device_type == "Pump":
            # Pump ...: Dispensing time for ... mode = 250
            # Pump ...: Save dispensing time for ... mode to 250
            # Pump ...: Manual mode ON
            # Pump ...: Automatic mode ON
            # Pump ... is rinsing the tube
            # Pump ... is dispensing the tube
            # Pump ... is filling the tube with chocolate sirup
            # Pump ... is empty the tube
            if "Dispensing time" in state or "dispensing time" in state:
                self._cell[device_id]["time"].setText(state.split(" ")[-1])
                self._cell[device_id]["time"].setTextAlignment(0x0004 | 0x0080)
            elif "mode ON" in state:
                *mode, _, _ = state.split(" ")
                self._cell[device_id]["mode"].setText(" ".join(mode))
                self._cell[device_id]["mode"].setTextAlignment(0x0004 | 0x0080)
            elif "interval" in state:
                self._cell[device_id]["interval"].setText(state.split(" ")[-1])
                self._cell[device_id]["interval"].setTextAlignment(0x0004 | 0x0080)
            elif "detect level" in state:
                self._cell[device_id]["distance"].setText(state.split(" ")[-1])
                self._cell[device_id]["distance"].setTextAlignment(0x0004 | 0x0080)
            elif "proximity sensor" in state:
                *mode, _, _ = state.split(" ")
                self._cell[device_id]["distance"].setText(state.split(" ")[-1])
                self._cell[device_id]["distance"].setTextAlignment(0x0004 | 0x0080)

        elif "ERROR" in state:
            self._cell[device_id]["status"].setText(" ".join(state.split(" ")[1:]))
            self._cell[device_id]["status"].setTextAlignment(0x0004 | 0x0080)
            self._cell[device_id]["status"].setBackground(QtGui.QColor(255, 0, 0))

        else:
            if "MOTOR FORBIDDEN" in state:
                # pass this warning message, in order to prevent from overwriting the door's status.
                # This message raises when we try to open an opened door or close a closed door.
                pass
            else:
                self._cell[device_id]["status"].setText("".join(state))
                self._cell[device_id]["status"].setBackground(QtGui.QColor(255, 255, 255))
                self._cell[device_id]["status"].setTextAlignment(0x0004 | 0x0080)
