import inspect
import logging
import re
import time

import PyQt5.QtCore
from PyQt5.QtCore import pyqtSignal

import hive.implementation as automatic_task
import hive.trigger as Triggers
from hive.actions import HardwareFailureError


__all__ = ["CallAction", "CallTask"]


class CallAction(PyQt5.QtCore.QObject):
    """Collect all catalogs of actions needed for the application and give a unique access through keyword for every available command.

    .. note::

        The keyword to call an call_action match the name of the method. Therefore, it cannot have the same name methods
        in multiple call_action classes.

    Attributes
    ----------
    _catalog_action : :obj: `list` of call_action classes
        list all objects containing an call_action catalog
    """

    log = pyqtSignal(str)
    """str : pyqtSignal emits all logs transmits from the different catalog classes."""

    def __init__(self):
        PyQt5.QtCore.QObject.__init__(self)
        self._catalog_action = []
        self._failure_handler = {}

    def __str__(self):
        message = ""
        for catalog in self._catalog_action:
            actions = [
                func[0]
                for func in inspect.getmembers(catalog, predicate=inspect.ismethod)
                if callable(getattr(catalog, func[0])) and not func[0].startswith("_")
            ]
            message += str(catalog) + ":\n    - " + "\n    - ".join(actions)
        return "All actions keyword available are :\n " + message

    def add_action_catalog(self, action):
        """Add an call_action catalog in the registry though a keyword.

        Parameters
        ----------
        action : ``object``
            Append the catalog object created before calling this method
        """
        self._catalog_action.append(action)

    def set_user(self, username):
        """Set the user in the communication protocol of all call_action class listed in the registry.

        Parameters
        ----------
        username : ``str``

        """
        for class_action in self._catalog_action:
            fct = getattr(class_action, "set_user", None)
            if fct:
                fct(username)

    def set_handler(self, failure_handler):
        for hdw_type in failure_handler:
            self._failure_handler[hdw_type] = {}
            types = []
            if isinstance(failure_handler[hdw_type], dict):
                failure_handler[hdw_type] = [failure_handler[hdw_type]]

            for handler in failure_handler[hdw_type]:
                handler_cfg = {"mode": "log", "type": ".*", "options": {}}
                handler_cfg.update(handler)
                if handler_cfg["type"] in types:
                    raise ValueError(
                        "ERROR Wrong error handler specification - cannot specify multiple modes for the same error type. \n"
                    )
                types.append(handler_cfg["type"])
                del handler_cfg["type"]

                if isinstance(handler_cfg["mode"], dict):
                    key = list(handler_cfg["mode"].keys())[0]
                    handler_cfg["options"] = handler_cfg["mode"][key]
                    handler_cfg["mode"] = key

                if handler_cfg["mode"] == "retry":
                    handler_cfg["mode"] = self.retry_failure_mode
                elif handler_cfg["mode"] == "command":
                    handler_cfg["mode"] = self.command_failure_mode
                    if isinstance(handler_cfg["options"], str):
                        options = handler_cfg["options"].split(" ")
                        handler_cfg["options"] = {"key": options[0]}
                        if len(options) > 1:
                            handler_cfg["options"]["options"] = options[1:]
                        handler_cfg["options"]["element_id"] = None

                    elif isinstance(handler_cfg["options"], dict):
                        default_option = {"options": [], "element_id": None}
                        default_option.update(handler_cfg["options"])
                        handler_cfg["options"] = default_option

                else:
                    raise ValueError(
                        "ERROR Not an existing failure mode : " + handler_cfg["mode"]
                    )

                self._failure_handler[hdw_type][types[-1]] = handler_cfg

    def setup(self, hdw_type, device_id):
        """Call the setup method based on the type of the device.

        Parameters
        ----------
        hdw_type : ``str``
            device type (example: door, food_delivery... etc.)
        device_id: ``int`` or ``str``

        .. note :: An call_action class should have a method called type_setup for each device type it is using.
        """

        for class_action in self._catalog_action:

            fct = getattr(class_action, str(hdw_type.lower()) + "_setup", None)
            if fct:
                try:
                    fct(device_id)
                except HardwareFailureError as error:
                    logging.warning(str(error))
                return

        raise ValueError(
            "ERROR No set-up implemented for this type of device : "
            + str(hdw_type)
            + "\n"
        )

    def get_action(self, key, *options, retry=0):
        """Get the right method based on the keyword.

        Parameters
        ----------
        key : :obj:`str`
            keyword - should be the name of one the method in the call_action class.
        """
        for class_action in self._catalog_action:
            fct = getattr(class_action, key, None)
            if fct:
                try:
                    fct(*options)
                except HardwareFailureError as error:
                    logging.warning(str(error))
                    self.manage_failure(error, retry, key, *options)

                return
        raise ValueError("ERROR Action " + key + " is not implemented\n")

    def manage_failure(self, error, retry, key, *options):
        handler_cfg = self._failure_handler.get(error.hdw_type, None)
        if handler_cfg is None:
            return

        message = str(error)
        retry += 1
        # find the right handle_failure
        error_type = None
        for handler_spec in handler_cfg:
            if handler_spec != ".*" and re.fullmatch(handler_spec, message):
                error_type = handler_spec
                break

        if error_type is None:
            error_type = ".*"

        handler = handler_cfg.get(error_type, None)
        if handler:
            max_times = handler["options"].get("max_times", 1)
            if max_times >= retry and handler:
                handler["mode"](key, *options, retry_time=retry, **handler["options"])

    def retry_failure_mode(self, key, *options, retry_time, wait_time=0, **kwargs):
        logging.warning(
            "After failure, try again (x"
            + str(retry_time)
            + ") times after sleeping for "
            + str(wait_time)
            + " secs."
        )
        time.sleep(wait_time)
        self.get_action(key, *options, retry=retry_time)

    def command_failure_mode(self, *options, retry_time, **replace_command):
        key = replace_command["key"]

        logging.warning("After failure, try this new command: " + key)
        replacement_element_id = replace_command.pop("element_id", None)
        element_id = options[1] if len(options) > 1 else None

        if replacement_element_id is None:
            if element_id:
                replacement_element_id = element_id
            else:
                self.get_action(
                    key, *replace_command.pop("options", []), retry=retry_time
                )
                return

        self.get_action(
            key,
            replacement_element_id,
            *replace_command.pop("options", []),
            retry=retry_time
        )


class CallTask:
    """Connect the automatic task and the triggers chosen from the config file.

    Parameters
    ----------
    action : :obj:`CallAction`
        interface object to send command from diverse catalogs of actions

    Attributes
    ----------
    _trigger : :obj:`hive.triger.Trigger`
    _task:

    """

    def __init__(self, action):
        self._trigger = {}
        self._task = None
        self._action = action

    @property
    def task(self):
        """:obj:`hive.implementation.task.Task` : give the automatic task chosen.

        .. note:: Not modifiable directly from outside. To set a new auto-mode, use the ``create_task`` method
        """
        return self._task

    def add_trigger(self, keyword, **kwargs):
        r"""Create a trigger based on a name of class in ``hive.trigger``.

        Parameters
        ----------
        keyword : :obj:`str`, {Server, Keyboard}
            Trigger class name

        **kwargs : options relative to the trigger class called

        See Also
        --------
        hive.trigger.Server : external trigger send by the Peira server
        hive.trigger.Keyboard : external trigger send through the keyboard by the user

        Raises
        ------
        ValueError
            If the keyword has no corresponding class

        """

        cls = getattr(Triggers, keyword, None)
        if cls and callable(cls):

            trigger = cls(**kwargs)
            self._trigger.update({keyword: trigger})
            return self._trigger

        available_keyword = _list_elements_in_module(Triggers, "class")
        raise ValueError(
            "Impossible to find the corresponding class to the trigger name, "
            + keyword
            + ", set in the config file. \n  Available triggers are "
            + ", ".join(available_keyword)
        )

    def create_task(self, keyword, **kwargs):
        """Set the automatic mode based on a name of class in ``hive.implementation``.

        Parameters
        ----------
        keyword : :obj:`str`, {SequenceTask}
            Automatic mode class name

        See Also
        --------
        hive.implementation.sequence_task.SequenceTask

        Raises
        ------
        ValueError
            If no trigger already specified (see ``create_trigger``)
            If the keyword has no corresponding class
        """
        if self._trigger is None:
            raise ValueError(
                "No trigger defined - You should add in your config file a trigger definition."
            )
        cls = getattr(automatic_task, keyword, None)

        if cls and callable(cls):
            self._task = cls(self._action, self._trigger, **kwargs)
            return self._task

        available_keyword = _list_elements_in_module(automatic_task, "class")
        raise ValueError(
            "Impossible to find the corresponding class to the task mode, "
            + keyword
            + ", set in the config file.\nAvailable modes are "
            + ", ".join(available_keyword)
        )


def _list_elements_in_module(module, element_type):
    if element_type == "class":
        class_name = [
            func[0]
            for func in inspect.getmembers(module, predicate=inspect.isclass)
            if callable(getattr(module, func[0])) and not func[0].startswith("_")
        ]
        if "QThread" in class_name:
            class_name.remove("QThread")
        if "pyqtSignal" in class_name:
            class_name.remove("pyqtSignal")
        return class_name
    elif element_type == "method":
        return [
            func[0]
            for func in inspect.getmembers(module, predicate=inspect.ismethod)
            if callable(getattr(module, func[0])) and not func[0].startswith("_")
        ]

    return None
