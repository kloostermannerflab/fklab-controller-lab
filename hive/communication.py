import socket

import zmq

__all__ = ["Server"]


class Server:
    """Create Peira server layer.

    Parameters
    ----------
    test : bool, optional
        flag for fake communication
    **connection : :obj:`dict` of :obj:`str`, optional
        can contains keys to change the default connection setting {seq_num, ip_address, port}

    Attributes
    ----------
    seq_num : :obj:`int`
        keep track of the number of message sent and need it in the message format to send to the server

    write_id : :obj:`socket`
        written socket

    test : bool
        flag from the user indicating if the Server need to be mocked with a fake communication
    """

    def __init__(self, test=False, **connection):
        default_connection = {"seq_num": 0, "ip_address": "192.168.0.100", "port": 3000}
        default_connection.update(connection)
        self.seq_num = default_connection["seq_num"]
        self._user = "unset"

        self.test = test

        if test:
            return
        self.write_id = socket.socket()
        self.write_id.connect(
            (default_connection["ip_address"], default_connection["port"])
        )

    @property
    def user(self):
        """:obj:`str`: Peira server accepts message only from user which have also claimed the device."""
        return self._user

    @user.setter
    def user(self, username):
        self._user = username

    def send_message_to_hardware(self, command, element_id):
        """Send command to the Peira server.

        Parameters
        ----------
        command : :obj:`str`
            code defined in the call_action method

        element_id : :obj:`str`

        See Also
        --------
        fklab.hive.actions.PeiraDevices.claim : Example of an call_action method generating a specific code for the claim call_action
        """
        if self._user == "unset":
            return "ERROR First set an user"

        if self.test:
            status = self._fake_comm(command, element_id)
        else:
            status = self._comm(command, element_id)

        if b"->>" in status:
            status = status.rsplit(b"->>", 1)[1].decode("utf-8")
        elif b"->" in status:
            status = status.rsplit(b"->", 1)[1].decode("utf-8")
        else:
            status = status.decode("utf-8")
        # Detect error message
        if status == "UNKNOWN COMMAND":
            return "ERROR Unknown command"
        elif "TCP Write in TCP" in status:
            return "ERROR Broken wifi connection at the device level".format(element_id)
        elif "not connected" in status:
            return "ERROR Not connected".format(element_id)
        elif "not reply" in status:
            return "ERROR No reply from the device".format(element_id)
        elif "ALREADY CLAIMED" in status or "CLAIMED BY" in status:
            return "ERROR The device is already claimed by someone else.\n".format(
                element_id
            )

        return str(status)

    def _comm(self, command, element_id):
        self.seq_num += 1
        message = (
            "["
            + self._user
            + ","
            + str(element_id)
            + ",2,1000,"
            + str(self.seq_num)
            + "] "
            + command
        )
        self.write_id.send(message.encode("utf-8"))
        status = self.write_id.recv(1024)
        return status

    def _fake_comm(self, command, element_id):
        if "RDIF" in command:
            if int(element_id) < 100:
                status = "RDIF->>INFO 22-04-2020 18:32:09 T'DOOR' SW2 ID101 B067% I0x0000 L1 PUL1 DM0 E1"
            else:
                status = "RDIF->>INFO 22-04-2020 18:32:09 T'REWARD' SW2 ID101 B067% I0x0000 L1 PUL1 DM0 E1"
        elif "READ" in command:
            status = "READ->>READ BLA 100"
        elif "CLAIM" in command:
            status = "CLAIM->CLAIM"
        elif "RELEASE" in command:
            status = "CLAIM->RELEASE"
        elif "PUMP" in command:
            status = "ERROR Not connected"
        else:
            status = "-->" + str(command)

        return status.encode("utf-8")


class CheetahError(Exception):
    pass


class CheetahConnectionError(CheetahError):
    pass


class CheetahRequestError(CheetahError):
    pass


class Cheetah:
    def __init__(self, address="localhost", port=5555, context=None):
        self._ip_address = str(address)
        self._port = int(port)

        self._context = context or zmq.Context.instance()
        self._poller = zmq.Poller()
        self._request_socket = None

        self._connected = False
        self._connect()

    def _connect(self):
        if self.connected:
            return

        self._request_socket = self._context.socket(zmq.REQ)
        self._request_socket.connect(self.server_address)
        self._poller.register(self._request_socket, zmq.POLLIN)
        self._connected = True

    def _disconnect(self):
        if not self.connected:
            return

        self._request_socket.setsockopt(zmq.LINGER, 0)
        self._request_socket.close()
        self._poller.unregister(self._request_socket)
        self._request_socket = None
        self._connected = False

    def _reconnect(self):
        self._disconnect()
        self._connect()

    def request(self, *commands, **kwargs):
        if not self._connected:
            self._connect()

        self._request_socket.send_multipart(commands)

        socks = dict(self._poller.poll(kwargs.get("timeout", 5000)))

        if socks.get(self._request_socket) == zmq.POLLIN:
            reply = self._request_socket.recv_multipart()

        else:
            self._disconnect()
            raise CheetahConnectionError("Cheetah ZMQ Router is not online.")

        return reply

    @property
    def connected(self):
        return self._connected

    @property
    def ip_address(self):
        return self._ip_address

    @ip_address.setter
    def ip_address(self, value):
        self._ip_address = str(value)

    @property
    def port(self):
        return self._port

    @port.setter
    def port(self, value):
        self._port = int(value)
        self._reconnect()

    @property
    def server_address(self):
        return "tcp://{ip}:{port}".format(ip=self._ip_address, port=self._port)

    def event(self, eventstring, ttl=0, eventid=0):
        reply = self.request(
            b"event", eventstring, str(int(ttl)).encode(), str(int(eventid)).encode()
        )

        if reply[0] == b"OK":
            return reply[1] if len(reply) > 1 else None
        else:
            raise CheetahRequestError(
                reply[1].decode() if len(reply) > 1 else "Unspecified error."
            )
