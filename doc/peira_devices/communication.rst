Setup the server
================
TBD

Device internal communication
=============================

Messages are sent on the port 3000 (writing port) follow by an answer on the same port.
Some messages are also sent by the device on the port 3001 (listening port).

Device messages
---------------

CLAIM
.....

Send message: [lies,1,2,1000,1] CLAIM
Received message: [lies,1,2,1000,1] CLAIM->DEVICE CLAIMED

RELEASE
.......

Send message: [lies,1,2,1000,192] RELEASE
Received message: [lies,1,2,1000,192] RELEASE->DEVICE RELEASED

INFO
....

Send message: [lies,1,2,1000,3] RDIF

DOOR
****

Received message: [lies,1,2,1000,3] RDIF->>INFO 22-04-2020 18:32:17 T'DOOR' SW2 ID1 B055% I0x0000 L1 PUL1 H20000 R20000 E1

FOOD DELIVERY DEVICE
********************
Received message: [lies,101,2,1000,16] RDIF->>INFO 22-04-2020 18:32:09 T'REWARD' SW2 ID101 B067% I0x0000 L1 PUL1 DM0 E1
timestamp clock  device type Software Version device ID Battery level input state Led’s on  Led power up state dispensing mode Auto_event
- 2=One shot mode
- 1=automatic with proximity sensor,
- 0=manual

Error messages
--------------

UNKNOWN COMMAND
...............
Send message: [lies,1,2,1000,63] READ H 1  => Problem with this command
Received message: [lies,1,2,1000,63] READ H 1->>UNKNOWN COMMAND

CONNEXION PROBLEM
..................

Send message: [lies,13,2,1000,8] CLAIM
Received message: [lies,13,2,1000,8] CLAIM->DEVICE CLAIMED
Send message: [lies,13,2,1000,9] HOME
Received message: [lies,13,2,1000,9] HOME->ERROR TCP Write in TCP driver.vi:5070017

NOT CONNECTED
.............

Send message: [lies,3,2,1000,7] CLAIM
Received message: [lies,3,2,1000,7] CLAIM->ERROR Device is not connected

NO REPLY
........

Send message: [lies,12,2,1000,5] HOME
Received message: [lies,12,2,1000,5] HOME->ERROR No reply from device.

Door messages
-------------

HOME
....

Send message: [lies,1,2,1000,2] HOME
Received message: [lies,1,2,1000,2] HOME->>HOME
Received message on the listen port: >EVNT 10 1 56 22-04-2020 18:57:19 command home   --- Start homing
Received message on the listen port: >EVNT 10 1 57 22-04-2020 18:57:19 homing         --- Finish homing

OPEN DOOR
.........

Send message: [lies,1,2,1000,95] DOPN 60 250 800
Received message: [lies,1,2,1000,95] DOPN 60 250 800->>DOPN 60 250 800
Received message on the listen port: >EVNT 10 1 60 22-04-2020 18:58:25 command open
Received message on the listen port: >EVNT 10 1 61 22-04-2020 18:58:25 opening
Received message on the listen port: >EVNT 10 1 62 22-04-2020 18:58:28 is open

CLOSE DOOR
..........

Send message: [lies,1,2,1000,104] DCLS 60 250 30
Received message: [lies,1,2,1000,104] DCLS 60 250 30->>DCLS 60 250 30
Received message on the listen port: >EVNT 10 1 63 22-04-2020 18:58:30 command close
Received message on the listen port: >EVNT 10 1 64 22-04-2020 18:58:30 closing
Received message on the listen port: >EVNT 10 1 65 22-04-2020 18:58:34 is closed

INFO DOOR STATE
...............

Send message: [lies,1,2,1000,103] RDMO
Received message: [lies,1,2,1000,103] RDMO->>MOTOR_STATE 0 POS_OPEN
Received message: [lies,1,2,1000,105] RDMO->>MOTOR_STATE 0 POS_CLOSED

MOTOR FORBIDDEN
...............

Send message: [lies,1,2,1000,87] DCLS 60 250 30
Received message: [lies,1,2,1000,87] DCLS 60 250 30->>MOTOR FORBIDDEN

Food delivery messages
----------------------

DELIVER FOOD (IF MODE MANUAL ACTIVATED)
.......................................

Send message: [lies,101,2,1000,47] PUMP 0
Received message: [lies,101,2,1000,47] PUMP 10600->>PUMP 0
Received message on the listen port: >EVNT 30 104 823 22-04-2020 18:56:49 command 0


FILL TUBE WITH CHOCOLATE SIRUP
..............................

Send message: [lies,101,2,1000,47] PUMP 10600
Received message: [lies,101,2,1000,47] PUMP 10600->>PUMP 10600
Received message on the listen port: >EVNT 30 104 823 22-04-2020 18:56:49 command 10600

EMPTY THE TUBE
..............

Send message: [lies,101,2,1000,115] RINS 10 11000 1
Received message: [lies,101,2,1000,115] RINS 10 11000 1->>RINS 10 11000 1
Received message on the listen port: >EVNT 32 101 311 22-04-2020 19:27:47 10 11000 1

RINSE THE TUBE
..............

Send message: [lies,101,2,1000,115] RINS 6000 9000 4
Received message: [lies,101,2,1000,115] RINS 10 11000 1->>RINS 6000 9000 4
Received message on the listen port: >EVNT 32 101 311 22-04-2020 19:27:47 6000 9000 4


SAVE FOOD DELIVERY PARAMETERS
.............................

SAVE DELIVERY TIME FOR ALL MODES
********************************

Send message: [lies,101,2,1000,139] SAVE DT 300 1
Received message: [lies,101,2,1000,139] SAVE DT 300 1->>SAVE DT 300 1
Received message on the listen port: >PARM 50 107 25 22-04-2020 19:04:23 SAVE DT 300 1

UPDATE THE DISPENSING INTERVAL
******************************

Send message: [lies,101,2,1000,179] SAVE DI 5000 1
Received message: [lies,101,2,1000,179] SAVE DI 5000 1->>SAVE DI 5000 1
Received message on the listen port: >PARM 50 101 273 22-04-2020 19:04:56 SAVE DI 5000 1

MODE MANUAL
***********

Send message: [lies,101,2,1000,190] SAVE DM 0
Received message: [lies,101,2,1000,190] SAVE DM 0->>SAVE DM 0
Received message on the listen port: >PARM 50 101 266 22-04-2020 18:57:12 SAVE DM 0

MODE AUTO
*********

Send message: [lies,101,2,1000,190] SAVE DM 1
Received message: [lies,101,2,1000,190] SAVE DM 0->>SAVE DM 1
Received message on the listen port: >PARM 50 101 266 22-04-2020 18:57:12 SAVE DM 1

MODE ONE SHOT
*************

TBD


READ FOOD DELIVERY PARAMETERS
.............................

Send message: [lies,101,2,1000,131] READ DTC 1
Received message: [lies,101,2,1000,131] READ DTC 1->>READ DTC 100

Send message: [lies,108,2,1000,162] READ DTA 1
Received message: [lies,108,2,1000,162] READ DTA 1->>READ DTA 300

Send message: [lies,101,2,1000,163] READ DTB 1
Received message: [lies,101,2,1000,163] READ DTB 1->>READ DTB 300

Send message: [lies,101,2,1000,171] READ DI 1
Received message: [lies,101,2,1000,171] READ DI 1->>READ DI 5000

Send message: [lies,101,2,1000,71] READ DC 1
Received message: [lies,101,2,1000,71] READ DC 1->>READ DC 2


ANIMAL DETECTION
................

Received message on the listen port: >EVNT 31 104 808 22-04-2020 18:52:53 off
Received message on the listen port: >EVNT 31 104 809 22-04-2020 18:52:53 on


.. note::

    What is happening if mode automatic activated and send the command PUMP 0 ?
    What messages are send when setting the mode one-shot ?
    What messages are send when the mode one-shot is triggered ?
