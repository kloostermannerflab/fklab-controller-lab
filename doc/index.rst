.. raw:: html

    <style>
        .row {clear: both}
        .row h2  {border-bottom: 1px solid gray;}

        .column img {border: 1px solid gray;}

        @media only screen and (min-width: 1000px),
               only screen and (min-width: 500px) and (max-width: 768px){

            .column {
                padding-left: 20px;
                padding-right: 20px;
                float: left;
            }

            .column3  {
                padding-left: 70px;
                padding-right: 20px;
                width: 20%;
            }

            .column2  {
                width: 49%;
            }
        }

    </style>


######################
Hive - Kloosterman lab
######################

:Release: |release|
:Date: |today|

.. rst-class:: clearfix row

===============
Lab application
===============

A lab app have been developed to make the platform easily controllable in remote. It can be entirely personalized to your
experimentation from the user command to send to the type of tasks (automatically run) with your own hardware.

.. rst-class:: clearfix row

.. rst-class:: column column3

`User Guide <app.rst>`_
.......................

.. image:: image/view_app.gif
   :width: 900
   :height: 200
   :alt: image to go in the user guide section
   :target: app.html

.. rst-class:: column column3

`App Configuration <configuration.rst>`_
........................................

.. image:: image/config.png
   :width: 600
   :height: 180
   :alt: image to go in the configuration section
   :target: configuration.html

.. rst-class:: column column3

`App Development <development.rst>`_
....................................

.. image:: image/experiment_control_architecture.svg
   :width: 200
   :height: 180
   :alt: image to go in the development section
   :target: development.html

.. rst-class:: clearfix row


======================
Work with the hardware
======================

Set up the devices, platform ... etc.

TBD

.. toctree::
   :maxdepth: 2

   peira_device
   fklab_device



.. toctree::
   :maxdepth: 1
   :hidden:

   app
   configuration
   development
   peira_device
   fklab_device
