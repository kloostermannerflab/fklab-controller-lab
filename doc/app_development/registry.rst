Event/Action mapping (registry module)
**************************************

The hardware layer and the app layer are separated by an interface (registry). It allows flexibility by adding new triggers
or action catalog with always the same entry point (registry class). Then, the app, when reading the config file, will
automatically add the useful hardware classes based on the hardware needed.

There is two different registry class :

.. automodsumm:: hive.registry
   :toctree: ../generated
