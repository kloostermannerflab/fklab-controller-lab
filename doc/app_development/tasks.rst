
Create new tasks
****************

To ask for others implementations - either open an issue or create your own in the implementation folder.

You have to create your task derived from AbstractTask. The response to user command are already define
but you can still override certain command answers or add new command in your own class.
You can also add new parameters and setup the condition for a food reward.

More information on how to personalize your task in the class description below.

.. automodsumm:: hive.implementation.abstract_task
   :toctree: ../generated

You have also to define your protocol. Create a class derived from SequenceControl and implement the protocol method.
More information in the class description below.

.. automodsumm:: hive.implementation.task_protocol
   :toctree: ../generated

For more flexibility, you could also start from an upper level with no state machine already developed.

The structure to read detection and read user input is already implemented but the answer of this events is completely free for
you to implement it. There is also no reward and score system.

.. automodsumm:: hive.implementation.task
   :toctree: ../generated

Create new triggers
*******************

It needs to derive from the Trigger class and emit with the detection pyqtsignal a sequence id (most of the time a delivery food id)
in string format.

.. code-block::

    def who_to_connect_where(self):
        return self.pyqtslotevent, "process_event"

The idea is to return in the who_attached_who method the pyqtSlot used to emit data. When adding the trigger to the task
it will automatically connect this pyqtSlot to the method indicated as second string output. Most of the time, we want
to proceed the event in the process_event method.

The event emitted should have this format :

.. code-block::

    self.pyqtslotevent.emit("event_type: event")

Event type already implemented in the process_event are:

- detection - the event should be an unique zone id.
- battery level - the event has this format "battery level: device_id,battery_percentage"

You can easily add your own event type and then define what to do with it in the process_event method of your class.
