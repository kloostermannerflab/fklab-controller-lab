How to maintain or improve the app
----------------------------------

Code architecture
.................

The view is designed with QT5 designer. ui files and their python translation can be found in the view_design folder.
Most choice design are developped and explained in the View class.

.. automodsumm:: hive.lab_app
   :toctree: ../generated

How to use actions in the app
.............................

.. code-block:: python

    from hive.registry import CallAction
    from hive.communication import Servor

    call_action = CallAction()

    servor = Servor()# Possibility to add a connexion dictionary in parameter to change the ip adress or the input/output port
                         # Default:  "ip_address": "192.168.0.100", "port_send": 3000
    call_action.add_action_catalog("PeiraDevices", servor)

    #Example 1 : Move the door 3
    key = "move_door"   # Key is the action to call (see the dictionary of available action)
    device_id = 3
    log_message = call_action.get_action(key, device_id)

    #Example 2: Change the time delivery setting to 10ms of the device 101 for all modes
    log_message = call_action.get_action("delivery_setting", 101, "all", 10)
