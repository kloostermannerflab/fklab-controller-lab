Create new actions
******************

To ask for others signals - either open an issue or add your method via pull request:

- in a new catalog :

.. code-block:: python

    class new_catalog:
        def __init__(communication):
            self.communication = communication

        def type_setup():  # replace type by the type of your devices.
           # Setup your device here

        def add_actions():
            # Do your stuff
            return "Type " + element_id + ": " + status    # See previous code snippet for a focus on this method

.. note::

    The catalog needs also to be add in the registry class ``hive.registry.CallActions``
    with a keyword in the method ``hive.registry.CallActions.add_action_catalog``

- in an already existing catalog :

.. code-block:: python

    def keyword_action(self, element_id, **options) :
        # Do what you want
        # Build the message message = ?
        message = "...\r\n"
        status = self.send_message_to_hardware(message, element_id)
        if success_condition: # Success of the command depend of the type of communication used
           return "Device" + element_id + ": your_message" # Because of : in the message, this message will be parse to be add in the status grid of the ui
                                                           # without it the message is only wrote in the log file and the log box in the ui.

        return "Device" + element_id + " " + status      # Device can be change by the device type, (Door, Pump...)

Don't forget to modify this documentation by adding your action in the list
