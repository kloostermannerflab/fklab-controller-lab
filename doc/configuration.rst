How to configure the app to your own experimentation
****************************************************

.. toctree::
   :maxdepth: 2

   app_configuration/config_file
   app_configuration/config_example
   app_configuration/actions_and_tasks
