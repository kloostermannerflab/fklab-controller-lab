Tasks and Triggers
==================

A task is specified in two parts :

- the Task itself and the one you should call in your config file. (the name end by Task).
  This part control the options and reward system.

- The protocol definition, called internally in the task class. (the name end by Control).
  This part define if the actual move is a success following a certain protocol and what should be the next step.

It will also be associated with some triggers sending information as detection or user input.

**Use in Hive**:

#. Make sure you have correct setting in config file
#. Open hive and select you config file. then type experimenter (*User name* field) to initial device.
#. Type 'HELP' to see what commands supported by this task in log window.

    beside 'help', you can type 'score', 'visit', 'status' anytime to print information in log window.

#. If needed by the task,
    - set task mode by `mode <mode>` (replace `<mode>` to mode number).
    - set sequence you want to test by `set <seq...>` (replace `<seq>` with sequence ID spread by space).
    - set any other parameters

#. Start task by 'start' or by checking the SEQUENCE AUTO MODE button in the ui: all doors are opened
   and the setup method specific to your task is activated.

#. If using keyboard to trigger detection, input digital number which the animal visit. if not, wait for automatic detection.
#. Stop the task by 'end' or by unchecking the SEQUENCE AUTO MODE button

Existing tasks
..............

- **Incremental difficulty sequence task**

.. automodsumm:: hive.implementation.sequence_task_inc_diff
   :toctree: ../generated

- **Error correction sequence task**

This task has two different protocols called depending on the mode selected.

.. automodsumm:: hive.implementation.sequence_task_err_corr
   :toctree: ../generated


- **W alternation sequence task**

.. automodsumm:: hive.implementation.w_alter_task
   :toctree: ../generated

- **Two phases win-shift task**

.. automodsumm:: hive.implementation.two_phases_win_shift
   :toctree: ../generated

Existing triggers
.................

The User command trigger is always associated to the task by default. It allows to send commands for the task
(defined in the task class) or additional command for the maze devices (defined in the config file).

The other triggers to be used needs to be added in the config file. Check each trigger definitions to see how.

.. automodsumm:: hive.trigger
   :toctree: ../generated

Existing catalog of actions
===========================

This list the possible action keywords to be add in your config file to create personalized user commands.

.. automodsumm:: hive.actions
   :toctree: ../generated
