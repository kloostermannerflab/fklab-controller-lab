Write your own app config file
==============================

For the moment, the config files should be put in a folder config_files where the software is launched.
It will be improved in the future.

There is 4 parts in the configuration file. See a full `example <config_example.rst>`_ of a config file.

Hardware
--------

Declare which device you need to use.

.. code-block:: yaml

    Hardware :
        Door: # Doors used
            # virtual ID: actual ID
            D1: '001' # name device '001' as 'D1'

        Food_delivery: # Food delivery used
            # virtual ID: actual ID
            F1: '101' # name device '101' as 'F1'

There are two major device group `Door` and `Food_delivery` (name cannot be changed).
In the device group, you can declare which devices you want to use, and also can give
those device a name.


Declare device with their actual ID and give it a name (virtual ID).

.. code-block:: yaml

    Door: # device group
        # device name mapping. virtual ID: actual ID
        D1: '001' # name device '001' as 'D1'
        D2: '002' # name device '002' as 'D2'

You still can use old-style declaration.

.. code-block:: yaml

    Door: ['001', '002']

    # equals to
    Door:
        '001': '001'
        '002': '002'


With the name mapping table, we can use virtual ID in *Command* section, and change the
device easily (by changing the actual device ID which virtual ID maps to instead of finding
and replacing used site in whole config file).

.. code-block:: yaml

    Command:
        C1:
            action: close_door
            output target: D1

Task
----
This key is describing the automated task with the mode and the triggers to associate.
The platform is available with only one mode at the time but with as many you want triggers.
Take a look in the `task and trigger documentation <actions_and_tasks.rst>`_ to see what is available.

.. code-block:: yaml

    Task:
        mode : SequenceTask
        mode options :
            max_trials: 100
        trigger :
          - name: Keyboard
          - name: Server

Command
-------

This part is matching the command send by the user and the actions (1 or more) and the targets (one or more).
if you want to specify some value option from the user command at running time, you need to add options
specifying the option name. (see the config example file for more detail on it).

Declare commands which you can type at *Input* text field.

.. code-block:: yaml

    Command: # Command section
        C1: # command
            action: close_door
            output target: D1

Command can be a group of sequential commands.

.. code-block:: yaml

    Command: # Command section
        C1: # command
          - action: close_door
            output target: D1
          - action: 1 # sleep 1 sec
          - action: open_door
            output target: D1

The detail command declaration:

*   action

    Name of the action, which are defined in the different `catalogue of actions <actions_and_tasks.rst#existing-catalog-of-actions>`_. It can be a name of list of name.
    If you give a digit number, hive will consider it a *sleep* command.

*   output target

    used device. can be virtual ID, actual device ID, device group or list of above.

*   options

    command options in list type. the extra arguments to the actions.

    special options:

    *   output_target

On failure
----------

The on failure section is used to specify which behavior we want in case of errors returned by the hadware.
There is two modes :

- retry with possible options
        - max_time : specified the number of retry
        - wait_time : specified the time to wait in between retry

- try another command

You can differentiate the mode wanted between hardware type and error messages received. See example below:

.. code-block:: yaml

    On failure:
      Door:
       - type: ".* not respond"
         mode:
            retry:
              max_times: 3
              wait_time: 1

       - type: ".*"
         mode:
            command: open_door

      Food_delivery:
        mode:
            retry:
              max_times: 5

This example means :

- retry 3 time with waiting 1sec between when receiving a "not respond" error on door devices
- open the door by default for any other error messages on door devices
- retry 5 times the same command for any errors message on the pump devices
