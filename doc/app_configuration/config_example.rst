Configuration example
=====================

.. code-block:: yaml

    Hardware :
        Door: # Doors id used
            # virtual ID: actual ID
            D1: '001' # name device '001' as 'D1'
            D2: '015'
            D3: '017'

        Food_delivery: # Food delivery id used
            # virtual ID: actual ID
            F1: '101' # name device '101' as 'F1'
            F2: '102'
            F3: '103'
    Task:
        mode : WAlterTask
        mode options :
            sequences: # set sequence used in this task
                - 1  # Left = F1
                - 3  # Center = F3
                - 2  # Right = F2
        trigger :
        - name: Keyboard      # Added this trigger if you want to trigger the animal detection via the keyboard
        - name: Server        # The server trigger needs to be always added if we want to be warn of low level battery

    Command:
        'm1':   			# Order to be sent by the user
            action : move_door      # Action triggered by this order
            output target: D1     # Device id concerned by this order

        'id':
            action : identify
            options: ['output_target']  # You can add options -- the user needs to send the order "id D1" to identify the device 1 but it can also send "id D2" to identify the device 15

        'DT':
            action : delivery
            output target: F1
            options: ['delivery mode', 'delivery time'] # Some actions as specific options - see the action documentations

        'SETUP':  # Multi action in the command
            - action : home
              output target : Door
            - action: identify
              output target: ['Food_delivery','Door']
            - action: fill
              output target: Food_delivery
            - action: mode_auto
              output target: Food_delivery

        'c2_3':
            action: close_door
            output target: [D2, D3] # you can targeted multiple devices by specifying it in an array.
                                       # Careful, don't forget "", it needs to be str

        'o':
            action: open_door
            output target: Doors  # you can targeted all devices by putting the type keyword

        'c3_w_c2':
            - action: close_door
              output target: D3
            - action: 8                # By adding a number in the action, you will wait 8 ms between both actions
            - action: open_door
              output target: D2       # Put a space as target device

        'c3_w_o3':
            action: close_door,8,open_door
            output target: D3            # This is also working except that here the device targeted by open_door is also
                                         # 17 and not 15 as in the previous command
    On failure:
        Door:
        - type: ".* not respond"
          mode:
              retry:
                max_times: 3
                wait_time: 1

        - type: ".*"
          mode:
              command: open_door

        Food_delivery:
            mode:
              retry:
                max_times: 5
