Peira devices
*************

.. toctree::
   :maxdepth: 2

   peira_devices/setup
   peira_devices/communication
