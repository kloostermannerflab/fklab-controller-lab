Setup the serial communication
******************************

Plug an arduino on usb and setup a serial port of your choice with the arduino IDE.
The baudrate by default should be 11200 but could be changed in hive if needed.

The minimal arduino code should read on the serial input and act accordingly to the message received.

Device internal communication
*****************************

Command already parametred in hive :

+---------------+------------------+----------------------------+
| Hive command  | message received |         Description        |
|               | in the arduino   |                            |
+===============+==================+============================+
| start_pulse   |        d         | Activate stimulation phase |
+---------------+------------------+----------------------------+
| stop_pulse    |        s         | Stop stimulation phase     |
+---------------+------------------+----------------------------+
