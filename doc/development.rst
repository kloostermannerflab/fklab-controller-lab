
How to develop add-ons for your app
***********************************

There is two types of development:

- development of new tasks and port of new hardware in the app

    .. toctree::
       :maxdepth: 1

       app_development/actions
       app_development/tasks

- development of the app itself with the gui and how it is linked to the task running in background.

    .. toctree::
       :maxdepth: 1

       app_development/gui
       app_development/registry

.. image:: image/experiment_control_architecture.svg
