![logo](doc/image/logo.png?raw=true "Title")


## Requirements

Make sure you have the following packages installed.

- PyQt5
- pyqtgraph

**Additional packages for building the documentation:**

- sphinx
- sphinx_automodapi
- nbsphinx

## Install the app
```
git clone https://bitbucket.org/kloostermannerflab/fklab-controller-lab
cd fklab-controller-lab
python setup.py build_ext --inplace
pip install -e . --no-deps
```

## Documentation
After installing the app, the documentation can be build by running in the root directory
```
make html
```
Then, open the index.html in the directory build/html

## Launch the app

```
usage: lab [-h] [--test [TEST]] [--log_path [LOG_PATH]]

Launch an experiment

optional arguments:
  -h, --help            show this help message and exit
  --test [TEST]         Enable test the ui without connexion to the servor
  --log_path [LOG_PATH]
                        Specify where to save the log - Default : $HOME/Desktop

```

## Status of the project
- controller app : generated by a config file for each experimentation

    - [x] create a behavior app by reading a config file
    - [x] create a view with a log part which display messages and a device status grid
    - [x] claim devices and verify if not already claimed by someone else
    - [x] home doors
    - [x] add battery level
    - [x] receive messages from devices in a different threads
    - [ ] state machine engine in background to specify more complex experimentation than a sequence
    - [ ] display and use these messages with timestamps
    - [ ] send log messages to Neuralynx
    - [ ] receive log message from falcon


- config experimentation app : link events, actions and components together [0%done]
