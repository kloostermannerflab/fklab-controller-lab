import re

from setuptools import setup

VERSIONFILE = "hive/_version.py"
verstrline = open(VERSIONFILE, "rt").read()
VSRE = r"^__version__ = ['\"]([^'\"]*)['\"]"
mo = re.search(VSRE, verstrline, re.M)
if mo:
    verstr = mo.group(1)
else:
    raise RuntimeError("Unable to find version string in %s." % (VERSIONFILE,))


setup(
    name="hive",
    version=verstr,
    packages=["hive", "hive.implementation", "hive.view_design"],
    install_requires=["pyyaml>=5.3", "pyzmq", "numpy", "pyserial"],
    entry_points={"console_scripts": ["hive=hive.lab_app:main"]},
    author="Chaput Marine",
    author_email="marine.chaput@nerf.be",
    description="Application for control of the experimentation environment.",
    zip_safe=False,
    include_package_data=True,
)
